package graph

import (
	"gitlab.com/unie.kz/nova/finance/cmd/client"
	"gitlab.com/unie.kz/nova/email/cmd/client"
)

//GraphQLServer Twirp RPC client struct
type Resolver struct {
	financeClient *finance.Client
	emailClient		*smtp.Client
}

func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

//NewGraphQLServer Twirp RPC client init
func New(financeURL, emailURL  string) Config {
	financeClient, _ := finance.NewClient(financeURL)
	emailClient,_ := smtp.NewClient(emailURL)
	c := Config{
		Resolvers: &Resolver{
			financeClient,
			orderClient,
			smsClient,
			emailClient,
			cpClient,
			lifestyleClient,
		},
	}
	return c
}

type mutationResolver struct{ *Resolver }

type queryResolver struct{ *Resolver }
