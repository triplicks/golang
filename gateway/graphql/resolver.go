//go:generate gorunpkg github.com/99designs/gqlgen

package graphql

import (
	context "context"

	graph "gitlab.com/unie.kz/nova/gateway/graph"
	model "gitlab.com/unie.kz/nova/gateway/model"
)

type Resolver struct{}

func (r *Resolver) Mutation() graph.MutationResolver {
	return &mutationResolver{r}
}
func (r *Resolver) Query() graph.QueryResolver {
	return &queryResolver{r}
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (model.Todo, error) {
	panic("not implemented")
}
func (r *mutationResolver) CreateOrder(ctx context.Context, input *model.NewOrder) (int, error) {
	panic("not implemented")
}
func (r *mutationResolver) UpdateOrderStatus(ctx context.Context, input *model.Status) (bool, error) {
	panic("not implemented")
}
func (r *mutationResolver) SendSms(ctx context.Context, Input *model.SMS) (bool, error) {
	panic("not implemented")
}
func (r *mutationResolver) ValidationSms(ctx context.Context, Input *model.SMS) (model.AnswerSMS, error) {
	panic("not implemented")
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) GetOrder(ctx context.Context, OrderId int) (*model.Order, error) {
	panic("not implemented")
}
func (r *queryResolver) GetOrders(ctx context.Context) ([]*model.Order, error) {
	panic("not implemented")
}
func (r *queryResolver) Credit(ctx context.Context, id *int) (model.Credit, error) {
	panic("not implemented")
}
func (r *queryResolver) GetCurrencies(ctx context.Context) ([]model.Currency, error) {
	panic("not implemented")
}
func (r *queryResolver) GetCurrency(ctx context.Context, id int) ([]model.Currency, error) {
	panic("not implemented")
}
