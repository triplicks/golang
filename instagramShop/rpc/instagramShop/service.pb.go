// Code generated by protoc-gen-go. DO NOT EDIT.
// source: gitlab.com/unie.kz/nova/instagramShop/rpc/instagramShop/service.proto

package instagramShop

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type NewCategoryRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Partner              int64    `protobuf:"varint,2,opt,name=partner,proto3" json:"partner,omitempty"`
	Tags                 []*Tag   `protobuf:"bytes,3,rep,name=tags,proto3" json:"tags,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *NewCategoryRequest) Reset()         { *m = NewCategoryRequest{} }
func (m *NewCategoryRequest) String() string { return proto.CompactTextString(m) }
func (*NewCategoryRequest) ProtoMessage()    {}
func (*NewCategoryRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{0}
}
func (m *NewCategoryRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewCategoryRequest.Unmarshal(m, b)
}
func (m *NewCategoryRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewCategoryRequest.Marshal(b, m, deterministic)
}
func (dst *NewCategoryRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewCategoryRequest.Merge(dst, src)
}
func (m *NewCategoryRequest) XXX_Size() int {
	return xxx_messageInfo_NewCategoryRequest.Size(m)
}
func (m *NewCategoryRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_NewCategoryRequest.DiscardUnknown(m)
}

var xxx_messageInfo_NewCategoryRequest proto.InternalMessageInfo

func (m *NewCategoryRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *NewCategoryRequest) GetPartner() int64 {
	if m != nil {
		return m.Partner
	}
	return 0
}

func (m *NewCategoryRequest) GetTags() []*Tag {
	if m != nil {
		return m.Tags
	}
	return nil
}

type NewCategoryResponse struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *NewCategoryResponse) Reset()         { *m = NewCategoryResponse{} }
func (m *NewCategoryResponse) String() string { return proto.CompactTextString(m) }
func (*NewCategoryResponse) ProtoMessage()    {}
func (*NewCategoryResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{1}
}
func (m *NewCategoryResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewCategoryResponse.Unmarshal(m, b)
}
func (m *NewCategoryResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewCategoryResponse.Marshal(b, m, deterministic)
}
func (dst *NewCategoryResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewCategoryResponse.Merge(dst, src)
}
func (m *NewCategoryResponse) XXX_Size() int {
	return xxx_messageInfo_NewCategoryResponse.Size(m)
}
func (m *NewCategoryResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_NewCategoryResponse.DiscardUnknown(m)
}

var xxx_messageInfo_NewCategoryResponse proto.InternalMessageInfo

func (m *NewCategoryResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

type CategoryRequest struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Partner              int64    `protobuf:"varint,3,opt,name=partner,proto3" json:"partner,omitempty"`
	Tags                 []*Tag   `protobuf:"bytes,4,rep,name=tags,proto3" json:"tags,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CategoryRequest) Reset()         { *m = CategoryRequest{} }
func (m *CategoryRequest) String() string { return proto.CompactTextString(m) }
func (*CategoryRequest) ProtoMessage()    {}
func (*CategoryRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{2}
}
func (m *CategoryRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CategoryRequest.Unmarshal(m, b)
}
func (m *CategoryRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CategoryRequest.Marshal(b, m, deterministic)
}
func (dst *CategoryRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CategoryRequest.Merge(dst, src)
}
func (m *CategoryRequest) XXX_Size() int {
	return xxx_messageInfo_CategoryRequest.Size(m)
}
func (m *CategoryRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CategoryRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CategoryRequest proto.InternalMessageInfo

func (m *CategoryRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *CategoryRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CategoryRequest) GetPartner() int64 {
	if m != nil {
		return m.Partner
	}
	return 0
}

func (m *CategoryRequest) GetTags() []*Tag {
	if m != nil {
		return m.Tags
	}
	return nil
}

type NewProductsRequest struct {
	Products             []*Product `protobuf:"bytes,2,rep,name=products,proto3" json:"products,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *NewProductsRequest) Reset()         { *m = NewProductsRequest{} }
func (m *NewProductsRequest) String() string { return proto.CompactTextString(m) }
func (*NewProductsRequest) ProtoMessage()    {}
func (*NewProductsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{3}
}
func (m *NewProductsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewProductsRequest.Unmarshal(m, b)
}
func (m *NewProductsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewProductsRequest.Marshal(b, m, deterministic)
}
func (dst *NewProductsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewProductsRequest.Merge(dst, src)
}
func (m *NewProductsRequest) XXX_Size() int {
	return xxx_messageInfo_NewProductsRequest.Size(m)
}
func (m *NewProductsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_NewProductsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_NewProductsRequest proto.InternalMessageInfo

func (m *NewProductsRequest) GetProducts() []*Product {
	if m != nil {
		return m.Products
	}
	return nil
}

type NewProductResponse struct {
	Status               bool     `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *NewProductResponse) Reset()         { *m = NewProductResponse{} }
func (m *NewProductResponse) String() string { return proto.CompactTextString(m) }
func (*NewProductResponse) ProtoMessage()    {}
func (*NewProductResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{4}
}
func (m *NewProductResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewProductResponse.Unmarshal(m, b)
}
func (m *NewProductResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewProductResponse.Marshal(b, m, deterministic)
}
func (dst *NewProductResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewProductResponse.Merge(dst, src)
}
func (m *NewProductResponse) XXX_Size() int {
	return xxx_messageInfo_NewProductResponse.Size(m)
}
func (m *NewProductResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_NewProductResponse.DiscardUnknown(m)
}

var xxx_messageInfo_NewProductResponse proto.InternalMessageInfo

func (m *NewProductResponse) GetStatus() bool {
	if m != nil {
		return m.Status
	}
	return false
}

type StatusResponse struct {
	Status               bool     `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StatusResponse) Reset()         { *m = StatusResponse{} }
func (m *StatusResponse) String() string { return proto.CompactTextString(m) }
func (*StatusResponse) ProtoMessage()    {}
func (*StatusResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{5}
}
func (m *StatusResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StatusResponse.Unmarshal(m, b)
}
func (m *StatusResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StatusResponse.Marshal(b, m, deterministic)
}
func (dst *StatusResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StatusResponse.Merge(dst, src)
}
func (m *StatusResponse) XXX_Size() int {
	return xxx_messageInfo_StatusResponse.Size(m)
}
func (m *StatusResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_StatusResponse.DiscardUnknown(m)
}

var xxx_messageInfo_StatusResponse proto.InternalMessageInfo

func (m *StatusResponse) GetStatus() bool {
	if m != nil {
		return m.Status
	}
	return false
}

type TagsResponse struct {
	Tags                 []*Tag   `protobuf:"bytes,1,rep,name=tags,proto3" json:"tags,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TagsResponse) Reset()         { *m = TagsResponse{} }
func (m *TagsResponse) String() string { return proto.CompactTextString(m) }
func (*TagsResponse) ProtoMessage()    {}
func (*TagsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{6}
}
func (m *TagsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TagsResponse.Unmarshal(m, b)
}
func (m *TagsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TagsResponse.Marshal(b, m, deterministic)
}
func (dst *TagsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TagsResponse.Merge(dst, src)
}
func (m *TagsResponse) XXX_Size() int {
	return xxx_messageInfo_TagsResponse.Size(m)
}
func (m *TagsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_TagsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_TagsResponse proto.InternalMessageInfo

func (m *TagsResponse) GetTags() []*Tag {
	if m != nil {
		return m.Tags
	}
	return nil
}

type IdRequest struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Partner              int64    `protobuf:"varint,2,opt,name=partner,proto3" json:"partner,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *IdRequest) Reset()         { *m = IdRequest{} }
func (m *IdRequest) String() string { return proto.CompactTextString(m) }
func (*IdRequest) ProtoMessage()    {}
func (*IdRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{7}
}
func (m *IdRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_IdRequest.Unmarshal(m, b)
}
func (m *IdRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_IdRequest.Marshal(b, m, deterministic)
}
func (dst *IdRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_IdRequest.Merge(dst, src)
}
func (m *IdRequest) XXX_Size() int {
	return xxx_messageInfo_IdRequest.Size(m)
}
func (m *IdRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_IdRequest.DiscardUnknown(m)
}

var xxx_messageInfo_IdRequest proto.InternalMessageInfo

func (m *IdRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *IdRequest) GetPartner() int64 {
	if m != nil {
		return m.Partner
	}
	return 0
}

type EmptyRequest struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EmptyRequest) Reset()         { *m = EmptyRequest{} }
func (m *EmptyRequest) String() string { return proto.CompactTextString(m) }
func (*EmptyRequest) ProtoMessage()    {}
func (*EmptyRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{8}
}
func (m *EmptyRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmptyRequest.Unmarshal(m, b)
}
func (m *EmptyRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmptyRequest.Marshal(b, m, deterministic)
}
func (dst *EmptyRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmptyRequest.Merge(dst, src)
}
func (m *EmptyRequest) XXX_Size() int {
	return xxx_messageInfo_EmptyRequest.Size(m)
}
func (m *EmptyRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_EmptyRequest.DiscardUnknown(m)
}

var xxx_messageInfo_EmptyRequest proto.InternalMessageInfo

func (m *EmptyRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

type CategoriesResponse struct {
	Categories           []*CategoryResponse `protobuf:"bytes,1,rep,name=categories,proto3" json:"categories,omitempty"`
	XXX_NoUnkeyedLiteral struct{}            `json:"-"`
	XXX_unrecognized     []byte              `json:"-"`
	XXX_sizecache        int32               `json:"-"`
}

func (m *CategoriesResponse) Reset()         { *m = CategoriesResponse{} }
func (m *CategoriesResponse) String() string { return proto.CompactTextString(m) }
func (*CategoriesResponse) ProtoMessage()    {}
func (*CategoriesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{9}
}
func (m *CategoriesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CategoriesResponse.Unmarshal(m, b)
}
func (m *CategoriesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CategoriesResponse.Marshal(b, m, deterministic)
}
func (dst *CategoriesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CategoriesResponse.Merge(dst, src)
}
func (m *CategoriesResponse) XXX_Size() int {
	return xxx_messageInfo_CategoriesResponse.Size(m)
}
func (m *CategoriesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CategoriesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CategoriesResponse proto.InternalMessageInfo

func (m *CategoriesResponse) GetCategories() []*CategoryResponse {
	if m != nil {
		return m.Categories
	}
	return nil
}

type CategoryResponse struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Tags                 []*Tag   `protobuf:"bytes,3,rep,name=tags,proto3" json:"tags,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CategoryResponse) Reset()         { *m = CategoryResponse{} }
func (m *CategoryResponse) String() string { return proto.CompactTextString(m) }
func (*CategoryResponse) ProtoMessage()    {}
func (*CategoryResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{10}
}
func (m *CategoryResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CategoryResponse.Unmarshal(m, b)
}
func (m *CategoryResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CategoryResponse.Marshal(b, m, deterministic)
}
func (dst *CategoryResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CategoryResponse.Merge(dst, src)
}
func (m *CategoryResponse) XXX_Size() int {
	return xxx_messageInfo_CategoryResponse.Size(m)
}
func (m *CategoryResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CategoryResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CategoryResponse proto.InternalMessageInfo

func (m *CategoryResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *CategoryResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CategoryResponse) GetTags() []*Tag {
	if m != nil {
		return m.Tags
	}
	return nil
}

type ProductsResponse struct {
	Products             []*ProductResponse `protobuf:"bytes,1,rep,name=products,proto3" json:"products,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *ProductsResponse) Reset()         { *m = ProductsResponse{} }
func (m *ProductsResponse) String() string { return proto.CompactTextString(m) }
func (*ProductsResponse) ProtoMessage()    {}
func (*ProductsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{11}
}
func (m *ProductsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProductsResponse.Unmarshal(m, b)
}
func (m *ProductsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProductsResponse.Marshal(b, m, deterministic)
}
func (dst *ProductsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProductsResponse.Merge(dst, src)
}
func (m *ProductsResponse) XXX_Size() int {
	return xxx_messageInfo_ProductsResponse.Size(m)
}
func (m *ProductsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ProductsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ProductsResponse proto.InternalMessageInfo

func (m *ProductsResponse) GetProducts() []*ProductResponse {
	if m != nil {
		return m.Products
	}
	return nil
}

type ProductResponse struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Description          string   `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	Price                int64    `protobuf:"varint,4,opt,name=price,proto3" json:"price,omitempty"`
	CategoryId           int64    `protobuf:"varint,6,opt,name=categoryId,proto3" json:"categoryId,omitempty"`
	CategoryName         string   `protobuf:"bytes,7,opt,name=categoryName,proto3" json:"categoryName,omitempty"`
	Tags                 []*Tag   `protobuf:"bytes,5,rep,name=tags,proto3" json:"tags,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ProductResponse) Reset()         { *m = ProductResponse{} }
func (m *ProductResponse) String() string { return proto.CompactTextString(m) }
func (*ProductResponse) ProtoMessage()    {}
func (*ProductResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{12}
}
func (m *ProductResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProductResponse.Unmarshal(m, b)
}
func (m *ProductResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProductResponse.Marshal(b, m, deterministic)
}
func (dst *ProductResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProductResponse.Merge(dst, src)
}
func (m *ProductResponse) XXX_Size() int {
	return xxx_messageInfo_ProductResponse.Size(m)
}
func (m *ProductResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ProductResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ProductResponse proto.InternalMessageInfo

func (m *ProductResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ProductResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *ProductResponse) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *ProductResponse) GetPrice() int64 {
	if m != nil {
		return m.Price
	}
	return 0
}

func (m *ProductResponse) GetCategoryId() int64 {
	if m != nil {
		return m.CategoryId
	}
	return 0
}

func (m *ProductResponse) GetCategoryName() string {
	if m != nil {
		return m.CategoryName
	}
	return ""
}

func (m *ProductResponse) GetTags() []*Tag {
	if m != nil {
		return m.Tags
	}
	return nil
}

// Single messages
type Tag struct {
	Id                   int64      `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Value                string     `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	Products             []*Product `protobuf:"bytes,3,rep,name=products,proto3" json:"products,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *Tag) Reset()         { *m = Tag{} }
func (m *Tag) String() string { return proto.CompactTextString(m) }
func (*Tag) ProtoMessage()    {}
func (*Tag) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{13}
}
func (m *Tag) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Tag.Unmarshal(m, b)
}
func (m *Tag) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Tag.Marshal(b, m, deterministic)
}
func (dst *Tag) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Tag.Merge(dst, src)
}
func (m *Tag) XXX_Size() int {
	return xxx_messageInfo_Tag.Size(m)
}
func (m *Tag) XXX_DiscardUnknown() {
	xxx_messageInfo_Tag.DiscardUnknown(m)
}

var xxx_messageInfo_Tag proto.InternalMessageInfo

func (m *Tag) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Tag) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

func (m *Tag) GetProducts() []*Product {
	if m != nil {
		return m.Products
	}
	return nil
}

type Product struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Description          string   `protobuf:"bytes,2,opt,name=description,proto3" json:"description,omitempty"`
	Price                int64    `protobuf:"varint,3,opt,name=price,proto3" json:"price,omitempty"`
	Url                  string   `protobuf:"bytes,4,opt,name=url,proto3" json:"url,omitempty"`
	InstagramId          string   `protobuf:"bytes,5,opt,name=instagramId,proto3" json:"instagramId,omitempty"`
	CategoryId           int64    `protobuf:"varint,6,opt,name=categoryId,proto3" json:"categoryId,omitempty"`
	CategoryName         string   `protobuf:"bytes,7,opt,name=categoryName,proto3" json:"categoryName,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Product) Reset()         { *m = Product{} }
func (m *Product) String() string { return proto.CompactTextString(m) }
func (*Product) ProtoMessage()    {}
func (*Product) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_74da8c6629fd7501, []int{14}
}
func (m *Product) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Product.Unmarshal(m, b)
}
func (m *Product) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Product.Marshal(b, m, deterministic)
}
func (dst *Product) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Product.Merge(dst, src)
}
func (m *Product) XXX_Size() int {
	return xxx_messageInfo_Product.Size(m)
}
func (m *Product) XXX_DiscardUnknown() {
	xxx_messageInfo_Product.DiscardUnknown(m)
}

var xxx_messageInfo_Product proto.InternalMessageInfo

func (m *Product) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Product) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *Product) GetPrice() int64 {
	if m != nil {
		return m.Price
	}
	return 0
}

func (m *Product) GetUrl() string {
	if m != nil {
		return m.Url
	}
	return ""
}

func (m *Product) GetInstagramId() string {
	if m != nil {
		return m.InstagramId
	}
	return ""
}

func (m *Product) GetCategoryId() int64 {
	if m != nil {
		return m.CategoryId
	}
	return 0
}

func (m *Product) GetCategoryName() string {
	if m != nil {
		return m.CategoryName
	}
	return ""
}

func init() {
	proto.RegisterType((*NewCategoryRequest)(nil), "instagramShop.NewCategoryRequest")
	proto.RegisterType((*NewCategoryResponse)(nil), "instagramShop.NewCategoryResponse")
	proto.RegisterType((*CategoryRequest)(nil), "instagramShop.CategoryRequest")
	proto.RegisterType((*NewProductsRequest)(nil), "instagramShop.NewProductsRequest")
	proto.RegisterType((*NewProductResponse)(nil), "instagramShop.NewProductResponse")
	proto.RegisterType((*StatusResponse)(nil), "instagramShop.StatusResponse")
	proto.RegisterType((*TagsResponse)(nil), "instagramShop.TagsResponse")
	proto.RegisterType((*IdRequest)(nil), "instagramShop.IdRequest")
	proto.RegisterType((*EmptyRequest)(nil), "instagramShop.EmptyRequest")
	proto.RegisterType((*CategoriesResponse)(nil), "instagramShop.CategoriesResponse")
	proto.RegisterType((*CategoryResponse)(nil), "instagramShop.CategoryResponse")
	proto.RegisterType((*ProductsResponse)(nil), "instagramShop.ProductsResponse")
	proto.RegisterType((*ProductResponse)(nil), "instagramShop.ProductResponse")
	proto.RegisterType((*Tag)(nil), "instagramShop.Tag")
	proto.RegisterType((*Product)(nil), "instagramShop.Product")
}

func init() {
	proto.RegisterFile("gitlab.com/unie.kz/nova/instagramShop/rpc/instagramShop/service.proto", fileDescriptor_service_74da8c6629fd7501)
}

var fileDescriptor_service_74da8c6629fd7501 = []byte{
	// 668 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x56, 0xdd, 0x6e, 0xd3, 0x30,
	0x14, 0x56, 0x92, 0xb6, 0x5b, 0xcf, 0xb6, 0x6e, 0x32, 0xd5, 0x14, 0x6d, 0xa2, 0x2b, 0x91, 0x40,
	0xbd, 0x40, 0xad, 0x34, 0x04, 0x17, 0xdc, 0x20, 0xed, 0x07, 0x56, 0x21, 0x0d, 0xc8, 0x36, 0x90,
	0xb8, 0x00, 0x79, 0x89, 0x15, 0x02, 0x6d, 0x12, 0x1c, 0xa7, 0xd3, 0x78, 0x26, 0x5e, 0x85, 0x07,
	0xe0, 0x6d, 0x50, 0x9d, 0x3f, 0xc7, 0xcd, 0x4f, 0x2f, 0xb8, 0x8b, 0x8f, 0x8f, 0x3f, 0x7f, 0xdf,
	0x39, 0xc7, 0x5f, 0x0b, 0xe7, 0x8e, 0xcb, 0x66, 0xf8, 0x76, 0x6c, 0xf9, 0xf3, 0x49, 0xe4, 0xb9,
	0x64, 0xfc, 0xe3, 0xd7, 0xc4, 0xf3, 0x17, 0x78, 0xe2, 0x7a, 0x21, 0xc3, 0x0e, 0xc5, 0xf3, 0xab,
	0x6f, 0x7e, 0x30, 0xa1, 0x81, 0x25, 0x45, 0x42, 0x42, 0x17, 0xae, 0x45, 0xc6, 0x01, 0xf5, 0x99,
	0x8f, 0x76, 0x0a, 0x9b, 0xc6, 0x77, 0x40, 0x97, 0xe4, 0xee, 0x14, 0x33, 0xe2, 0xf8, 0xf4, 0xde,
	0x24, 0x3f, 0x23, 0x12, 0x32, 0x84, 0xa0, 0xe5, 0xe1, 0x39, 0xd1, 0x95, 0xa1, 0x32, 0xea, 0x9a,
	0xfc, 0x1b, 0xe9, 0xb0, 0x11, 0x60, 0xca, 0x3c, 0x42, 0x75, 0x75, 0xa8, 0x8c, 0x34, 0x33, 0x5d,
	0xa2, 0x27, 0xd0, 0x62, 0xd8, 0x09, 0x75, 0x6d, 0xa8, 0x8d, 0xb6, 0x8e, 0xd1, 0xb8, 0x70, 0xc3,
	0xf8, 0x1a, 0x3b, 0x26, 0xdf, 0x37, 0x1e, 0xc3, 0x83, 0xc2, 0x5d, 0x61, 0xe0, 0x7b, 0x21, 0x41,
	0x3d, 0x50, 0x5d, 0x9b, 0x5f, 0xa5, 0x99, 0xaa, 0x6b, 0x1b, 0x77, 0xb0, 0x2b, 0xf3, 0x91, 0x52,
	0x32, 0x7e, 0x6a, 0x39, 0x3f, 0xad, 0x9c, 0x5f, 0xab, 0x81, 0xdf, 0x05, 0xaf, 0xc5, 0x7b, 0xea,
	0xdb, 0x91, 0xc5, 0xc2, 0xf4, 0xee, 0x63, 0xd8, 0x0c, 0x92, 0x90, 0xae, 0x72, 0x84, 0x7d, 0x09,
	0x21, 0x39, 0x61, 0x66, 0x79, 0xc6, 0x53, 0x11, 0x29, 0x13, 0xba, 0x0f, 0x9d, 0x90, 0x61, 0x16,
	0x85, 0x5c, 0xc9, 0xa6, 0x99, 0xac, 0x8c, 0x11, 0xf4, 0xae, 0xf8, 0x57, 0x63, 0xe6, 0x0b, 0xd8,
	0xbe, 0xc6, 0x4e, 0x9e, 0x97, 0x2a, 0x53, 0x1a, 0x94, 0x3d, 0x87, 0xee, 0xd4, 0xae, 0x2a, 0x66,
	0x65, 0x63, 0x8d, 0x01, 0x6c, 0x9f, 0xcf, 0x03, 0x56, 0xd5, 0x06, 0xe3, 0x06, 0x50, 0xd2, 0x29,
	0x97, 0xe4, 0xa4, 0x5e, 0x01, 0x58, 0x59, 0x34, 0xa1, 0x76, 0x24, 0x51, 0x93, 0x87, 0xc0, 0x14,
	0x8e, 0x18, 0x5f, 0x60, 0xaf, 0x69, 0x48, 0x4a, 0x27, 0x60, 0xdd, 0x39, 0xbc, 0x84, 0xbd, 0xbc,
	0xc9, 0x09, 0xfe, 0x4b, 0xa1, 0xcb, 0x31, 0xe5, 0x41, 0x45, 0x97, 0x53, 0xc6, 0x79, 0xb7, 0xff,
	0x2a, 0xb0, 0x2b, 0xf7, 0x7a, 0x1d, 0xbe, 0x43, 0xd8, 0xb2, 0x49, 0x68, 0x51, 0x37, 0x60, 0xae,
	0xef, 0xf1, 0xa9, 0xed, 0x9a, 0x62, 0x08, 0xf5, 0xa1, 0x1d, 0x50, 0xd7, 0x22, 0x7a, 0x8b, 0x03,
	0xc5, 0x0b, 0x34, 0xc8, 0x0a, 0x7c, 0x3f, 0xb5, 0xf5, 0x0e, 0xdf, 0x12, 0x22, 0xc8, 0x80, 0xed,
	0x74, 0x75, 0xb9, 0xbc, 0x73, 0x83, 0x03, 0x17, 0x62, 0x59, 0xad, 0xda, 0x0d, 0xb5, 0xfa, 0x0a,
	0xda, 0x35, 0x76, 0x56, 0xe4, 0xf4, 0xa1, 0xbd, 0xc0, 0xb3, 0x28, 0xd5, 0x13, 0x2f, 0x0a, 0x4f,
	0x45, 0x5b, 0xf3, 0xa9, 0xfc, 0x51, 0x60, 0x23, 0x89, 0x96, 0xda, 0x8e, 0x54, 0x24, 0xb5, 0xa6,
	0x48, 0x9a, 0x58, 0xa4, 0x3d, 0xd0, 0x22, 0x3a, 0xe3, 0x85, 0xeb, 0x9a, 0xcb, 0xcf, 0x25, 0x52,
	0x46, 0x66, 0x6a, 0xeb, 0xed, 0x18, 0x49, 0x08, 0xfd, 0x8f, 0xc2, 0x1e, 0xff, 0xee, 0x40, 0x7f,
	0x2a, 0x6a, 0xbe, 0x8a, 0xed, 0x17, 0x7d, 0x82, 0xde, 0x29, 0x25, 0x98, 0x91, 0x74, 0xb6, 0xd1,
	0x23, 0xa9, 0x38, 0xab, 0x46, 0x7c, 0x60, 0xd4, 0xa5, 0x24, 0xa3, 0xf6, 0x0e, 0x7a, 0x37, 0x81,
	0x2d, 0x02, 0x0f, 0x2a, 0x5f, 0x5b, 0x8c, 0xfa, 0x50, 0xda, 0x97, 0xdc, 0x67, 0x0a, 0xbd, 0x33,
	0x32, 0x23, 0x02, 0xa0, 0x2e, 0x1d, 0xc8, 0xcc, 0xa4, 0x09, 0xea, 0x63, 0x2a, 0x3a, 0x7d, 0x70,
	0x65, 0xa2, 0x25, 0xc7, 0x3d, 0xa8, 0x4e, 0x11, 0x34, 0xef, 0xbc, 0x21, 0x2c, 0x37, 0x1f, 0x74,
	0x28, 0x9d, 0x11, 0x7d, 0x6b, 0x05, 0xb0, 0xc4, 0xb4, 0x2e, 0x60, 0x2b, 0x07, 0xac, 0x13, 0xdc,
	0xe4, 0x64, 0xe8, 0x2d, 0x47, 0xca, 0xf4, 0xd6, 0x12, 0x3b, 0x2a, 0x7f, 0x1e, 0x39, 0xad, 0x0f,
	0xd0, 0xcf, 0xc1, 0x4e, 0xf3, 0x49, 0x5c, 0x9f, 0xdf, 0x0a, 0xe4, 0x6b, 0x80, 0x1c, 0xb2, 0x06,
	0xa8, 0xc1, 0xff, 0xd0, 0x19, 0xd7, 0xb9, 0xfc, 0x39, 0x3a, 0xa9, 0x67, 0x74, 0xb8, 0x6a, 0x2e,
	0x19, 0x9b, 0x93, 0xdd, 0xcf, 0xc5, 0x3f, 0x24, 0xb7, 0x1d, 0xfe, 0x37, 0xe5, 0xd9, 0xbf, 0x00,
	0x00, 0x00, 0xff, 0xff, 0x4e, 0xe1, 0x54, 0x70, 0xef, 0x08, 0x00, 0x00,
}
