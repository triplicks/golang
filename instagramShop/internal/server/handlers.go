package instagramshopserver;

import(
	"fmt";
	"context";
	"database/sql";
	"encoding/json";
	_ "github.com/lib/pq";
	pb "gitlab.com/unie.kz/nova/instagramShop/rpc/instagramShop";
)

func New() *server {
	return new(server);
}

type server struct {};


var connect string = "postgres://fzihfpbinanlxp:5cc22774fa8103ed1d5b6d5d0655d96cf84b6aaa6ce1e204e23b7d150fe3304f@ec2-23-23-80-20.compute-1.amazonaws.com:5432/dmev6hv777ni";



// Mutations

func (s *server) CreateCategory(ctx context.Context, request *pb.NewCategoryRequest) (response *pb.NewCategoryResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	tx, err := db.Begin();
	if err != nil { return;}
	response = &pb.NewCategoryResponse{};
	err = tx.QueryRow("INSERT INTO category(name, _partner) VALUES ($1, $2) returning id", request.Name, request.Partner).Scan(&response.Id);
	if err != nil { 
		tx.Rollback();
		return;
	}
	count := len(request.Tags);
	for i := 0; i < count; i++ {
		_, err = tx.Exec("INSERT INTO tags(_category, value) VALUES ($1, $2)", response.Id, request.Tags[i].Value);
		if err != nil { 
			tx.Rollback();
			return;
		}
	}
	tx.Commit();
	return;
}
func (s *server) UpdateCategory(ctx context.Context, request *pb.CategoryRequest) (response *pb.StatusResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	tx, err := db.Begin();
	if err != nil { return;}
	var buffer sql.NullInt64
	err = tx.QueryRow(
				`UPDATE category SET name = $1 WHERE id = $2 and _partner = $3 returning 
					(SELECT id FROM category WHERE _partner = $3)`, request.Name, request.Id, request.Partner).Scan(&buffer);
	if err != nil { 
		fmt.Println("tut");
		tx.Rollback();
		return;
	}
	if buffer.Valid != true {
		tx.Rollback();
		return;
	}
	// if buffer.
	_, err = tx.Exec("DELETE FROM tags WHERE _category = $1", request.Id);
	if err != nil { 
		tx.Rollback();
		return;
	}
	count := len(request.Tags);
	for i := 0; i < count; i++ {
		var tagId int64;
		err = tx.QueryRow("INSERT INTO tags(value,_category) VALUES ($1, $2) returning id",request.Tags[i].Value,request.Id).Scan(&tagId);
		if err != nil {
			tx.Rollback();
			return;
		}
	}
	tx.Commit();		
	response = &pb.StatusResponse{Status:true};
	return;
}
func (s *server) DeleteCategory(ctx context.Context, request *pb.IdRequest) (response *pb.StatusResponse, err error) {
	db, err := sql.Open("postgres", connect); 
	if err != nil { return;}
	response = &pb.StatusResponse{Status:false};
	_, err = db.Query("DELETE FROM category WHERE id = $1 and _partner = $2", request.Id, request.Partner);
	if err != nil { return;}
	response.Status = true;
	return;
} 
func (s *server) CreateProducts(ctx context.Context, request *pb.NewProductsRequest) (response *pb.NewProductResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	count := len(request.Products);
	for i := 0; i < count; i++ {
		_,test:= db.Query(
				`INSERT INTO products(_tags, name, description, price, _instagram, url) VALUES ($1, $2, $3, $4, $5, $6)`, 
				request.Products[i].CategoryId,
				request.Products[i].Name,
				request.Products[i].Description,
				request.Products[i].Price,
				request.Products[i].InstagramId,
				request.Products[i].Url,
			);
		fmt.Println(test);
	}
	response = &pb.NewProductResponse{Status:true};
	return;
}




// Queries

func (s *server) GetCategories(ctx context.Context, request *pb.EmptyRequest) (response *pb.CategoriesResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	response = &pb.CategoriesResponse{};
	rows, err := db.Query(
							`
								SELECT 
									c.id,c.name, 
									array_to_json(array_remove(array_agg(distinct ta),NULL))
								FROM category c
								LEFT JOIN tags ta
								ON ta._category = c.id
								WHERE c._partner = $1
								GROUP BY c.id
							`,request.Id,
						);
	if err != nil { return;}
	for rows.Next() {
		category := &pb.CategoryResponse{};
		jsonBuffer := "";
		err = rows.Scan(&category.Id, &category.Name, &jsonBuffer);
		if err != nil { return;}
		err = json.Unmarshal([]byte(jsonBuffer), &category.Tags);
		if err != nil { return;}
		response.Categories = append(response.Categories, category);
	}
	return;
}
func (s *server) GetCategory(ctx context.Context, request *pb.IdRequest) (response *pb.CategoryResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	response = &pb.CategoryResponse{};
	rows, err := db.Query(
						`
							SELECT 
								c.id, c.name, 
								array_to_json(array_remove(array_agg(distinct ta), NULL))
							FROM category c 
							LEFT JOIN tags ta
							ON ta._category = c.id
							WHERE c.id = $1 AND c._partner = $2
							GROUP BY c.id
						`,request.Id,request.Partner,
					)
	if err != nil { return;}
	for rows.Next() {
		jsonBuffer := "";
		err = rows.Scan(&response.Id, &response.Name, &jsonBuffer);
		if err != nil { return;}
		err = json.Unmarshal([]byte(jsonBuffer), &response.Tags);
		if err != nil { return;}
	}
	return;
}
func (s *server) GetProducts(ctx context.Context, request *pb.EmptyRequest) (response *pb.ProductsResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	response = &pb.ProductsResponse{};
	rows, err := db.Query(
						`
							SELECT
								p.id,p.name,
								p.description,p.price,
								c.id, c.name
							FROM products p
							LEFT JOIN tags ta
							ON ta.id = p._tags
							LEFT JOIN category c
							ON c.id = ta._category
							WHERE c._partner = $1
						`,request.Id,
					);
	if err != nil { return;}
	for rows.Next() {
		product := &pb.ProductResponse{};
		rows.Scan(&product.Id,&product.Name, &product.Description, &product.Price, &product.CategoryId, &product.CategoryName);
		response.Products = append(response.Products, product);
	}
	return;
}
func (s *server) GetProductCategoryId(ctx context.Context, request *pb.IdRequest) (response *pb.ProductsResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	response = &pb.ProductsResponse{};
	rows, err := db.Query(
						`
							SELECT
								p.id,p.name,
								p.description,p.price,
								c.id, c.name
							FROM products p 
							LEFT JOIN tags ta
							ON ta.id = p._tags
							LEFT JOIN category c
							ON c.id = ta._category
							WHERE c.id = $1 AND c._partner = $2
						`, request.Id, request.Partner,
					);
	if err != nil { return;}
	for rows.Next() {
		product := &pb.ProductResponse{};
		err = rows.Scan(&product.Id, &product.Name, &product.Description, &product.Price, &product.CategoryId, &product.CategoryName);
		if err != nil { return;}
		response.Products = append(response.Products, product);
	}
	return;
}
func (s *server) GetProduct(ctx context.Context, request *pb.IdRequest) (response *pb.ProductResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
	response = &pb.ProductResponse{};
	rows, err := db.Query(
						`
							SELECT
								p.id,p.name,
								p.description, p.price,
								c.id, c.name
							FROM products p 
							LEFT JOIN tags ta
							ON ta.id = p._tags
							LEFT JOIN category c
							ON c.id = ta._category
							WHERE p.id = $1 AND c._partner = $2
						`, request.Id,request.Partner,
					);
	for rows.Next() {
		err = rows.Scan(&response.Id, &response.Name, &response.Description, &response.Price, &response.CategoryId, &response.CategoryName);
		if err != nil { return;}
	}
	return;
}
func (s *server) GetTagsById(ctx context.Context, request *pb.IdRequest) (response *pb.TagsResponse, err error) {
	db, err := sql.Open("postgres", connect);
	if err != nil { return;}
		fmt.Println(request);
	rows, err := db.Query(
						`
							SELECT ta.id,ta.value 
							FROM tags ta 
							LEFT JOIN category c
							ON c.id = ta._category 
							WHERE c._partner = $1
						`,request.Id,
					);
	if err != nil { return;}
	response = &pb.TagsResponse{};
	for rows.Next() {
		tag := &pb.Tag{};
		err = rows.Scan(&tag.Id, &tag.Value);
		fmt.Println(tag);
		if err != nil { return;}
		response.Tags = append(response.Tags, tag);
	}
	_, err = db.Query("DELETE FROM products WHERE _tags IN (SELECT ta.id FROM tags ta LEFT JOIN category c ON c.id = ta._category WHERE c._partner = $1)", request.Id);	
	if err != nil { return;}	
	return;
}