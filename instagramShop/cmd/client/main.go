package instagramShop;

import (
	"context";
	"gitlab.com/unie.kz/nova/instagramShop/rpc/instagramShop";
	"net/http";
)

type Client struct{
	service instagramShop.InstagramShopService
}

func NewClient(url string) (*Client, error){
	c := instagramShop.NewInstagramShopServiceProtobufClient(url, &http.Client{});
	return &Client{c}, nil;
}


// Mutations 

func (c *Client) CreateCategory(ctx context.Context, request *instagramShop.NewCategoryRequest) (response *instagramShop.NewCategoryResponse, err error) {
	response, err = c.service.CreateCategory(ctx, request);
	return;
}
func (c *Client) UpdateCategory(ctx context.Context, request *instagramShop.CategoryRequest) (response *instagramShop.StatusResponse, err error) {
	response, err = c.service.UpdateCategory(ctx, request);
	return;
}
func (c *Client) DeleteCategory(ctx context.Context, request *instagramShop.IdRequest) (response *instagramShop.StatusResponse, err error) {
	response, err = c.service.DeleteCategory(ctx, request);
	return;
}
func (c *Client) CreateProducts(ctx context.Context, request *instagramShop.NewProductsRequest) (response *instagramShop.NewProductResponse, err error) {
	response, err = c.service.CreateProducts(ctx, request);
	return;
}



// Queries

func (c *Client) GetCategories(ctx context.Context, request *instagramShop.EmptyRequest) (response *instagramShop.CategoriesResponse, err error) {
	response, err = c.service.GetCategories(ctx, request);
	return;
}
func (c *Client) GetCategory(ctx context.Context, request *instagramShop.IdRequest) (response *instagramShop.CategoryResponse, err error) {
	response, err = c.service.GetCategory(ctx, request);
	return;
}
func (c *Client) GetProducts(ctx context.Context, request *instagramShop.EmptyRequest) (response *instagramShop.ProductsResponse, err error) {
	response, err = c.service.GetProducts(ctx, request);
	return;
}
func (c *Client) GetProductCategoryId(ctx context.Context, request *instagramShop.IdRequest) (response *instagramShop.ProductsResponse, err error) {
	response, err = c.service.GetProductCategoryId(ctx, request);
	return;
}
func (c *Client) GetProduct(ctx context.Context, request *instagramShop.IdRequest) (response *instagramShop.ProductResponse, err error) {
	response, err = c.service.GetProduct(ctx, request);
	return;
}
func (c *Client) GetTagsById(ctx context.Context, request *instagramShop.IdRequest) (response *instagramShop.TagsResponse, err error) {
	response, err = c.service.GetTagsById(ctx, request);
	return;
}