package main

import(
	"fmt"
	"net/http"
	"gitlab.com/unie.kz/nova/instagramShop/internal/server"
	"gitlab.com/unie.kz/nova/instagramShop/rpc/instagramShop"
)

func main() {
	server := instagramshopserver.New();
	twirpHandler := instagramShop.NewInstagramShopServiceServer(server, nil)

	mux := http.NewServeMux();
	mux.Handle(instagramShop.InstagramShopServicePathPrefix, twirpHandler);
	mux.HandleFunc("/", healthz);

	fmt.Println("Server listening 9016 port");
	http.ListenAndServe(":9016", mux);
}

func healthz(w http.ResponseWriter, _ *http.Request){
	w.WriteHeader(http.StatusOK)
}