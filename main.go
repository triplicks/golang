package main

import (
	"context";
	"log";
	"github.com/machinebox/graphql";
)

type test struct {
	ValidationSms struct {
		Status bool
		Key string
	}
};


func main() {
	client := graphql.NewClient("http://localhost:8082/graphql");
	// make a request
	req := graphql.NewRequest(`	
		mutation($test: SMS!) {
			ValidationSms(
				Input:$test
			){
				status
				key
			}
		}
	`);
	req.Var("test",struct{
		Phone string
		Kod string
	}{
		Phone:"32432",
		Kod:"34234324",
	});
	// req.Var("test",struct{
	// 	phone:"23423423"
	// 	})

	ctx := context.Background();

	var res test;

	if err := client.Run(ctx, req, &res); err != nil {
		log.Println(res);	
		log.Fatal(err);
	}
	
}

// http://api-dev.unie.kz/v1?query={getCategory(id: 1){name id subcategories{id name }}}
