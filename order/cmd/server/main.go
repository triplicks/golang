package main

import (
	"net/http"
	"gitlab.com/unie.kz/nova/order/internal/server"
	p "gitlab.com/unie.kz/nova/order/rpc/order"
)

func main() {
	dbURL := os.Getenv("DB_URL")	
	server := orderserver.New(DB_URL)

	twirpHandler := p.NewOrderServiceServer(server, nil)

	mux := http.NewServeMux()
	mux.Handle(p.OrderServicePathPrefix, twirpHandler)
	mux.HandleFunc("/", healthz)

	http.ListenAndServe(":9000", mux)
}

func healthz(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
