package order

import (
	"context"
	"gitlab.com/unie.kz/nova/order/rpc/order"
	"net/http"
)

type Client struct {
	service order.OrderService
}

//NewClient init
func NewClient(url string) (*Client, error) {
	c := order.NewOrderServiceProtobufClient(url, &http.Client{})
	return &Client{c}, nil
}

//GetCredit resolver
func (c *Client) GetOrder(ctx context.Context, request *order.OrderIdReq) (response *order.Order, err error) {
	response, err = c.service.GetOrder(context.Background(), request)
	return
}
func (c *Client) GetOrders(ctx context.Context, request *order.OrderIdReq) (response *order.ListOrder, err error) {
	response, err = c.service.GetOrders(context.Background(), request)
	return
}
func (c *Client) GetOrdersB2b(ctx context.Context, request *order.OrderIdReq) (response *order.ListOrder, err error) {
	response, err = c.service.GetOrdersB2B(context.Background(), request)
	return
}
func (c *Client) GetOrdersProfile(ctx context.Context, request *order.OrderProfileRequest) (response *order.ListOrder, err error) {
	response, err = c.service.GetOrdersProfile(context.Background(), request)
	return
}





func (c *Client) SetOrder(ctx context.Context, request *order.Order) (response *order.StatusRes, err error) {
	response, err = c.service.SetOrder(context.Background(), request)
	return
}
func (c *Client) UpdateOrderStatus(ctx context.Context, request *order.StatusReq) (response *order.StatusRes, err error) {
	response, err = c.service.UpdateOrderStatus(context.Background(), request)
	return
}
func (c *Client) GetOrdersByMaster(ctx context.Context, request *order.OrderMasterReq) (response *order.Books, err error) {
	response, err = c.service.GetOrdersByMaster(context.Background(), request)
	return
}
