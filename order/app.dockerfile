FROM golang:alpine3.7 AS build
RUN apk --no-cache add gcc g++ make ca-certificates
WORKDIR /go/src/gitlab.com/unie.kz/nova/order
COPY order .
RUN go build -o /go/bin/app ./cmd/server/main.go

FROM alpine:3.7
WORKDIR /usr/bin
COPY --from=build /go/bin .
EXPOSE 9000
CMD ["./app"]
