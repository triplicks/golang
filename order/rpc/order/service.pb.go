// Code generated by protoc-gen-go. DO NOT EDIT.
// source: gitlab.com/unie.kz/nova/order/rpc/order/service.proto

package order

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type OrderIdReq struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	UserId               int64    `protobuf:"varint,2,opt,name=userId,proto3" json:"userId,omitempty"`
	PartnerId            int64    `protobuf:"varint,3,opt,name=partnerId,proto3" json:"partnerId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *OrderIdReq) Reset()         { *m = OrderIdReq{} }
func (m *OrderIdReq) String() string { return proto.CompactTextString(m) }
func (*OrderIdReq) ProtoMessage()    {}
func (*OrderIdReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{0}
}
func (m *OrderIdReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_OrderIdReq.Unmarshal(m, b)
}
func (m *OrderIdReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_OrderIdReq.Marshal(b, m, deterministic)
}
func (dst *OrderIdReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_OrderIdReq.Merge(dst, src)
}
func (m *OrderIdReq) XXX_Size() int {
	return xxx_messageInfo_OrderIdReq.Size(m)
}
func (m *OrderIdReq) XXX_DiscardUnknown() {
	xxx_messageInfo_OrderIdReq.DiscardUnknown(m)
}

var xxx_messageInfo_OrderIdReq proto.InternalMessageInfo

func (m *OrderIdReq) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *OrderIdReq) GetUserId() int64 {
	if m != nil {
		return m.UserId
	}
	return 0
}

func (m *OrderIdReq) GetPartnerId() int64 {
	if m != nil {
		return m.PartnerId
	}
	return 0
}

type OrderMasterReq struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Label                string   `protobuf:"bytes,2,opt,name=label,proto3" json:"label,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *OrderMasterReq) Reset()         { *m = OrderMasterReq{} }
func (m *OrderMasterReq) String() string { return proto.CompactTextString(m) }
func (*OrderMasterReq) ProtoMessage()    {}
func (*OrderMasterReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{1}
}
func (m *OrderMasterReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_OrderMasterReq.Unmarshal(m, b)
}
func (m *OrderMasterReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_OrderMasterReq.Marshal(b, m, deterministic)
}
func (dst *OrderMasterReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_OrderMasterReq.Merge(dst, src)
}
func (m *OrderMasterReq) XXX_Size() int {
	return xxx_messageInfo_OrderMasterReq.Size(m)
}
func (m *OrderMasterReq) XXX_DiscardUnknown() {
	xxx_messageInfo_OrderMasterReq.DiscardUnknown(m)
}

var xxx_messageInfo_OrderMasterReq proto.InternalMessageInfo

func (m *OrderMasterReq) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *OrderMasterReq) GetLabel() string {
	if m != nil {
		return m.Label
	}
	return ""
}

type Order struct {
	Id                   int64        `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Status               string       `protobuf:"bytes,2,opt,name=status,proto3" json:"status,omitempty"`
	UserId               int64        `protobuf:"varint,3,opt,name=userId,proto3" json:"userId,omitempty"`
	PartnerId            int64        `protobuf:"varint,4,opt,name=partnerId,proto3" json:"partnerId,omitempty"`
	StatusId             int64        `protobuf:"varint,5,opt,name=statusId,proto3" json:"statusId,omitempty"`
	IdMerchant           int64        `protobuf:"varint,6,opt,name=idMerchant,proto3" json:"idMerchant,omitempty"`
	FirstName            string       `protobuf:"bytes,7,opt,name=firstName,proto3" json:"firstName,omitempty"`
	LastName             string       `protobuf:"bytes,8,opt,name=lastName,proto3" json:"lastName,omitempty"`
	MiddleName           string       `protobuf:"bytes,9,opt,name=middleName,proto3" json:"middleName,omitempty"`
	Email                string       `protobuf:"bytes,10,opt,name=email,proto3" json:"email,omitempty"`
	Phone                string       `protobuf:"bytes,11,opt,name=phone,proto3" json:"phone,omitempty"`
	Address              string       `protobuf:"bytes,12,opt,name=address,proto3" json:"address,omitempty"`
	PostalCode           string       `protobuf:"bytes,13,opt,name=postalCode,proto3" json:"postalCode,omitempty"`
	City                 string       `protobuf:"bytes,14,opt,name=city,proto3" json:"city,omitempty"`
	TransactionId        int64        `protobuf:"varint,15,opt,name=transactionId,proto3" json:"transactionId,omitempty"`
	Comments             string       `protobuf:"bytes,16,opt,name=comments,proto3" json:"comments,omitempty"`
	Iin                  string       `protobuf:"bytes,17,opt,name=iin,proto3" json:"iin,omitempty"`
	Created              string       `protobuf:"bytes,18,opt,name=created,proto3" json:"created,omitempty"`
	OrderItems           []*OrderItem `protobuf:"bytes,19,rep,name=orderItems,proto3" json:"orderItems,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *Order) Reset()         { *m = Order{} }
func (m *Order) String() string { return proto.CompactTextString(m) }
func (*Order) ProtoMessage()    {}
func (*Order) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{2}
}
func (m *Order) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Order.Unmarshal(m, b)
}
func (m *Order) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Order.Marshal(b, m, deterministic)
}
func (dst *Order) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Order.Merge(dst, src)
}
func (m *Order) XXX_Size() int {
	return xxx_messageInfo_Order.Size(m)
}
func (m *Order) XXX_DiscardUnknown() {
	xxx_messageInfo_Order.DiscardUnknown(m)
}

var xxx_messageInfo_Order proto.InternalMessageInfo

func (m *Order) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Order) GetStatus() string {
	if m != nil {
		return m.Status
	}
	return ""
}

func (m *Order) GetUserId() int64 {
	if m != nil {
		return m.UserId
	}
	return 0
}

func (m *Order) GetPartnerId() int64 {
	if m != nil {
		return m.PartnerId
	}
	return 0
}

func (m *Order) GetStatusId() int64 {
	if m != nil {
		return m.StatusId
	}
	return 0
}

func (m *Order) GetIdMerchant() int64 {
	if m != nil {
		return m.IdMerchant
	}
	return 0
}

func (m *Order) GetFirstName() string {
	if m != nil {
		return m.FirstName
	}
	return ""
}

func (m *Order) GetLastName() string {
	if m != nil {
		return m.LastName
	}
	return ""
}

func (m *Order) GetMiddleName() string {
	if m != nil {
		return m.MiddleName
	}
	return ""
}

func (m *Order) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *Order) GetPhone() string {
	if m != nil {
		return m.Phone
	}
	return ""
}

func (m *Order) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *Order) GetPostalCode() string {
	if m != nil {
		return m.PostalCode
	}
	return ""
}

func (m *Order) GetCity() string {
	if m != nil {
		return m.City
	}
	return ""
}

func (m *Order) GetTransactionId() int64 {
	if m != nil {
		return m.TransactionId
	}
	return 0
}

func (m *Order) GetComments() string {
	if m != nil {
		return m.Comments
	}
	return ""
}

func (m *Order) GetIin() string {
	if m != nil {
		return m.Iin
	}
	return ""
}

func (m *Order) GetCreated() string {
	if m != nil {
		return m.Created
	}
	return ""
}

func (m *Order) GetOrderItems() []*OrderItem {
	if m != nil {
		return m.OrderItems
	}
	return nil
}

type OrderProfileRequest struct {
	Day                  int64    `protobuf:"varint,1,opt,name=day,proto3" json:"day,omitempty"`
	Month                int64    `protobuf:"varint,2,opt,name=month,proto3" json:"month,omitempty"`
	Year                 int64    `protobuf:"varint,3,opt,name=year,proto3" json:"year,omitempty"`
	UserId               int64    `protobuf:"varint,4,opt,name=userId,proto3" json:"userId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *OrderProfileRequest) Reset()         { *m = OrderProfileRequest{} }
func (m *OrderProfileRequest) String() string { return proto.CompactTextString(m) }
func (*OrderProfileRequest) ProtoMessage()    {}
func (*OrderProfileRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{3}
}
func (m *OrderProfileRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_OrderProfileRequest.Unmarshal(m, b)
}
func (m *OrderProfileRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_OrderProfileRequest.Marshal(b, m, deterministic)
}
func (dst *OrderProfileRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_OrderProfileRequest.Merge(dst, src)
}
func (m *OrderProfileRequest) XXX_Size() int {
	return xxx_messageInfo_OrderProfileRequest.Size(m)
}
func (m *OrderProfileRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_OrderProfileRequest.DiscardUnknown(m)
}

var xxx_messageInfo_OrderProfileRequest proto.InternalMessageInfo

func (m *OrderProfileRequest) GetDay() int64 {
	if m != nil {
		return m.Day
	}
	return 0
}

func (m *OrderProfileRequest) GetMonth() int64 {
	if m != nil {
		return m.Month
	}
	return 0
}

func (m *OrderProfileRequest) GetYear() int64 {
	if m != nil {
		return m.Year
	}
	return 0
}

func (m *OrderProfileRequest) GetUserId() int64 {
	if m != nil {
		return m.UserId
	}
	return 0
}

type Books struct {
	Books                []*Book  `protobuf:"bytes,1,rep,name=books,proto3" json:"books,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Books) Reset()         { *m = Books{} }
func (m *Books) String() string { return proto.CompactTextString(m) }
func (*Books) ProtoMessage()    {}
func (*Books) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{4}
}
func (m *Books) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Books.Unmarshal(m, b)
}
func (m *Books) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Books.Marshal(b, m, deterministic)
}
func (dst *Books) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Books.Merge(dst, src)
}
func (m *Books) XXX_Size() int {
	return xxx_messageInfo_Books.Size(m)
}
func (m *Books) XXX_DiscardUnknown() {
	xxx_messageInfo_Books.DiscardUnknown(m)
}

var xxx_messageInfo_Books proto.InternalMessageInfo

func (m *Books) GetBooks() []*Book {
	if m != nil {
		return m.Books
	}
	return nil
}

type OrderItem struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	SubjectId            int64    `protobuf:"varint,2,opt,name=subjectId,proto3" json:"subjectId,omitempty"`
	PartnerId            int64    `protobuf:"varint,3,opt,name=partnerId,proto3" json:"partnerId,omitempty"`
	Cost                 int64    `protobuf:"varint,4,opt,name=cost,proto3" json:"cost,omitempty"`
	Quantity             int64    `protobuf:"varint,5,opt,name=quantity,proto3" json:"quantity,omitempty"`
	Label                string   `protobuf:"bytes,6,opt,name=label,proto3" json:"label,omitempty"`
	DeliveryDate         string   `protobuf:"bytes,7,opt,name=deliveryDate,proto3" json:"deliveryDate,omitempty"`
	Type                 int64    `protobuf:"varint,8,opt,name=type,proto3" json:"type,omitempty"`
	TypeDesc             string   `protobuf:"bytes,9,opt,name=typeDesc,proto3" json:"typeDesc,omitempty"`
	Paid                 int64    `protobuf:"varint,10,opt,name=paid,proto3" json:"paid,omitempty"`
	InstalmentPeriod     string   `protobuf:"bytes,11,opt,name=instalmentPeriod,proto3" json:"instalmentPeriod,omitempty"`
	Book                 *Book    `protobuf:"bytes,12,opt,name=book,proto3" json:"book,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *OrderItem) Reset()         { *m = OrderItem{} }
func (m *OrderItem) String() string { return proto.CompactTextString(m) }
func (*OrderItem) ProtoMessage()    {}
func (*OrderItem) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{5}
}
func (m *OrderItem) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_OrderItem.Unmarshal(m, b)
}
func (m *OrderItem) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_OrderItem.Marshal(b, m, deterministic)
}
func (dst *OrderItem) XXX_Merge(src proto.Message) {
	xxx_messageInfo_OrderItem.Merge(dst, src)
}
func (m *OrderItem) XXX_Size() int {
	return xxx_messageInfo_OrderItem.Size(m)
}
func (m *OrderItem) XXX_DiscardUnknown() {
	xxx_messageInfo_OrderItem.DiscardUnknown(m)
}

var xxx_messageInfo_OrderItem proto.InternalMessageInfo

func (m *OrderItem) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *OrderItem) GetSubjectId() int64 {
	if m != nil {
		return m.SubjectId
	}
	return 0
}

func (m *OrderItem) GetPartnerId() int64 {
	if m != nil {
		return m.PartnerId
	}
	return 0
}

func (m *OrderItem) GetCost() int64 {
	if m != nil {
		return m.Cost
	}
	return 0
}

func (m *OrderItem) GetQuantity() int64 {
	if m != nil {
		return m.Quantity
	}
	return 0
}

func (m *OrderItem) GetLabel() string {
	if m != nil {
		return m.Label
	}
	return ""
}

func (m *OrderItem) GetDeliveryDate() string {
	if m != nil {
		return m.DeliveryDate
	}
	return ""
}

func (m *OrderItem) GetType() int64 {
	if m != nil {
		return m.Type
	}
	return 0
}

func (m *OrderItem) GetTypeDesc() string {
	if m != nil {
		return m.TypeDesc
	}
	return ""
}

func (m *OrderItem) GetPaid() int64 {
	if m != nil {
		return m.Paid
	}
	return 0
}

func (m *OrderItem) GetInstalmentPeriod() string {
	if m != nil {
		return m.InstalmentPeriod
	}
	return ""
}

func (m *OrderItem) GetBook() *Book {
	if m != nil {
		return m.Book
	}
	return nil
}

type Book struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Prepaid              int64    `protobuf:"varint,2,opt,name=prepaid,proto3" json:"prepaid,omitempty"`
	BookingDate          string   `protobuf:"bytes,3,opt,name=bookingDate,proto3" json:"bookingDate,omitempty"`
	StartDate            string   `protobuf:"bytes,4,opt,name=startDate,proto3" json:"startDate,omitempty"`
	EndDate              string   `protobuf:"bytes,5,opt,name=endDate,proto3" json:"endDate,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Book) Reset()         { *m = Book{} }
func (m *Book) String() string { return proto.CompactTextString(m) }
func (*Book) ProtoMessage()    {}
func (*Book) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{6}
}
func (m *Book) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Book.Unmarshal(m, b)
}
func (m *Book) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Book.Marshal(b, m, deterministic)
}
func (dst *Book) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Book.Merge(dst, src)
}
func (m *Book) XXX_Size() int {
	return xxx_messageInfo_Book.Size(m)
}
func (m *Book) XXX_DiscardUnknown() {
	xxx_messageInfo_Book.DiscardUnknown(m)
}

var xxx_messageInfo_Book proto.InternalMessageInfo

func (m *Book) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Book) GetPrepaid() int64 {
	if m != nil {
		return m.Prepaid
	}
	return 0
}

func (m *Book) GetBookingDate() string {
	if m != nil {
		return m.BookingDate
	}
	return ""
}

func (m *Book) GetStartDate() string {
	if m != nil {
		return m.StartDate
	}
	return ""
}

func (m *Book) GetEndDate() string {
	if m != nil {
		return m.EndDate
	}
	return ""
}

type ListOrder struct {
	List                 []*Order `protobuf:"bytes,1,rep,name=list,proto3" json:"list,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListOrder) Reset()         { *m = ListOrder{} }
func (m *ListOrder) String() string { return proto.CompactTextString(m) }
func (*ListOrder) ProtoMessage()    {}
func (*ListOrder) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{7}
}
func (m *ListOrder) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListOrder.Unmarshal(m, b)
}
func (m *ListOrder) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListOrder.Marshal(b, m, deterministic)
}
func (dst *ListOrder) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListOrder.Merge(dst, src)
}
func (m *ListOrder) XXX_Size() int {
	return xxx_messageInfo_ListOrder.Size(m)
}
func (m *ListOrder) XXX_DiscardUnknown() {
	xxx_messageInfo_ListOrder.DiscardUnknown(m)
}

var xxx_messageInfo_ListOrder proto.InternalMessageInfo

func (m *ListOrder) GetList() []*Order {
	if m != nil {
		return m.List
	}
	return nil
}

// TODO: Refactoring
type StatusReq struct {
	OrderId              int64    `protobuf:"varint,1,opt,name=orderId,proto3" json:"orderId,omitempty"`
	Status               int64    `protobuf:"varint,2,opt,name=status,proto3" json:"status,omitempty"`
	UserId               int64    `protobuf:"varint,3,opt,name=userId,proto3" json:"userId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StatusReq) Reset()         { *m = StatusReq{} }
func (m *StatusReq) String() string { return proto.CompactTextString(m) }
func (*StatusReq) ProtoMessage()    {}
func (*StatusReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{8}
}
func (m *StatusReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StatusReq.Unmarshal(m, b)
}
func (m *StatusReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StatusReq.Marshal(b, m, deterministic)
}
func (dst *StatusReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StatusReq.Merge(dst, src)
}
func (m *StatusReq) XXX_Size() int {
	return xxx_messageInfo_StatusReq.Size(m)
}
func (m *StatusReq) XXX_DiscardUnknown() {
	xxx_messageInfo_StatusReq.DiscardUnknown(m)
}

var xxx_messageInfo_StatusReq proto.InternalMessageInfo

func (m *StatusReq) GetOrderId() int64 {
	if m != nil {
		return m.OrderId
	}
	return 0
}

func (m *StatusReq) GetStatus() int64 {
	if m != nil {
		return m.Status
	}
	return 0
}

func (m *StatusReq) GetUserId() int64 {
	if m != nil {
		return m.UserId
	}
	return 0
}

type StatusRes struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Status               bool     `protobuf:"varint,2,opt,name=status,proto3" json:"status,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *StatusRes) Reset()         { *m = StatusRes{} }
func (m *StatusRes) String() string { return proto.CompactTextString(m) }
func (*StatusRes) ProtoMessage()    {}
func (*StatusRes) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_20c827713e6bca4d, []int{9}
}
func (m *StatusRes) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_StatusRes.Unmarshal(m, b)
}
func (m *StatusRes) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_StatusRes.Marshal(b, m, deterministic)
}
func (dst *StatusRes) XXX_Merge(src proto.Message) {
	xxx_messageInfo_StatusRes.Merge(dst, src)
}
func (m *StatusRes) XXX_Size() int {
	return xxx_messageInfo_StatusRes.Size(m)
}
func (m *StatusRes) XXX_DiscardUnknown() {
	xxx_messageInfo_StatusRes.DiscardUnknown(m)
}

var xxx_messageInfo_StatusRes proto.InternalMessageInfo

func (m *StatusRes) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *StatusRes) GetStatus() bool {
	if m != nil {
		return m.Status
	}
	return false
}

func init() {
	proto.RegisterType((*OrderIdReq)(nil), "order.OrderIdReq")
	proto.RegisterType((*OrderMasterReq)(nil), "order.OrderMasterReq")
	proto.RegisterType((*Order)(nil), "order.Order")
	proto.RegisterType((*OrderProfileRequest)(nil), "order.OrderProfileRequest")
	proto.RegisterType((*Books)(nil), "order.Books")
	proto.RegisterType((*OrderItem)(nil), "order.OrderItem")
	proto.RegisterType((*Book)(nil), "order.Book")
	proto.RegisterType((*ListOrder)(nil), "order.ListOrder")
	proto.RegisterType((*StatusReq)(nil), "order.StatusReq")
	proto.RegisterType((*StatusRes)(nil), "order.StatusRes")
}

func init() {
	proto.RegisterFile("gitlab.com/unie.kz/nova/order/rpc/order/service.proto", fileDescriptor_service_20c827713e6bca4d)
}

var fileDescriptor_service_20c827713e6bca4d = []byte{
	// 818 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x55, 0x5b, 0x6f, 0x23, 0x35,
	0x14, 0x56, 0x3b, 0x93, 0x26, 0x73, 0x9a, 0x2d, 0x89, 0x17, 0x90, 0x55, 0xad, 0x20, 0x8c, 0x78,
	0x58, 0x15, 0x91, 0xac, 0x5a, 0x71, 0x79, 0x44, 0x65, 0x25, 0x14, 0x89, 0x65, 0x57, 0x53, 0xed,
	0x0b, 0x6f, 0xce, 0xd8, 0xbb, 0x35, 0x3b, 0x33, 0x9e, 0xd8, 0x4e, 0xa5, 0xf0, 0xca, 0x1b, 0xef,
	0xfc, 0x31, 0x7e, 0x11, 0xf2, 0xb1, 0xe7, 0x92, 0x0b, 0x94, 0xa7, 0x9c, 0xf3, 0x7d, 0xf6, 0xb9,
	0xcd, 0xe7, 0x13, 0xf8, 0xe6, 0xbd, 0xb4, 0x05, 0x5b, 0xcd, 0x73, 0x55, 0x2e, 0x36, 0x95, 0x14,
	0xf3, 0x0f, 0xbf, 0x2f, 0x2a, 0xf5, 0xc0, 0x16, 0x4a, 0x73, 0xa1, 0x17, 0xba, 0xce, 0x83, 0x65,
	0x84, 0x7e, 0x90, 0xb9, 0x98, 0xd7, 0x5a, 0x59, 0x45, 0x06, 0x08, 0xa6, 0x19, 0xc0, 0x6b, 0x67,
	0x2c, 0x79, 0x26, 0xd6, 0xe4, 0x02, 0x4e, 0x25, 0xa7, 0x27, 0xb3, 0x93, 0xe7, 0x51, 0x76, 0x2a,
	0x39, 0xf9, 0x14, 0xce, 0x36, 0xc6, 0x91, 0xf4, 0x14, 0xb1, 0xe0, 0x91, 0x67, 0x90, 0xd4, 0x4c,
	0xdb, 0x0a, 0xa9, 0x08, 0xa9, 0x0e, 0x48, 0xbf, 0x85, 0x0b, 0x8c, 0xf9, 0x8a, 0x19, 0x2b, 0xf4,
	0xb1, 0xb8, 0x1f, 0xc3, 0xa0, 0x60, 0x2b, 0x51, 0x60, 0xd8, 0x24, 0xf3, 0x4e, 0xfa, 0x57, 0x0c,
	0x03, 0xbc, 0x78, 0xac, 0x0e, 0x63, 0x99, 0xdd, 0x98, 0x70, 0x21, 0x78, 0xbd, 0xfa, 0xa2, 0x7f,
	0xaf, 0x2f, 0xde, 0xab, 0x8f, 0x5c, 0xc2, 0xc8, 0xdf, 0x5f, 0x72, 0x3a, 0x40, 0xb2, 0xf5, 0xc9,
	0x67, 0x00, 0x92, 0xbf, 0x12, 0x3a, 0xbf, 0x67, 0x95, 0xa5, 0x67, 0xc8, 0xf6, 0x10, 0x17, 0xf9,
	0x9d, 0xd4, 0xc6, 0xfe, 0xc2, 0x4a, 0x41, 0x87, 0x58, 0x4c, 0x07, 0xb8, 0xc8, 0x05, 0x0b, 0xe4,
	0x08, 0xc9, 0xd6, 0x77, 0x91, 0x4b, 0xc9, 0x79, 0x21, 0x90, 0x4d, 0x90, 0xed, 0x21, 0x6e, 0x26,
	0xa2, 0x64, 0xb2, 0xa0, 0xe0, 0x67, 0x82, 0x8e, 0x43, 0xeb, 0x7b, 0x55, 0x09, 0x7a, 0xee, 0x51,
	0x74, 0x08, 0x85, 0x21, 0xe3, 0x5c, 0x0b, 0x63, 0xe8, 0x18, 0xf1, 0xc6, 0x75, 0x59, 0x6a, 0x65,
	0x2c, 0x2b, 0x7e, 0x54, 0x5c, 0xd0, 0x27, 0x3e, 0x4b, 0x87, 0x10, 0x02, 0x71, 0x2e, 0xed, 0x96,
	0x5e, 0x20, 0x83, 0x36, 0xf9, 0x12, 0x9e, 0x58, 0xcd, 0x2a, 0xc3, 0x72, 0x2b, 0x55, 0xb5, 0xe4,
	0xf4, 0x23, 0x6c, 0x7b, 0x17, 0x74, 0xbd, 0xe5, 0xaa, 0x2c, 0x45, 0x65, 0x0d, 0x9d, 0xf8, 0xde,
	0x1a, 0x9f, 0x4c, 0x20, 0x92, 0xb2, 0xa2, 0x53, 0x84, 0x9d, 0xe9, 0x2a, 0xcc, 0xb5, 0x60, 0x56,
	0x70, 0x4a, 0x7c, 0x85, 0xc1, 0x25, 0x2f, 0x00, 0x50, 0x7a, 0x4b, 0x2b, 0x4a, 0x43, 0x9f, 0xce,
	0xa2, 0xe7, 0xe7, 0xd7, 0x93, 0x39, 0x42, 0xf3, 0xd7, 0x0d, 0x91, 0xf5, 0xce, 0xa4, 0x12, 0x9e,
	0x22, 0xf1, 0x46, 0xab, 0x77, 0xb2, 0x10, 0x99, 0x58, 0x6f, 0x84, 0xb1, 0x2e, 0x29, 0x67, 0xdb,
	0xa0, 0x12, 0x67, 0xba, 0x61, 0x95, 0xaa, 0xb2, 0xf7, 0x41, 0xad, 0xde, 0x71, 0x2d, 0x6f, 0x05,
	0xd3, 0x41, 0x22, 0x68, 0xf7, 0x84, 0x13, 0xf7, 0x85, 0x93, 0x5e, 0xc1, 0xe0, 0x56, 0xa9, 0x0f,
	0x86, 0x7c, 0x01, 0x83, 0x95, 0x33, 0xe8, 0x09, 0x16, 0x78, 0x1e, 0x0a, 0x74, 0x64, 0xe6, 0x99,
	0xf4, 0xef, 0x53, 0x48, 0xda, 0x82, 0x0f, 0x24, 0xfb, 0x0c, 0x12, 0xb3, 0x59, 0xfd, 0x26, 0x72,
	0xdb, 0xbe, 0x9e, 0x0e, 0xf8, 0xef, 0x07, 0x84, 0x1f, 0x49, 0x19, 0x1b, 0x6a, 0x43, 0xdb, 0x8d,
	0x7f, 0xbd, 0x61, 0x95, 0x75, 0x1f, 0x2f, 0x88, 0xb6, 0xf1, 0xbb, 0xe7, 0x74, 0xd6, 0x7b, 0x4e,
	0x24, 0x85, 0x31, 0x17, 0x85, 0x7c, 0x10, 0x7a, 0xfb, 0x92, 0xd9, 0x46, 0xad, 0x3b, 0x98, 0xcb,
	0x64, 0xb7, 0xb5, 0x17, 0x6b, 0x94, 0xa1, 0xed, 0x32, 0xb9, 0xdf, 0x97, 0xc2, 0xe4, 0x41, 0xa6,
	0xad, 0xef, 0xce, 0xd7, 0x4c, 0x72, 0xd4, 0x68, 0x94, 0xa1, 0x4d, 0xae, 0x60, 0x22, 0x2b, 0x27,
	0x30, 0xa7, 0x85, 0x37, 0x42, 0x4b, 0xc5, 0x83, 0x5a, 0x0f, 0x70, 0xf2, 0x39, 0xc4, 0x6e, 0x78,
	0xa8, 0xda, 0xbd, 0xa9, 0x22, 0x91, 0xfe, 0x79, 0x02, 0xb1, 0x73, 0x0f, 0xe6, 0x49, 0x61, 0x58,
	0x6b, 0x81, 0xc9, 0xfd, 0x34, 0x1b, 0x97, 0xcc, 0xe0, 0xdc, 0x5d, 0x95, 0xd5, 0x7b, 0x6c, 0x33,
	0xc2, 0xd4, 0x7d, 0x08, 0xbf, 0x85, 0x65, 0xda, 0x22, 0x1f, 0xfb, 0x47, 0xdb, 0x02, 0x2e, 0xb2,
	0xa8, 0x38, 0x72, 0x03, 0x2f, 0xd5, 0xe0, 0xa6, 0x5f, 0x43, 0xf2, 0xb3, 0x34, 0xd6, 0xef, 0xa4,
	0x19, 0xc4, 0x85, 0x34, 0x36, 0x08, 0x62, 0xdc, 0x57, 0x6c, 0x86, 0x4c, 0xfa, 0x16, 0x92, 0x3b,
	0xdc, 0x23, 0x6e, 0xe5, 0x51, 0x18, 0x7a, 0x09, 0x37, 0x4d, 0x34, 0xee, 0xde, 0x32, 0x8b, 0x1e,
	0x5b, 0x66, 0xe9, 0x4d, 0x17, 0xd6, 0x3c, 0xb2, 0x19, 0x47, 0x4d, 0xb0, 0xeb, 0x3f, 0x22, 0x18,
	0x63, 0x6d, 0x77, 0x7e, 0xeb, 0x93, 0xaf, 0x60, 0xf4, 0x93, 0x08, 0xad, 0x4c, 0x77, 0x9e, 0x9b,
	0xdb, 0xfc, 0x97, 0x3b, 0xfd, 0x90, 0xef, 0x61, 0xda, 0x1c, 0x36, 0xb7, 0x5b, 0xbf, 0xc7, 0xc9,
	0x27, 0xfd, 0x23, 0xed, 0x6e, 0x6f, 0x6f, 0xfa, 0x77, 0xf3, 0x02, 0x92, 0xf6, 0xe6, 0xb1, 0x3c,
	0xcd, 0x4b, 0xef, 0xe6, 0x7a, 0x03, 0xe3, 0x2e, 0xd7, 0xf5, 0xea, 0xff, 0x5d, 0xba, 0x82, 0xd1,
	0x5d, 0xd3, 0xcd, 0x4e, 0xe9, 0xed, 0xd9, 0x6e, 0x64, 0xdf, 0xc1, 0xf4, 0x6d, 0xcd, 0x99, 0x15,
	0x7e, 0x1e, 0x7e, 0xd8, 0xfb, 0xc7, 0xd6, 0x47, 0x2e, 0xfe, 0x00, 0x93, 0xb6, 0xb2, 0xb0, 0x7b,
	0xc8, 0x65, 0x3f, 0xd9, 0xee, 0x42, 0x3a, 0x2c, 0xf3, 0x76, 0xf8, 0xab, 0xff, 0x9b, 0x5d, 0x9d,
	0xe1, 0x9f, 0xee, 0xcd, 0x3f, 0x01, 0x00, 0x00, 0xff, 0xff, 0x84, 0xf8, 0x0f, 0x62, 0xad, 0x07,
	0x00, 0x00,
}
