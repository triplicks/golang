package orderserver

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/twitchtv/twirp"
	pb "gitlab.com/unie.kz/nova/order/rpc/order"
)

// New server
func New(dbUrl string) *server {
	return new(server{dbUrl})
}

type server struct{ Url string}

// Mutations

func (s *server) UpdateOrderStatus(ctx context.Context, request *pb.StatusReq) (Res *pb.StatusRes, err error) {
	if request.Status <= 0 {
		return &pb.StatusRes{Status: false}, nil
	}
	connStr := s.Url
	db, err := sql.Open("postgres", connStr)
	defer db.Close()
	if err != nil {
		return nil, err
	}
	fmt.Println(request.UserId)
	stmt, err := db.Prepare(`UPDATE "order" SET status_id = $1 WHERE id = $2`)
	if err != nil {
		return nil, err
	}
	fmt.Println("update", request)
	res, err := stmt.Exec(request.Status, request.OrderId)
	if err != nil {
		return nil, err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}
	if affect == 0 {
		return nil, twirp.NewError(twirp.Aborted, "Permision denied")
	}
	return &pb.StatusRes{Status: true}, nil
}
func (s *server) SetOrder(ctx context.Context, request *pb.Order) (Res *pb.StatusRes, err error) {
	countItems := len(request.OrderItems)
	if countItems == 0 {
		panic("NO OrderItems")
	}
	connStr := s.Url
	db, err := sql.Open("postgres", connStr)
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	tx, err := db.Begin()
	if err != nil {
		panic("TRANSACTION NOT")
	}
	var InsertId int
	err = db.QueryRow(`INSERT INTO
							"order"
								(partner_id,first_name,last_name,
								middle_name,email,phone,address,postal_code,city,
								transaction_id,comments,iin,user_id,status_id,id_merchant)
							VALUES
								($1,$2,$3,$4,$5,
								$6,$7,$8,$9,$10,
								$11,$12,$13,1,$14
								)
							returning id`,
		request.PartnerId, request.FirstName,
		request.LastName, request.MiddleName, request.Email,
		request.Phone, request.Address, request.PostalCode,
		request.City, request.TransactionId, request.Comments,
		request.Iin, request.UserId, request.IdMerchant,
	).Scan(&InsertId)
	if err != nil {
		tx.Rollback()
		fmt.Println("order insert ", err)
		return nil, err
	}
	for i := 0; i < countItems; i++ {
		var buf int
		err = db.QueryRow(`
			INSERT INTO 
				order_items
					(
						order_id,subject_id,
						cost,quantity,
						label,delivery_date,
						paid,_type,instalment_period,
						partner_id
					)
			VALUES
				(
					$1,$2,
					$3,$4,
					$5,$6,
					$7,$8,
					$9,$10
				)
			returning id`,
			InsertId,
			request.OrderItems[i].SubjectId,
			request.OrderItems[i].Cost,
			request.OrderItems[i].Quantity,
			request.OrderItems[i].Label,
			request.OrderItems[i].DeliveryDate,
			1,
			request.OrderItems[i].Type,
			request.OrderItems[i].InstalmentPeriod,
			request.OrderItems[i].PartnerId,
		).Scan(&buf)
		if err != nil {
			tx.Rollback()
			fmt.Println("orderItem insert ", request.OrderItems[i].Type, err)
			return nil, err
		}
		if request.OrderItems[i].Type == 2 {
			var bufBook int
			err = db.QueryRow(`
				INSERT INTO 
					book(
							prepaid,booking_date,
							start_date,end_date,
							_order_item
						) 
				VALUES 
					($1,$2,$3,$4,$5) 
				returning id`,
				request.OrderItems[i].Book.Prepaid,
				request.OrderItems[i].Book.BookingDate,
				request.OrderItems[i].Book.StartDate,
				request.OrderItems[i].Book.EndDate,
				buf,
			).Scan(&bufBook)
			if err != nil {
				tx.Rollback()
				fmt.Println("Book insert", err)
				return nil, err
			}
		}
	}
	tx.Commit()
	fmt.Println(err, InsertId, request)
	return &pb.StatusRes{Id: int64(InsertId), Status: true}, nil
}

// Queries
var connect string = s.Url

func (s *server) GetOrder(ctx context.Context, request *pb.OrderIdReq) (ReqOrder *pb.Order, err error) {
	connStr := s.Url
	db, err := sql.Open("postgres", connStr)
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("id", request)
	rows, err := db.Query(
		`SELECT 
				b.id, name, partner_id, status_id, id_merchant, first_name, last_name, 
				middle_name, email, phone, address, postal_code, city, 
				transaction_id, comments, iin, ar_jsjs, created
		FROM(
			SELECT 
				o.id, created , user_id,partner_id, 
				status_id, id_merchant, first_name, 
				last_name, middle_name, email, phone, 
				address, postal_code, city, transaction_id, 
				comments, iin, array_to_json(array_agg(jsjs)) as ar_jsjs
			FROM "order" o
			LEFT JOIN(
				SELECT 
					id,
					order_id, 
					row_to_json(( 
						SELECT x FROM 
							(
								SELECT 
									id, subject_id as subjectId, 
									cost, quantity, label, 
									_type as type, delivery_date, paid,
									instalment_period, bo.book
							) 
					x)) AS jsjs
				FROM order_items
				LEFT JOIN(
					SELECT 
						_order_item, 
						row_to_json(( 
							SELECT x FROM 
								(
									SELECT 
										prepaid , booking_date bookingDate, 
										start_date startDate, end_date endDate
								) 
						x)) AS book
					FROM book
				)bo
				ON id = bo._order_item
			)a
			ON o.id = a.order_id
			GROUP BY 
				o.id, o.user_id, o.partner_id, 
				o.status_id, id_merchant, first_name, 
				last_name, middle_name, email, 
				phone, address, postal_code, city, 
				transaction_id, comments, iin, created
		)b 
		LEFT JOIN status s 
		ON s.id = b.status_id
		WHERE b.id = $1 AND (b.user_id = $2 OR b.partner_id = $2)`, request.Id, request.UserId)
	fmt.Println("asdfasdfasdfsadf", request.Id)
	if err != nil {
		return nil, err
	}
	Order := &pb.Order{}
	for rows.Next() {
		Item := []*pb.OrderItem{}
		var jsonStr string
		rows.Scan(
			&Order.Id, &Order.Status, &Order.PartnerId, &Order.StatusId, &Order.IdMerchant, &Order.FirstName,
			&Order.LastName, &Order.MiddleName, &Order.Email, &Order.Phone,
			&Order.Address, &Order.PostalCode, &Order.City, &Order.TransactionId,
			&Order.Comments, &Order.Iin, &jsonStr, &Order.Created)
		json.Unmarshal([]byte(jsonStr), &Item)
		fmt.Println("Item    ", Item, "\n")
		fmt.Println("jsonStr", jsonStr)

		countJson := len(Item)
		fmt.Println("ORDER    ", Order, countJson)
		if countJson != 0 && Item[0] != nil {
			for i := 0; i < countJson; i++ {
				Order.OrderItems = append(Order.OrderItems, Item[i])
			}
		}
		fmt.Println("\n", "ORDER ITEEM    ", Order.OrderItems, "\n")
		fmt.Println("tut", Order.Id)
	}
	return Order, nil
}
func (s *server) GetOrders(ctx context.Context, request *pb.OrderIdReq) (ReqOrders *pb.ListOrder, err error) {
	Buffer := []*pb.Order{}
	fmt.Println("\n\n\n\n\n\n\n\n")
	fmt.Println("CONTEXT-Handlers", ctx)
	fmt.Println("\n\n\n\n\n\n\n\n")
	connStr := s.Url
	db, err := sql.Open("postgres", connStr)
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	rows, err := db.Query(
		`SELECT 
				b.id,s.name name, partner_id, status_id, id_merchant, first_name, last_name, 
				middle_name, email, phone, address, postal_code, city, 
				transaction_id, comments, iin, ar_jsjs, created
		FROM(
			SELECT 
				o.id, created , user_id,partner_id, 
				status_id, id_merchant, first_name, 
				last_name, middle_name, email, phone, 
				address, postal_code, city, transaction_id, 
				comments, iin, array_to_json(array_agg(jsjs)) as ar_jsjs
			FROM "order" o
			LEFT JOIN(
				SELECT 
					id,
					order_id, 
					row_to_json(( 
						SELECT x FROM 
							(
								SELECT 
									id, subject_id as subjectId, 
									cost, quantity, label, 
									_type as type, delivery_date, paid,
									instalment_period, bo.book
							) 
					x)) AS jsjs
				FROM order_items
				LEFT JOIN(
					SELECT 
						_order_item, 
						row_to_json(( 
							SELECT x FROM 
								(
									SELECT 
										prepaid , booking_date bookingDate, 
										start_date startDate, end_date endDate
								) 
						x)) AS book
					FROM book
				)bo
				ON id = bo._order_item
			)a
			ON o.id = a.order_id
			GROUP BY 
				o.id, o.user_id, o.partner_id, 
				o.status_id, id_merchant, first_name, 
				last_name, middle_name, email, 
				phone, address, postal_code, city, 
				transaction_id, comments, iin, created
			ORDER BY o.id
		)b 
		LEFT JOIN status s 
		ON s.id = b.status_id
		WHERE  b.user_id = $1 OR b.partner_id = $1`, request.UserId)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		Order := &pb.Order{}
		Item := []*pb.OrderItem{}
		var jsonStr string
		rows.Scan(
			&Order.Id, &Order.Status, &Order.PartnerId, &Order.StatusId, &Order.IdMerchant, &Order.FirstName,
			&Order.LastName, &Order.MiddleName, &Order.Email, &Order.Phone, &Order.Address,
			&Order.PostalCode, &Order.City, &Order.TransactionId,
			&Order.Comments, &Order.Iin, &jsonStr, &Order.Created)
		json.Unmarshal([]byte(jsonStr), &Item)
		countJson := len(Item)
		fmt.Println("json", jsonStr)
		fmt.Println("\n", Item, "\n")
		if countJson != 0 && Item[0] != nil {
			for i := 0; i < countJson; i++ {
				Order.OrderItems = append(Order.OrderItems, Item[i])
				fmt.Println(Item[i], Item[i].SubjectId)
			}
		}
		Buffer = append(Buffer, Order)
	}

	fmt.Println(Buffer)
	test := &pb.ListOrder{List: Buffer}
	return test, nil
}
func (s *server) GetOrdersByMaster(ctx context.Context, request *pb.OrderMasterReq) (ReqOrders *pb.Books, err error) {
	Buffer := []*pb.Book{}
	connStr := s.Url
	db, err := sql.Open("postgres", connStr)
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	rows, err :=
		db.Query(
			`SELECT 
					oi.subject_id,
					b.start_date,
					b.end_date
				FROM
					order_items oi
				LEFT JOIN
					book b
				ON 
					b._order_item = oi.id
				WHERE 
					oi.subject_id = $1 AND 
					oi.label = $2`,
			request.Id,
			request.Label,
		)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		Book := &pb.Book{}
		err = rows.Scan(&Book.Id, &Book.StartDate, &Book.EndDate)
		fmt.Println("errrr", err)
		Buffer = append(Buffer, Book)
	}
	fmt.Println("tttutututu order ", Buffer)
	test := &pb.Books{Books: Buffer}
	return test, nil
}
func (s *server) GetOrdersB2B(ctx context.Context, request *pb.OrderIdReq) (ReqOrders *pb.ListOrder, err error) {
	Buffer := []*pb.Order{}
	connStr := s.Url
	db, err := sql.Open("postgres", connStr)
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	rows, err := db.Query(
		`
		SELECT
			data.id, statusName, partner_id, status_id, id_merchant, first_name, last_name, 
			middle_name, email, phone, address, postal_code, city, 
			transaction_id, comments, iin, ar_jsjs, created
		FROM
		  (
		    SELECT 
		        o.id,s.name statusName, created , user_id,o.partner_id, 
		        status_id, id_merchant, first_name, 
		        last_name, middle_name, email, phone, 
		        address, postal_code, city, transaction_id, 
		        comments, iin, array_to_json(array_agg(jsjs)) as ar_jsjs
		    FROM "order" o
		    INNER JOIN(
		        SELECT 
		          id,
		          order_id, 
		          partner_id,
		          row_to_json(( 
		            SELECT x FROM 
		              (
		                SELECT 
		                  id, subject_id as subjectId, 
		                  cost, quantity, label, 
		                  _type as type, delivery_date, paid,
		                  instalment_period, bo.book
		              ) 
		          x)) AS jsjs
		        FROM order_items
		        LEFT JOIN(
		          SELECT 
		            _order_item, 
		            row_to_json(( 
		              SELECT x FROM 
		                (
		                  SELECT 
		                    prepaid , booking_date bookingDate, 
		                    start_date startDate, end_date endDate
		                ) 
		            x)) AS book
		          FROM book
		        )bo
		        ON id = bo._order_item
		      )oi
		    ON oi.order_id = o.id AND oi.partner_id = $1
		    LEFT JOIN status s
		    ON s.id = o.status_id
		    GROUP BY 
		            o.id, s.name, o.user_id, o.partner_id, 
		            o.status_id, id_merchant, first_name, 
		            last_name, middle_name, email, 
		            phone, address, postal_code, city, 
		            transaction_id, comments, iin, created
			ORDER BY o.id DESC
		  )data`, request.UserId)
	fmt.Println("asdfasdf")
	if err != nil {
		fmt.Println("err",err);
		return nil, err
	}
	for rows.Next() {
		Order := &pb.Order{}
		Item := []*pb.OrderItem{}
		var jsonStr string
		err := rows.Scan(
			&Order.Id, &Order.Status, &Order.PartnerId, &Order.StatusId, &Order.IdMerchant, &Order.FirstName,
			&Order.LastName, &Order.MiddleName, &Order.Email, &Order.Phone, &Order.Address,
			&Order.PostalCode, &Order.City, &Order.TransactionId,
			&Order.Comments, &Order.Iin, &jsonStr, &Order.Created)

		fmt.Println(err, Order)
		json.Unmarshal([]byte(jsonStr), &Item)
		countJson := len(Item)
		fmt.Println("json", jsonStr)
		fmt.Println("\n", Item, "\n")
		if countJson != 0 && Item[0] != nil {
			for i := 0; i < countJson; i++ {
				Order.OrderItems = append(Order.OrderItems, Item[i])
				fmt.Println(Item[i], Item[i].SubjectId)
			}
		}
		Buffer = append(Buffer, Order)
	}

	fmt.Println(Buffer)
	test := &pb.ListOrder{List: Buffer}
	return test, nil
}
func (s *server) GetOrdersProfile(ctx context.Context, request *pb.OrderProfileRequest) (response *pb.ListOrder, err error) {
	db, err := sql.Open("postgres", connect)
	if err != nil {
		return
	}
	response = &pb.ListOrder{}
	fmt.Println(request)
	rows, err := db.Query(`
							SELECT
								row_to_json((
									SELECT x FROM(
										SELECT
											o.id, s.name status, o.created,data.json orderItems,
											o.user_id userId, o.partner_id partnerId,
											o.status_id statusId,o.id_merchant idMerchant,
											o.transaction_id transactionId,o.comments
									) x
								))
							FROM "order" o
							LEFT JOIN(
								SELECT 
									oi.order_id,
									ARRAY_TO_JSON(
										ARRAY_AGG((
											SELECT x FROM (
												SELECT 
													oi.subject_id subjectId,oi.cost,
													oi.quantity,oi.label,
													oi.delivery_date,
													oi.instalment_period,
													oi.paid,oi.partner_id, 
													CASE
														WHEN b.id IS NOT NULL THEN 
															row_to_json((
																SELECT x FROM(
																	SELECT 
																		b.id, b.prepaid,
																		b.start_date startDate,
																		b.end_date endDate
																) x
															))
														ELSE 
															null
													END book
											) x 
										)
									)
							) json
								FROM order_items oi
								LEFT JOIN book b 
								ON b._order_item = oi.id
								GROUP BY oi.order_id
							)data
							ON data.order_id = o.id
							LEFT JOIN status s
							ON s.id = o.status_id
							WHERE 
								date_part('day',o.created) = $1 AND 
								date_part('month', o.created) = $2 AND 
								date_part('year', o.created) = $3 AND
								o.user_id = $4
							ORDER BY o.id DESC
						`,
		request.Day,
		request.Month,
		request.Year,
		request.UserId,
	)

	if err != nil {
		return
	}
	for rows.Next() {
		jsonBuffer, order := "", &pb.Order{}
		rows.Scan(&jsonBuffer)
		json.Unmarshal([]byte(jsonBuffer), &order)
		response.List = append(response.List, order)
	}
	fmt.Println(request.UserId)
	fmt.Println(response)
	return
}
