package lifestyleserver

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/unie.kz/nova/lifestyle/rpc/lifestyle"
)

// New server
func New() *server {
	return new(server)
}

var connect string = "postgres://wyvjlrxzuhowur:5dca56d23e01a8780d01c4568047af5930bd4da8f51da0b1e916742e72a1720a@ec2-107-21-233-72.compute-1.amazonaws.com:5432/d5iirp9eemuhum"

type server struct{}

// Queries

func (s *server) GetCategory(ctx context.Context, request *lifestyle.CategoriesRequest) (*lifestyle.CategoriesResponse, error) {
	db, err := sql.Open("postgres", connect)
	categories := lifestyle.CategoriesResponse{}
	defer db.Close()
	if err != nil {
		return nil, err
	}
	rows, err := db.Query(
		`
			SELECT
				data.id, 
				data.name, 
				data.url,
				data.services 
			FROM
				(
					SELECT
						c.id,
						c.name,
						c.url,
						c._view,
						array_to_json(array_agg(json)) services
					FROM "Categories" c
					LEFT JOIN 
						(
							SELECT
								s._category,
								row_to_json((SELECT x FROM (SELECT so._office id) x )) json
							FROM "Service" s
							LEFT JOIN "ServiceOffice" so
							ON so._service = s.id
							GROUP BY s._category, so."_office"
						)service
					ON service._category = c.id
					GROUP BY 
						c.id,
						c.name,
						c.url,
						c._view
					ORDER BY id
				)data
			WHERE data._view = $1
		`, request.Id)
	for rows.Next() {
		var Json string
		Services := []*lifestyle.ServiceCategory{}
		category := lifestyle.Category{}
		rows.Scan(&category.Id, &category.Name, &category.Url, &Json)
		json.Unmarshal([]byte(Json), &Services)
		if Services[0] != nil {
			category.Services = Services
		}
		categories.Categories = append(categories.Categories, &category)
	}
	return &categories, nil
}
func (s *server) GetServices(ctx context.Context, request *lifestyle.ServicesRequest) (*lifestyle.ServicesResponse, error) {
	db, err := sql.Open("postgres", connect)
	services := lifestyle.ServicesResponse{}
	defer db.Close()
	if err != nil {
		return nil, err
	}
	rows, err := db.Query(
		`
						SELECT 
							so."_office",
							c.name
						FROM "ServiceOffice" so
						LEFT JOIN "Service" s
						ON so._service = s.id
						LEFT JOIN "Categories" c
						ON c.id = s."_category"
						WHERE s._category = $1
						GROUP BY so."_office", c.name
					`,
		request.Id,
	)
	fmt.Println(err, "\n")
	for rows.Next() {
		service := lifestyle.Service{}
		rows.Scan(&service.Id, &service.CategoryName)
		services.Services = append(services.Services, &service)
	}
	return &services, nil
}
func (s *server) GetInformationOfficeReservation(ctx context.Context, request *lifestyle.BranchRequest) (res *lifestyle.OfficeReservationResponse, err error) {
	db, err := sql.Open("postgres", connect)
	defer db.Close()
	if err != nil {
		return
	}
	ExampleWorks := []*lifestyle.ExampleWork{}
	Services := []*lifestyle.ServiceOffice{}
	rows, err := db.Query(
		`
			SELECT
				array_to_json(array_agg(distinct data.json1)) servicesJson,
				array_to_json(array_agg(distinct data.json2)) example_works
			FROM
				(
					SELECT 
						d."_service",
						d."_office",
						(SELECT x FROM (SELECT d.id, d.name,d.time,d.price) x) json1,
						(SELECT x FROM (SELECT d.url, d.description) x) json2
					FROM
					(
						SELECT
							so._service,
							so._office,
							smm.id, 
							s.name, 
							sm.time, 
							sm.price,
							e.url,
							e.description
						FROM "ServiceOffice" so
						LEFT JOIN "Master" m
						ON m._office = so._office
						LEFT JOIN "Example" e
						ON e._master = m.id
						LEFT JOIN "Service" s
						ON s.id = so._service
						LEFT JOIN "ServiceMode" sm
						ON sm._service = so._service
						INNER JOIN "ServiceModeMaster" smm
						ON smm."_serviceMode" = sm.id
					)d
				)data
			WHERE data."_office" = $1
		`,
		request.Id,
	)
	if err != nil {
		fmt.Println(err, "\n")
		return
	}
	for rows.Next() {
		var exampleWorksJson, servicesJson string
		err = rows.Scan(&servicesJson, &exampleWorksJson)
		fmt.Println(err)
		if err == nil {
			json.Unmarshal([]byte(exampleWorksJson), &ExampleWorks)
			json.Unmarshal([]byte(servicesJson), &Services)
		}
	}
	if len(ExampleWorks) > 0 {
		ExampleWorks = ExampleWorks[:len(ExampleWorks)-1]
	}
	return &lifestyle.OfficeReservationResponse{}, nil
}
func (s *server) GetInformation(ctx context.Context, request *lifestyle.BranchRequest) (response *lifestyle.BranchResponse, err error) {
	db, err := sql.Open("postgres", connect)
	defer db.Close()
	if err != nil { return;}
	ExampleWorks := []*lifestyle.ExampleWork{}
	Services := []*lifestyle.ServiceOffice{}
	rows, err := db.Query(
		`
			SELECT
				ARRAY_TO_JSON(ARRAY_REMOVE(ARRAY_AGG(DISTINCT 
					(CASE 
						WHEN e.url IS NOT NULL THEN JSONB_BUILD_OBJECT('url',e.url,'description',e.description)
						ELSE NULL
					END)
				),NULL)),
				ARRAY_TO_JSON(ARRAY_REMOVE(ARRAY_AGG(DISTINCT
					(CASE
						WHEN sm.id IS NOT NULL THEN JSONB_BUILD_OBJECT('id', sm.id,'name', s.name,'time', sm.time,'price', sm.price)
						ELSE NULL
					END)
				),NULL))
			FROM "ServiceOffice" so
			LEFT JOIN "Master" m 
			ON m._office = 1
			LEFT JOIN "Example" e
			ON e._master = m.id
			LEFT JOIN "Service" s
			ON s.id = so._service
			LEFT JOIN "ServiceMode" sm
			ON sm._service = so._service
			INNER JOIN "ServiceModeMaster" smm
			ON smm."_serviceMode" = sm.id
			WHERE so._office = $1
			GROUP BY so._office
		`,
		request.Id,
	)
	if err != nil {
		fmt.Println(err, "\n")
		return &lifestyle.BranchResponse{}, nil
	}
	for rows.Next() {
		var exampleWorksJson, servicesJson string
		err = rows.Scan(&exampleWorksJson, &servicesJson)
		if err != nil { return;}
		json.Unmarshal([]byte(exampleWorksJson), &ExampleWorks)
		json.Unmarshal([]byte(servicesJson), &Services)
	}
	return &lifestyle.BranchResponse{
		ExampleWorks: ExampleWorks,
		Services:     Services,
	}, nil
}
func (s *server) GetShedules(ctx context.Context, request *lifestyle.BranchRequest) (*lifestyle.SheduleResponse, error) {
	db, err := sql.Open("postgres", connect)
	res := lifestyle.SheduleResponse{}
	defer db.Close()
	if err != nil {
		return nil, err
	}
	rows, err := db.Query(
		`
			SELECT 
				data.id,
				data.first_name,
				data.last_name,
				data.patronymic,
				data.experience,
				data._office,
				data.masters
			FROM
				(
					SELECT 
						m.id,
						m.first_name,
						m.last_name,
						m.patronymic,
						m.experience,
						buffer.id smId,
						m._office,
						array_to_json(array_agg(json1)) as masters
					FROM "Master" m
					LEFT JOIN
						(
							SELECT
								smm._master,
								s.id,
								row_to_json((
									SELECT x FROM
										(
											SELECT smm.Id, s.time, s.price
										)
									x )) json1
							FROM "ServiceModeMaster" smm
							LEFT JOIN "ServiceMode" s
							ON s.id = smm."_serviceMode"
						) buffer
					ON buffer._master = m.id
					GROUP BY 
						m.id,
						m.first_name,
						m.last_name,
						buffer.id,
						m.patronymic,
						m.experience,
						m._office
				)data
			WHERE data.smId = $1
		`,
		request.Id,
	)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var timeJson string
		timeWorks := []*lifestyle.TimeWork{}
		master := lifestyle.Master{}
		rows.Scan(&master.Id, &master.FirstName, &master.LastName, &master.Patronymic, &master.Experiance, &master.OfficeId, &timeJson)
		json.Unmarshal([]byte(timeJson), &timeWorks)
		if timeWorks[0] != nil {
			master.TimeWorks = timeWorks
		}
		res.Masters = append(res.Masters, &master)
	}
	fmt.Println(res, err)
	return &res, nil
}
func (s *server) GetMastersOfficeById(ctx context.Context, request *lifestyle.BranchRequest) (response *lifestyle.MastersResponse, err error) {
	db, err := sql.Open("postgres", connect)
	if err != nil {
		return
	}
	defer db.Close()
	response = &lifestyle.MastersResponse{}
	rows, err := db.Query(`SELECT id,first_name,last_name,patronymic,experience,_office FROM "Master" WHERE _office = $1`, request.Id)
	if err != nil {
		return
	}
	for rows.Next() {
		master := lifestyle.Master{}
		err = rows.Scan(&master.Id, &master.FirstName, &master.LastName, &master.Patronymic, &master.Experiance, &master.OfficeId);
		if err != nil { return;}
		response.Masters = append(response.Masters, &master)
	}
	return
}
func (s *server) GetServicesOfficeById(ctx context.Context, request *lifestyle.ServicesRequest) (response *lifestyle.ServicesOfficeResponse, err error) {
	db, err := sql.Open("postgres", connect)
	if err != nil {
		return
	}
	defer db.Close()
	response = &lifestyle.ServicesOfficeResponse{}
	rows, err :=
		db.Query(
			`
				SELECT
					DATA.ID,
					DATA.NAME,
					array_to_json(
						ARRAY_AGG(
							DISTINCT DATA.json
						)
					) services
				FROM
				(
					SELECT
					buffer.serviceId ID,
					buffer.serviceName "name",
					(
						SELECT 
							x 
						FROM 
						(
							SELECT 
								buffer.serviceModeId ID,
								buffer.TIME "time",buffer.price, 
								ARRAY_REMOVE(ARRAY_AGG(
										DISTINCT buffer.json
								),NULL) masters
						) x
					) json
					FROM
						(
						SELECT
							s.ID serviceId,
							s.NAME serviceName,
							sm.ID serviceModeId,
							sm.TIME,
							sm.price,
							m.json
						FROM "Service" s
						LEFT JOIN "ServiceOffice" so
						ON so._service = s.ID
						INNER JOIN "ServiceMode" sm
						ON sm._service = s.ID
						LEFT JOIN "ServiceModeMaster" smm
						ON smm."_serviceMode" = sm.ID
						LEFT JOIN(
								SELECT
									id,
									(SELECT x FROM
											(SELECT 
												id,first_name firstName,last_name lastName,patronymic
											) x
									)json
								FROM "Master"
						)m
						ON m.id = smm._master
						WHERE so._office = $1
						GROUP BY
							sm.time,sm.price, 
							s.ID,s.NAME,sm.ID, m.json
						) buffer
					GROUP BY
						buffer.serviceId,buffer.serviceName,
						buffer.time,buffer.price, 
						buffer.serviceModeId
				) data
				GROUP BY 
					data.ID,data.NAME
			`,
			request.Id,
		)
	if err != nil {
		fmt.Println(err)
		return
	}
	for rows.Next() {
		service := &lifestyle.ServiceOfficeB2B{}
		var jsonBuffer string
		err = rows.Scan(&service.Id, &service.Name, &jsonBuffer)
		if err == nil {
			err = json.Unmarshal([]byte(jsonBuffer), &service.ServicesMode)
			if err != nil { return;}
		}
		response.Services = append(response.Services, service)
	}
	testPB := &lifestyle.ServicesOfficeResponse{}
	test, err := json.Marshal(response)
	json.Unmarshal(test, &testPB)
	return
}

// Mutations

func (s *server) CreateCategory(ctx context.Context, request *lifestyle.NewCategoryRequest) (*lifestyle.IdResponse, error) {
	db, err := sql.Open("postgres", connect)
	res := lifestyle.IdResponse{}
	defer db.Close()
	if err != nil {
		return nil, err
	}
	err = db.QueryRow(`INSERT INTO  "Categories"(name,url) VALUES ($1,$2) returning id`, request.Name, request.Url).Scan(&res.Id)
	if err != nil {
		return nil, err
	}
	return &res, nil
}
func (s *server) CreateService(ctx context.Context, request *lifestyle.NewServiceRequest) (*lifestyle.IdResponse, error) {
	db, err := sql.Open("postgres", connect)
	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	res := lifestyle.IdResponse{}
	defer db.Close()
	if err != nil {
		return nil, err
	}
	err = db.QueryRow(
		`
			INSERT 
			INTO 
				"Service"
					(short_description,description,conditions,name,_category)
			VALUES
				($1,$2,$3,$4,$5)
			returning id
		`,
		request.ShortDescription,
		request.Description,
		request.Conditions,
		request.Name,
		request.CategoryId,
	).Scan(&res.Id)
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	countServicePrice := len(request.PriceList)
	for i := 0; i < countServicePrice; i++ {
		_, err = db.Query(`INSERT INTO "ServiceMode"(_service,time,price) VALUES ($1,$2,$3)`, res.Id, request.PriceList[i].Time, request.PriceList[i].Price)
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}
	_, err = db.Query(`INSERT INTO "ServiceOffice"(_office,_service) VALUES ($1,$2)`, request.OfficeId, res.Id)
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	return &res, nil
}
func (s *server) CreateMaster(ctx context.Context, request *lifestyle.NewMasterRequest) (response *lifestyle.IdResponse, err error) {
	db, err := sql.Open("postgres", connect)
	if err != nil { return;}
	tx, err := db.Begin()
	if err != nil { return;}
	response = &lifestyle.IdResponse{};
	defer db.Close()
	if err != nil { return;}
	countMasters := len(request.Masters);
	for i := 0; i < countMasters; i++ {
		err = tx.QueryRow(`INSERT INTO "Master"(first_name,last_name,patronymic,experience,_office) VALUES ($1,$2,$3,$4,$5) returning id`,
			request.Masters[i].FirstName, request.Masters[i].LastName, request.Masters[i].Patronymic, request.Masters[i].Experiance, request.Masters[i].OfficeId,
		).Scan(&response.Id)
		if err != nil { tx.Rollback(); return;}
		count := len(request.Masters[i].TimeWorks)
		for j := -2; j < count; j++ {
			_, err = tx.Exec(`INSERT INTO "ServiceModeMaster"("_serviceMode",_master) VALUES ($1,$2)`, request.Masters[i].TimeWorks[j].Id, response.Id)
			if err != nil { tx.Rollback(); return;}
		}
	}
	return;
}
