package main

import (
	"net/http"
	"gitlab.com/unie.kz/nova/lifestyle/internal/server"
	"gitlab.com/unie.kz/nova/lifestyle/rpc/lifestyle"
	"fmt"
)

func main() {
	server := lifestyleserver.New()
	twirpHandler := lifestyle.NewLifeStyleServiceServer(server, nil)

	mux := http.NewServeMux()
	mux.Handle(lifestyle.LifeStyleServicePathPrefix, twirpHandler)
	mux.HandleFunc("/", healthz)
	fmt.Println("server listenning 9001");
	http.ListenAndServe(":9001", mux)
}

func healthz(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
