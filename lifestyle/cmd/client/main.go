package lifestyle

import (
	"context"
	"gitlab.com/unie.kz/nova/lifestyle/rpc/lifestyle"
	"net/http"
)

type Client struct {
	service lifestyle.LifeStyleService
}

//NewClient init
func NewClient(url string) (*Client, error) {
	c := lifestyle.NewLifeStyleServiceProtobufClient(url, &http.Client{})
	return &Client{c}, nil
}

func (c *Client) GetCategory(ctx context.Context, request *lifestyle.CategoriesRequest) (response *lifestyle.CategoriesResponse, err error) {
	response, err = c.service.GetCategory(context.Background(), request)
	return
}

func (c *Client) GetServices(ctx context.Context, request *lifestyle.ServicesRequest) (response *lifestyle.ServicesResponse, err error) {
	response, err = c.service.GetServices(context.Background(), request)
	return
}

func (c *Client) GetInformation(ctx context.Context, request *lifestyle.BranchRequest) (response *lifestyle.BranchResponse, err error) {
	response, err = c.service.GetInformation(context.Background(), request)
	return
}

func (c *Client) GetShedules(ctx context.Context, request *lifestyle.BranchRequest) (response *lifestyle.SheduleResponse, err error) {
	response, err = c.service.GetShedules(context.Background(), request)
	return
}

func (c *Client) CreateCategory(ctx context.Context, request *lifestyle.NewCategoryRequest) (response *lifestyle.IdResponse, err error) {
	response, err = c.service.CreateCategory(context.Background(), request)
	return
}
func (c *Client) CreateService(ctx context.Context, request *lifestyle.NewServiceRequest) (response *lifestyle.IdResponse, err error) {
	response, err = c.service.CreateService(context.Background(), request)
	return
}
func (c *Client) CreateMaster(ctx context.Context, request *lifestyle.NewMasterRequest) (response *lifestyle.IdResponse, err error) {
	response, err = c.service.CreateMaster(context.Background(), request)
	return
}

func (c *Client) GetMastersOfficeById(ctx context.Context, request *lifestyle.BranchRequest) (response *lifestyle.MastersResponse, err error) {
	response, err = c.service.GetMastersOfficeById(context.Background(), request)
	return;
}
func (c *Client) GetServicesOfficeById(ctx context.Context, request *lifestyle.ServicesRequest) (response *lifestyle.ServicesOfficeResponse, err error) {
	response, err = c.service.GetServicesOfficeById(context.Background(), request)
	return;
}
