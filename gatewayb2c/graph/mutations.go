package graph

import (
	context "context"
	"encoding/json"
	"fmt"
	graph "github.com/99designs/gqlgen/graphql"
	"github.com/machinebox/graphql"
	cp "gitlab.com/unie.kz/nova/cloudpayments/rpc/cloudpayments"
	smtpFunc "gitlab.com/unie.kz/nova/email/rpc/smtp"
	model "gitlab.com/unie.kz/nova/gatewayb2c/model"
	orderFunc "gitlab.com/unie.kz/nova/order/rpc/order"
	smsFunc "gitlab.com/unie.kz/nova/sms/rpc/sms"
	storeFunc "gitlab.com/unie.kz/nova/store/rpc/store"
	bonuseFunc "gitlab.com/unie.kz/nova/bonuse/rpc/bonuse"
	"log"
	"strconv"
)

// Test Functions

func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (model.Todo, error) {
	panic("not implemented")
}

// Order Struct

type Production struct {
	GetOfferByPartnerId struct {
		Qty           int
		Cost          int
		Name          string `json:"name"`
		PartnerPrice  int    `json:"partnerPrice"`
		ShopPartnerId struct {
			Id string `json:"id"`
		} `json:"shopPartnerId"`
		ProdId struct {
			Name string `json:"name"`
		} `json:"prodId"`
	} `json:"getOfferByPartnerId"`
}

// Order Functions

func (r *mutationResolver) CreateOrder(ctx context.Context, request model.NewOrder) (model.Order, error) {
	fmt.Println(ctx.Value("jwt").(string))
	var User int64 = 0
	// userId, err := r.cpClient.CheckToken(ctx, &cp.CheckTokenRequest{Token: ctx.Value("jwt").(string)})
	// if err != nil {
	// 	// graph.AddErrorf(ctx, "Permision denied: token is invalid")
	// 	// return 0,nil;
	// 	User = 0
	// } else {
	// 	User = userId.Id
	// }

	client := graphql.NewClient("http://api.unie.kz/v1")
	orderItems := []*orderFunc.OrderItem{}
	countItems := len(request.OrderItems)
	if countItems == 0 {
		graph.AddErrorf(ctx, "orderItems is has not sendded")
		return model.Order{}, nil
	}
	products := []Production{}
	var sum int
	for i := 0; i < countItems; i++ {
		var cost int
		var res Production
		if request.OrderItems[i].SubjectID != nil {
			req := graphql.NewRequest(`
				query ($id: Int!){
					getOfferByPartnerId(id:$id) {
						shopPartnerId{
					      id
					    }
						partnerPrice
						prodId {
							name
						}
					}
				}
			`)
			req.Var("id", *request.OrderItems[i].SubjectID)
			fmt.Println("tututt", strconv.Itoa(int(*request.OrderItems[i].SubjectID)))

			if err := client.Run(ctx, req, &res); err != nil {
				graph.AddErrorf(ctx, "product is not defined")
				return model.Order{}, err
			}
			cost = res.GetOfferByPartnerId.PartnerPrice * request.OrderItems[i].Quantity

			res.GetOfferByPartnerId.Qty = request.OrderItems[i].Quantity
			res.GetOfferByPartnerId.Cost = cost
			fmt.Println("ressss ", res)
			sum += cost
			products = append(products, res)
		} else {
			intP := 0
			request.OrderItems[i].SubjectID = &intP
			if request.OrderItems[i].Cost == nil {
				graph.AddErrorf(ctx, "Cost is undefined")
				return model.Order{}, nil
			}
			cost = *request.OrderItems[i].Cost
		}
		var bookItem *orderFunc.Book
		if request.OrderItems[i].Type == 2 {
			bookItem =
				&orderFunc.Book{
					Prepaid:     int64(request.OrderItems[i].Book.Prepaid),
					BookingDate: request.OrderItems[i].Book.BookingDate,
					StartDate:   request.OrderItems[i].Book.StartDate,
					EndDate:     request.OrderItems[i].Book.EndDate,
				}
		}
		partnerId, _ := strconv.Atoi(res.GetOfferByPartnerId.ShopPartnerId.Id)
		fmt.Println(partnerId)
		orderItems = append(orderItems, &orderFunc.OrderItem{
			SubjectId:    int64(*request.OrderItems[i].SubjectID),
			PartnerId:    int64(partnerId),
			Cost:         int64(cost),
			Quantity:     int64(request.OrderItems[i].Quantity),
			Label:        request.OrderItems[i].Label,
			DeliveryDate: request.OrderItems[i].DeliveryDate,
			Type:         int64(request.OrderItems[i].Type),
			Book:         bookItem,
		})
	}
	// test := int(request.BankID);
	if request.BankID == nil {
		nill := 0
		request.BankID = &nill
	}
	order := &orderFunc.Order{
		UserId:     User,
		PartnerId:  int64(*request.BankID),
		FirstName:  request.FirstName,
		LastName:   request.LastName,
		MiddleName: request.MiddleName,
		Email:      request.Email,
		Phone:      request.Phone,
		Address:    request.Address,
		PostalCode: request.PostalCode,
		City:       request.City,
		// Refactoring
		TransactionId: int64(12312312),
		Comments:      request.Comments,
		Iin:           request.Iin,
		OrderItems:    orderItems,
	}
	result, err := r.orderClient.SetOrder(ctx, order)
	if err != nil {
		graph.AddErrorf(ctx, "order has not created")
		return model.Order{}, err
	}
	getproducts := []*smtpFunc.Production{}
	for i := 0; i < len(products); i++ {
		getproducts = append(getproducts, &smtpFunc.Production{
			Name: products[i].GetOfferByPartnerId.Name,
			Qty:  int64(products[i].GetOfferByPartnerId.Qty),
			Cost: int64(products[i].GetOfferByPartnerId.Cost),
		})
	}

	array := []*smtpFunc.Emails{
		// {Email:"trip1998@mail.ru"},
		{Email: "eldar@unie.kz"},
	}
	emails := smtpFunc.Request{
		Email:       array,
		Id:          result.Id,
		LastName:    request.LastName,
		ClientEmail: request.Email,
		FirstName:   request.FirstName,
		MiddleName:  request.MiddleName,
		Iin:         request.Iin,
		City:        request.City,
		Phone:       request.Phone,
		Product:     getproducts,
		Amount:      int64(sum),
	}
	orderReturn := model.Order{
		ID:         int(result.Id),
		Status:     order.Status,
		PartnerID:  int(order.PartnerId),
		FirstName:  order.FirstName,
		LastName:   order.LastName,
		MiddleName: order.MiddleName,
		Email:      order.Email,
		Phone:      order.Phone,
		Address:    order.Address,
		PostalCode: order.PostalCode,
		City:       order.City,
		Comments:   order.Comments,
		Iin:        order.Iin,
		Created:    order.Created,
	}
	fmt.Println(emails)
	// r.emailClient.SendEmail(ctx, &emails)
	return orderReturn, nil
}
func (r *mutationResolver) CreateOrderLifeStyleMaster(ctx context.Context, request model.NewOrderLifeStyleMaster) (model.Order, error) {
	fmt.Println(ctx.Value("jwt").(string))
	var User int64
	userId, err := r.cpClient.CheckToken(ctx, &cp.CheckTokenRequest{Token: ctx.Value("jwt").(string)})
	if err != nil {
		User = 0
	} else {
		User = userId.Id
	}
	twirp := orderFunc.Order{}
	twirp.UserId = int64(User)
	response := model.Order{}
	jsonRequest, _ := json.Marshal(request)
	json.Unmarshal(jsonRequest, &twirp)
	json.Unmarshal(jsonRequest, &response)
	result, err := r.orderClient.SetOrder(ctx, &twirp)
	if err != nil {
		return model.Order{}, err
	}
	response.ID = int(result.Id)
	return response, nil
}
func (r *mutationResolver) UpdateOrderStatus(ctx context.Context, input *model.Status) (bool, error) {
	userId, err := r.cpClient.CheckToken(ctx, &cp.CheckTokenRequest{Token: ctx.Value("jwt").(string)})
	if err != nil {
		graph.AddErrorf(ctx, "Permision denied: token is invalid")
		return false, nil
	}
	result, err := r.orderClient.UpdateOrderStatus(ctx, &orderFunc.StatusReq{OrderId: int64(input.OrderID), Status: int64(input.Status), UserId: userId.Id})
	if err != nil {
		graph.AddErrorf(ctx, "order status has not updated, permision denied")
		return false, nil
	}
	return result.Status, nil
}

// SMS registration Functions

func (r *mutationResolver) SendSms(ctx context.Context, request *model.SMS) (bool, error) {
	sms := &smsFunc.SMS{Phone: request.Phone}
	result, err := r.smsClient.SendSms(ctx, sms)
	if err != nil {
		graph.AddErrorf(ctx, "sms has not gone", request.Kod)
		return false, nil
	}
	return result, nil
}
func (r *mutationResolver) ValidationSms(ctx context.Context, request *model.SMS) (model.AnswerSMS, error) {
	sms := &smsFunc.SMS{Phone: request.Phone, Kod: request.Kod}
	result, err := r.smsClient.ValidationKodSms(ctx, sms)
	// fmt.Println(err);
	if err != nil {
		graph.AddErrorf(ctx, "kod is invalide %s", request.Kod)
		return model.AnswerSMS{}, nil
	}
	return model.AnswerSMS{Status: result.Status, Key: result.Key}, nil
}

// Finance Functions

func (r *mutationResolver) LinkCard(ctx context.Context, card model.CardIn) (model.CardOutId, error) {
	log.Println(ctx.Value("jwt"))
	log.Println(card)

	result, err := r.cpClient.LinkCard(ctx, &cp.CardLinkRequest{
		CardHolder: card.CardHolder,
		Ccpacket:   card.Ccpacket,
		ExpDate:    card.ExpDate,
		CardToken:  card.CardToken,
		LastFour:   card.LastFour,
	})
	if err != nil {
		return model.CardOutId{
			ID: 0,
		}, err
	} else {
		return model.CardOutId{
			ID: int(result.GetId()),
		}, nil
	}

}
func (r *mutationResolver) ChangeDetails(ctx context.Context, userDetails model.UserDetailsIn) (int, error) {
	log.Println(ctx.Value("jwt"))
	log.Println(userDetails)

	result, err := r.cpClient.ChangeDetails(ctx, &cp.UserDetails{
		FirstName:    userDetails.FirstName,
		LastName:     userDetails.LastName,
		Iin:          userDetails.Iin,
		Address:      userDetails.Address,
		SocialStatus: userDetails.SocialStatus,
		Email:        userDetails.Email,
		Bday:         userDetails.Bday,
	})
	if err != nil {
		return 0, err
	} else {
		return int(result.GetSuccess()), nil
	}
}


func (r *mutationResolver) AddFavorit(ctx context.Context, id int) (response model.Favorit, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return;
	}
	buffer, err := r.storeClient.AddProductToFavorite(ctx, &storeFunc.FavoriteProductId{UserId:int64(userId), ProductId:int64(id)});
	if err != nil { return;}
	client := graphql.NewClient("http://api.unie.kz/v1")
	response = model.Favorit{};
	response.ID = int(buffer.Id);
	response.UserID = userId;
	req := graphql.NewRequest(`
			query ($id: Int!){
				getOfferByPartnerId(id:$id) {
					prodId {
						oid
						name
						description
					}
				}
			}
		`)
	req.Var("id", id)
	var res ProductFromGetOrder
	err = client.Run(ctx, req, &res)
	if err != nil { return;}
	response.ProductID = res.GetOfferByPartnerId.OrderItems.Id
	response.Name = res.GetOfferByPartnerId.OrderItems.Name
	response.Description = res.GetOfferByPartnerId.OrderItems.Description
	return;
}
func (r *mutationResolver) DeleteFavorit(ctx context.Context, id int) (response bool, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return;
	}
	buffer, err := r.storeClient.DeleteProductFavorite(ctx, &storeFunc.FavoriteProductId{UserId:int64(userId), ProductId:int64(id)});
	response = buffer.Res;
	return;
}
func (r *mutationResolver) DeleteAllFavorities(ctx context.Context) (response bool, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return;
	}
	buffer, err := r.storeClient.DeleteAllFromFavorite(ctx, &storeFunc.UserId{Id:int64(userId)});
	response = buffer.Res;
	return;
}
///refactor

// func (r *mutationResolver) SignUp(ctx context.Context, user model.UserSignUp) (int, error) {
// 	result, err := r.cpClient.SignUp(ctx, &cp.UserSignUpRequest{
// 		Number:   user.Number,
// 		Password: user.Password,
// 		Crypted:  user.Crypted,
// 		Details: &cp.UserDetails{
// 			FirstName:    user.Details.FirstName,
// 			LastName:     user.Details.LastName,
// 			SocialStatus: user.Details.SocialStatus,
// 			Iin:          user.Details.Iin,
// 			Email:        user.Details.Email,
// 			Bday:         user.Details.Bday,
// 			Address:      user.Details.Address,
// 		},
// 	})
// 	if err != nil {
// 		return 0, err
// 	}

// 	return int(result.GetId()), nil
// }


// bonuse

func (r *mutationResolver) CreateAccount(ctx context.Context, request model.NewAccount) (response int, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return response, err;
	}	
	jsonBuffer, err := json.Marshal(request);
	if err != nil { return;}
	twirpResponse := &bonuseFunc.NewAccountRequest{};
	twirpResponse.Id = int64(userId);
	json.Unmarshal(jsonBuffer, &twirpResponse)
	if err != nil { return;}
	buffer, err := r.bonuseClient.CreateAccount(ctx, twirpResponse);
	if err != nil { return;}
	response = int(buffer.Id);
	return;
}