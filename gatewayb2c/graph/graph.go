package graph

import (
	"fmt"
	"gitlab.com/unie.kz/nova/b2b/cmd/client"
	cpClient "gitlab.com/unie.kz/nova/cloudpayments/cmd/client"
	"gitlab.com/unie.kz/nova/email/cmd/client"
	"gitlab.com/unie.kz/nova/finance/cmd/client"
	financedkzClient "gitlab.com/unie.kz/nova/finance_dkz/twirp/cmd/client"
	"gitlab.com/unie.kz/nova/lifestyle/cmd/client"
	"gitlab.com/unie.kz/nova/order/cmd/client"
	"gitlab.com/unie.kz/nova/partner/cmd/client"
	"gitlab.com/unie.kz/nova/sms/cmd/client"
	"gitlab.com/unie.kz/nova/store/cmd/client"
	"gitlab.com/unie.kz/nova/profile/cmd/client"
	"gitlab.com/unie.kz/nova/bonuse/cmd/client"
	"gitlab.com/unie.kz/nova/instagramShop/cmd/client"
)

//Resolver Twirp RPC client struct
type Resolver struct {
	financeClient	    *finance.Client
	orderClient	    	*order.Client
	smsClient	        *sms.Client
	emailClient	      	*smtp.Client
	cpClient	        *cpClient.Client
	lifestyleClient	  	*lifestyle.Client
	financedkzClient	*financedkzClient.Client
	b2bClient	        *b2b.Client
	partnerClient	    *partner.Client
	storeClient		 	*store.Client
	profileClient		*profile.Client
	bonuseClient		*bonuse.Client
	instagramShopClient *instagramShop.Client
}

func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

func New(financeURL, orderURL, smsURL, emailURL, cpURL, lifestyleURL, financedkzURL, b2bURL, partnerURL, storeURL, profileURL, bonuseURL, instagramShopURL string) Config {
	financeClient, _ := finance.NewClient(financeURL)
	orderClient, _ := order.NewClient(orderURL)
	smsClient, _ := sms.NewClient(smsURL)
	emailClient, _ := smtp.NewClient(emailURL)
	cpClient, _ := cpClient.NewClient(cpURL)
	lifestyleClient, _ := lifestyle.NewClient(lifestyleURL)
	financedkzClient, _ := financedkzClient.NewClient(financedkzURL)
	fmt.Println("B@B", b2bURL)
	b2bClient, _ := b2b.NewClient(b2bURL)
	partnerClient, _ := partner.NewClient(partnerURL)
	storeClient, _ := store.NewClient(storeURL);
	profileClient, _ := profile.NewClient(profileURL)
	bonuseClient, _ := bonuse.NewClient(bonuseURL);
	instagramShopClient, _ := instagramShop.NewClient(instagramShopURL);
	c := Config{
		Resolvers: &Resolver{
			financeClient,
			orderClient,
			smsClient,
			emailClient,
			cpClient,
			lifestyleClient,
			financedkzClient,
			b2bClient,
			partnerClient,
			storeClient,
			profileClient,
			bonuseClient,
			instagramShopClient,
		},
	}
	return c
}

type mutationResolver struct{ *Resolver }

type queryResolver struct{ *Resolver }
