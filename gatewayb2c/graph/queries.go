package graph

import (
	context "context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
	graph "github.com/99designs/gqlgen/graphql"
	"github.com/PuerkitoBio/goquery"
	"github.com/dgrijalva/jwt-go"
	"github.com/machinebox/graphql"
	b2b "gitlab.com/unie.kz/nova/b2b/rpc/b2b"
	cp "gitlab.com/unie.kz/nova/cloudpayments/rpc/cloudpayments"
	rpc "gitlab.com/unie.kz/nova/finance_dkz/twirp/rpc/finance"
	model "gitlab.com/unie.kz/nova/gatewayb2c/model"
	lifestyleFunc "gitlab.com/unie.kz/nova/lifestyle/rpc/lifestyle"
	orderFunc "gitlab.com/unie.kz/nova/order/rpc/order"
	partnerFunc "gitlab.com/unie.kz/nova/partner/rpc/partner"
	profileFunc "gitlab.com/unie.kz/nova/profile/rpc/profile"
	storeFunc "gitlab.com/unie.kz/nova/store/rpc/store"
	bonuseFunc "gitlab.com/unie.kz/nova/bonuse/rpc/bonuse"
	instagramShopFunc "gitlab.com/unie.kz/nova/instagramShop/rpc/instagramShop"
	// "log"
	// "net/http"
	// "strconv"
	// "strings"
	// "time"
)

// Order Struct

type Product struct {
	GetProduct struct {
		Oid         int    `json:"oid"`
		Name        string `json:"name"`
		Description string `json:"description"`
	} `json:"getProduct"`
}
type ProductFromGetOrder struct {
	GetOfferByPartnerId struct {
		Qty           int
		Cost          int
		Name          string `json:"name"`
		PartnerPrice  int    `json:"partnerPrice"`
		ShopPartnerId struct {
			Id int `json:"id"`
		} `json:"shopPartnerId"`
		OrderItems struct {
			Id          int    `json:"oid"`
			Name        string `json:"name"`
			Description string `json:"description"`
		} `json:"prodId"`
	} `json:"getOfferByPartnerId"`
}
type Offer struct {
	GetOfferByPartnerId struct {
		Oid    int `json:"oid"`
		ProdId struct {
			Name        string `json:"name"`
			Description string `json:"description"`
		} `json:"prodId"`
	} `json:"getOfferByPartnerId"`
}

// B2b Struct

type QueryFindPartnerByID struct {
	FindPartnerByID model.Partner
}
type QueryAllPartners struct {
	AllPartners []model.Partner
}

func jwtEncoding(buffer string) (userId int, err error) {
	claims := jwt.MapClaims{}
	bufferBearrer := strings.Split(buffer, " ")
	buffer = bufferBearrer[1]
	_, err = jwt.ParseWithClaims(buffer, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte("ANdHiSNaMEISjOHNcENa"), nil
	})
	if err != nil { return;}
	userId = int(claims["id"].(float64))
	return
}
func jwtEncodingProfile(buffer string) (userId int, err error) {
	claims := jwt.MapClaims{};
	_, err = jwt.ParseWithClaims(buffer, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte("eboboshka"), nil
	});
	if err != nil {	return;}
	userId = int(claims["uid"].(float64));
	return;
}

// Profile Functions

func (r *queryResolver) SignIn(ctx context.Context, request model.UserSignIn) (string, error) {
	response, err := r.profileClient.SignIn(ctx, &profileFunc.UserSignInRequest{Number: request.Number, Password: request.Password})
	if err != nil {
		return "", err
	}
	return response.Token, nil
}

// Orders Functions

func (r *queryResolver) GetOrder(ctx context.Context, OrderId int) (*model.Order, error) {
	client := graphql.NewClient("http://api.unie.kz/v1")
	result, err := r.orderClient.GetOrder(ctx, &orderFunc.OrderIdReq{Id: int64(OrderId), UserId: 0})
	if err != nil {
		graph.AddErrorf(ctx, "has not answer, order server ")
		return nil, err
	}
	countItems := len(result.OrderItems)
	orderItemsRequest := []*model.OrderItem{}
	for i := 0; i < countItems; i++ {
		req := graphql.NewRequest("query {getOfferByPartnerId(id: " +
			strconv.Itoa(int(result.OrderItems[i].SubjectId)) +
			"){oid prodId { oid name description }}}")
		var res Offer
		err := client.Run(ctx, req, &res)
		if err == nil {
			var Book model.Book
			if result.OrderItems[i].Book != nil {
				Book = model.Book{
					Prepaid:     int(result.OrderItems[i].Book.Prepaid),
					BookingDate: result.OrderItems[i].Book.BookingDate,
					StartDate:   result.OrderItems[i].Book.StartDate,
					EndDate:     result.OrderItems[i].Book.EndDate,
				}
			}
			orderItemsRequest = append(orderItemsRequest, &model.OrderItem{
				ID:          res.GetOfferByPartnerId.Oid,
				SubjectID:   int(result.OrderItems[i].SubjectId),
				Cost:        int(result.OrderItems[i].Cost),
				Quantity:    int(result.OrderItems[i].Quantity),
				Label:       result.OrderItems[i].Label,
				Name:        res.GetOfferByPartnerId.ProdId.Name,
				Description: res.GetOfferByPartnerId.ProdId.Description,
				Type:        int(result.OrderItems[i].Type),
				Book:        &Book,
			})
		}
	}
	test := &model.Order{
		ID:         int(result.Id),
		Status:     result.Status,
		PartnerID:  int(result.PartnerId),
		StatusID:   int(result.StatusId),
		IDMerchant: int(result.IdMerchant),
		FirstName:  result.FirstName,
		LastName:   result.LastName,
		MiddleName: result.MiddleName,
		Email:      result.Email,
		Phone:      result.Phone,
		Address:    result.Address,
		PostalCode: result.PostalCode,
		City:       result.City,
		Comments:   result.Comments,
		Iin:        result.Iin,
		Created:    result.Created,
		OrderItems: orderItemsRequest,
	}
	fmt.Println("TEEEEEEEEEEEEEEEST", test, orderItemsRequest)
	return test, nil
}
func (r *queryResolver) GetOrdersB2b(ctx context.Context) (result []*model.Order, err error) {
	token := ctx.Value("b2b").(string)
	userId, err := jwtEncoding(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return nil, err
	}
	fmt.Println("userId", userId)
	client := graphql.NewClient("http://api.unie.kz/v1")
	twirpBuffer, err := r.orderClient.GetOrdersB2b(ctx, &orderFunc.OrderIdReq{UserId: int64(userId)})
	jsonBuffer, _ := json.Marshal(twirpBuffer.List)
	json.Unmarshal(jsonBuffer, &result)
	count := len(result)
	for i := 0; i < count; i++ {
		countItems := len(result[i].OrderItems)
		for j := 0; j < countItems; j++ {
			req := graphql.NewRequest(`
				query ($id: Int!){
					getOfferByPartnerId(id:$id) {
						prodId {
							oid
							name
							description
						}
					}
				}
			`)
			req.Var("id", int(result[i].OrderItems[j].SubjectID))
			var res ProductFromGetOrder
			err := client.Run(ctx, req, &res)
			fmt.Println(err)
			result[i].OrderItems[j].ID = res.GetOfferByPartnerId.OrderItems.Id
			result[i].OrderItems[j].Name = res.GetOfferByPartnerId.OrderItems.Name
			result[i].OrderItems[j].Description = res.GetOfferByPartnerId.OrderItems.Description
		}
	}
	return
}
func (r *queryResolver) GetOrdersProfile(ctx context.Context, request *model.Date) (result []*model.OrderProfile, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return nil, err
	}
	client := graphql.NewClient("http://api.unie.kz/v1")
	twirpBuffer, err := r.orderClient.GetOrdersProfile(ctx, &orderFunc.OrderProfileRequest{Day: int64(request.Day), Month: int64(request.Month), Year: int64(request.Year), UserId: int64(userId)})
	fmt.Println(err, twirpBuffer)
	if err != nil {
		return
	}
	jsonBuffer, _ := json.Marshal(twirpBuffer.List)
	json.Unmarshal(jsonBuffer, &result)
	count := len(result)
	for i := 0; i < count; i++ {
		countItems := len(result[i].OrderItems)
		for j := 0; j < countItems; j++ {
			req := graphql.NewRequest(`
				query ($id: Int!){
					getOfferByPartnerId(id:$id) {
						prodId {
							oid
							name
							description
						}
					}
				}
			`)
			req.Var("id", int(result[i].OrderItems[j].SubjectID))
			fmt.Println(result[i].OrderItems[j].SubjectID)
			var res ProductFromGetOrder
			err := client.Run(ctx, req, &res)
			if err != nil {
				continue
			}
			result[i].OrderItems[j].ID = res.GetOfferByPartnerId.OrderItems.Id
			result[i].OrderItems[j].Name = res.GetOfferByPartnerId.OrderItems.Name
			result[i].OrderItems[j].Description = res.GetOfferByPartnerId.OrderItems.Description
		}
	}
	return
}
func (r *queryResolver) GetOrders(ctx context.Context) ([]*model.Order, error) {
	token := ctx.Value("b2b").(string)
	userId, err := jwtEncoding(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return nil, err
	}
	fmt.Println(userId)
	client := graphql.NewClient("http://api.unie.kz/v1")
	result, err := r.orderClient.GetOrders(ctx, &orderFunc.OrderIdReq{UserId: 0})
	if err != nil {
		fmt.Println(err, "tut")
		return nil, err
	}
	fmt.Println("\n\n\n\n\n\n\n\n")
	fmt.Println("CONTEXT", ctx)
	fmt.Println("\n\n\n\n\n\n\n\n")
	message := []*model.Order{}
	countOrder := len(result.List)
	for i := 0; i < countOrder; i++ {

		countItems := len(result.List[i].OrderItems)
		orderItemsRequest := []*model.OrderItem{}

		for j := 0; j < countItems; j++ {
			id := strconv.Itoa(int(result.List[i].OrderItems[j].SubjectId))
			req := graphql.NewRequest("query {getOfferByPartnerId(id: " +
				id +
				"){oid prodId { oid name description }}}")
			var res Offer

			err := client.Run(ctx, req, &res)
			fmt.Println(err)
			if err == nil {
				var Book model.Book
				if result.List[i].OrderItems[j].Book != nil {
					Book = model.Book{
						Prepaid:     int(result.List[i].OrderItems[j].Book.Prepaid),
						BookingDate: result.List[i].OrderItems[j].Book.BookingDate,
						StartDate:   result.List[i].OrderItems[j].Book.StartDate,
						EndDate:     result.List[i].OrderItems[j].Book.EndDate,
					}
				}
				orderItemsRequest = append(orderItemsRequest, &model.OrderItem{
					ID:          res.GetOfferByPartnerId.Oid,
					SubjectID:   int(result.List[i].OrderItems[j].SubjectId),
					Cost:        int(result.List[i].OrderItems[j].Cost),
					Quantity:    int(result.List[i].OrderItems[j].Quantity),
					Label:       result.List[i].OrderItems[j].Label,
					Name:        res.GetOfferByPartnerId.ProdId.Name,
					Description: res.GetOfferByPartnerId.ProdId.Description,
					Type:        int(result.List[i].OrderItems[j].Type),
					Book:        &Book,
				})

			}
		}
		fmt.Println("end", result.List[i].OrderItems)
		order := &model.Order{
			ID:            int(result.List[i].Id),
			Status:        result.List[i].Status,
			PartnerID:     int(result.List[i].PartnerId),
			StatusID:      int(result.List[i].StatusId),
			IDMerchant:    int(result.List[i].IdMerchant),
			FirstName:     result.List[i].FirstName,
			LastName:      result.List[i].LastName,
			MiddleName:    result.List[i].MiddleName,
			Email:         result.List[i].Email,
			Phone:         result.List[i].Phone,
			Address:       result.List[i].Address,
			PostalCode:    result.List[i].PostalCode,
			City:          result.List[i].City,
			TransactionID: int(result.List[i].TransactionId),
			Comments:      result.List[i].Comments,
			Iin:           result.List[i].Iin,
			Created:       result.List[i].Created,
			OrderItems:    orderItemsRequest,
		}

		message = append(message, order)
	}

	return message, nil
}

// Lifestyle Functions

func (r *queryResolver) GetCategories(ctx context.Context, id int, limit *int, first *int, last *int) ([]model.Categories, error) {
	request, err := r.lifestyleClient.GetCategory(ctx, &lifestyleFunc.CategoriesRequest{Id: int64(id)})
	response := []model.Categories{}
	if err != nil {
		graph.AddErrorf(ctx, "Server Error!")
		return nil, err
	}
	b, _ := json.Marshal(request.Categories)
	json.Unmarshal(b, &response)
	for key, el := range response {
		for key1, elem := range el.Services {
			b2bRequest, err := r.b2bClient.GetOfficeById(ctx, &b2b.IdRequest{Id: int64(elem.ID)})
			fmt.Println(err, "errrrrrrrrror")
			if err == nil {
				response[key].Services[key1].Address = b2bRequest.Street
				response[key].Services[key1].OfficeName = b2bRequest.Name
				response[key].Services[key1].URL = "https://storage.googleapis.com/unie-assets/153526888104a96c"
			}
		}
	}

	return response, nil
}
func (r *queryResolver) GetServices(ctx context.Context, id int, limit int, first *int, last *int) ([]model.Services, error) {
	request, err := r.lifestyleClient.GetServices(ctx, &lifestyleFunc.ServicesRequest{Id: int64(id)})
	response := []model.Services{}
	if err != nil {
		graph.AddErrorf(ctx, "Server Error!")
		return nil, err
	}
	for _, el := range request.Services {
		b2bRequest, err := r.b2bClient.GetOfficeById(ctx, &b2b.IdRequest{Id: el.Id})
		if err == nil {
			service := model.Services{}
			service.ID = int(el.Id)
			service.CategoryName = el.CategoryName
			service.Address = b2bRequest.Street
			service.OfficeName = b2bRequest.Name
			service.URL = "https://storage.googleapis.com/unie-assets/153526888104a96c"
			response = append(response, service)
		}
	}
	return response, nil
}
func (r *queryResolver) GetInformation(ctx context.Context, id int) (model.Office, error) {
	request, err := r.lifestyleClient.GetInformation(ctx, &lifestyleFunc.BranchRequest{Id: int64(id)})
	response := model.Office{}
	if err != nil {
		graph.AddErrorf(ctx, "Server Error!")
		return model.Office{}, err
	}
	b2bRequest, err := r.b2bClient.GetOfficeById(ctx, &b2b.IdRequest{Id: int64(id)})
	fmt.Println("erweqre", b2bRequest)
	if err == nil {
		response.ID = int(b2bRequest.Id)
		response.Name = b2bRequest.Name
		response.Description = b2bRequest.Description
		response.FullName = b2bRequest.FullName
		response.Number = b2bRequest.Number
		response.Street = b2bRequest.Street
		response.Lon = b2bRequest.Lon
		response.Lat = b2bRequest.Lat
		response.PostCode = b2bRequest.PostCode
		response.BuildingCode = b2bRequest.BuildingCode
		response.CityID = int(b2bRequest.City)
		response.PartnerID = int(b2bRequest.Partner)
	}
	b2bRequest1, err := r.b2bClient.GetSheduleByOfficeId(ctx, &b2b.IdRequest{Id: int64(id)})
	days := []model.Day{}
	fmt.Println(err)
	for _, el := range b2bRequest1.Shedule {
		day := model.Day{
			ID:    int(el.Day),
			Start: int(el.Start),
			End:   int(el.End),
		}
		days = append(days, day)
	}
	response.Days = days

	for _, el := range request.ExampleWorks {
		exampleWork := model.ExampleWork{
			URL:         el.Url,
			Description: el.Description,
		}
		response.ExampleWorks = append(response.ExampleWorks, exampleWork)
	}

	for _, el := range request.Services {
		service := model.ServiceOffice{
			ID:    int(el.Id),
			Name:  el.Name,
			Time:  int(el.Time),
			Price: int(el.Price),
		}
		response.Services = append(response.Services, service)
	}

	return response, nil
}
func (r *queryResolver) GetShedules(ctx context.Context, id int) (model.Shedule, error) {
	request, err := r.lifestyleClient.GetShedules(ctx, &lifestyleFunc.BranchRequest{Id: int64(id)})
	response := model.Shedule{}
	if err != nil {
		graph.AddErrorf(ctx, "Server Error!")
		return model.Shedule{}, err
	}
	b2bRequest, err := r.b2bClient.GetSheduleByOfficeId(ctx, &b2b.IdRequest{Id: request.Masters[0].OfficeId})
	fmt.Println("office", request.Masters[0].OfficeId, err)
	days := []model.Day{}
	for _, el := range b2bRequest.Shedule {
		day := model.Day{
			ID:    int(el.Day),
			Start: int(el.Start),
			End:   int(el.End),
		}
		days = append(days, day)
	}
	response.Days = days
	fmt.Println("asdfasdf")
	for _, el := range request.Masters {
		fmt.Println("asdfasdf")
		master := model.Master{
			ID:         int(el.Id),
			FirstName:  el.FirstName,
			LastName:   el.LastName,
			Patronymic: el.Patronymic,
		}
		booksModel := []model.BookLifeStyle{}
		for _, el := range el.TimeWorks {
			books, err := r.orderClient.GetOrdersByMaster(ctx, &orderFunc.OrderMasterReq{Id: el.Id, Label: "LIFESTYLE"})
			fmt.Println("boook", books)
			if err != nil {
				graph.AddErrorf(ctx, "Server order Error!")
				return model.Shedule{}, err
			}
			for _, bEl := range books.Books {
				book := model.BookLifeStyle{
					ID:    int(el.Id),
					Start: bEl.StartDate,
					End:   bEl.EndDate,
					Long:  int(el.Time),
					Price: int(el.Price),
				}
				booksModel = append(booksModel, book)
			}
		}
		master.Book = booksModel
		response.Masters = append(response.Masters, master)
	}
	return response, nil
}

// Finance Functions

func (r *queryResolver) GetLoan(ctx context.Context, id int) (model.Loan, error) {
	twRequest := &rpc.GetLoanReq{
		Id: int64(id),
	}

	response, err := r.financedkzClient.GetLoan(ctx, twRequest)
	if err != nil {
		graph.AddErrorf(ctx, "something went worng")
		return model.Loan{}, nil
	}
	gResponse := model.Loan{
		ID:                   int(response.GetId()),
		Name:                 response.GetName(),
		MinAge:               int(response.GetMinAge()),
		MaxAge:               int(response.GetMaxAge()),
		MinLoanTerm:          int(response.GetMinLoanTerm()),
		MaxLoanTerm:          int(response.GetMaxLoanTerm()),
		EarlyRepayment:       response.GetEarlyRepayment(),
		EarningsConfirmation: response.GetEarningsConfirmation(),
		WorkExperience:       int(response.GetWorkExperience()),
		LastWorkExperience:   int(response.GetLastWorkExperience()),
		Currency:             response.GetCurrency(),
		Guarantor:            response.GetGuarantor(),
		Comisson:             int(response.GetComission()),
		MinAmount:            int(response.GetMinAmount()),
		MaxAmount:            int(response.GetMaxAmount()),
		PicturePath:          response.GetPicturePath(),
		Pawn:                 response.GetPawn(),
		Partner:              int(response.GetPartner()),
		Description:          response.GetDescription(),
		PartnerLogo:          response.GetPartnerLogo(),
	}
	for id, aim := range response.GetAims() {
		log.Println(id)
		gResponse.Aims = append(gResponse.Aims, model.Aim{
			ID:   int(aim.GetId()),
			Name: aim.GetName(),
		})
	}
	for id, city := range response.GetCities() {
		log.Print(id)
		gResponse.Cities = append(gResponse.Cities, model.City{
			ID:   int(city.GetId()),
			Name: city.GetName(),
		})
	}
	for id, pawn := range response.GetPawns() {
		log.Print(id)
		gResponse.Pawns = append(gResponse.Pawns, model.Pawn{
			ID:   int(pawn.GetId()),
			Name: pawn.GetName(),
		})
	}
	for id, occupation := range response.GetOccupations() {
		log.Print(id)
		gResponse.Occupations = append(gResponse.Occupations, model.Occupation{
			ID:   int(occupation.GetId()),
			Name: occupation.GetName(),
		})
	}
	for id, history := range response.GetHistories() {
		log.Print(id)
		gResponse.Histories = append(gResponse.Histories, model.History{
			ID:   int(history.GetId()),
			Name: history.GetName(),
		})
	}
	for id, scheme := range response.GetPaymentSchemes() {
		log.Print(id)
		gResponse.PaymentSchemes = append(gResponse.PaymentSchemes, model.PaymentScheme{
			ID:   int(scheme.GetId()),
			Name: scheme.GetName(),
		})
	}
	log.Println(gResponse)
	return gResponse, nil
}
func (r *queryResolver) GetCreditCard(ctx context.Context, id int) (model.CreditCard, error) {
	twRequest := &rpc.GetCreditCardReq{
		Id: int64(id),
	}
	twResponse, err := r.financedkzClient.GetCreditCard(ctx, twRequest)
	if err != nil {
		graph.AddErrorf(ctx, "something went worng")
		return model.CreditCard{}, err
	}
	response := model.CreditCard{
		ID:                  int(twResponse.GetId()),
		Name:                twResponse.GetName(),
		EarningConfirmation: twResponse.GetEarningConfirmation(),
		Bonuses:             twResponse.GetBonuses(),
		GracePeriod:         twResponse.GetGracePeriod(),
		Secure3D:            twResponse.GetSecure3D(),
		Visa:                twResponse.GetVisa(),
		MasterCard:          twResponse.GetMasterCard(),
		Amex:                twResponse.GetAmex(),
		PlayMarket:          twResponse.GetPlayMarket(),
		AppStore:            twResponse.GetAppStore(),
		Overdraft:           twResponse.GetOverdraft(),
		PayWave:             twResponse.GetPayWave(),
		PicturePath:         twResponse.GetPicturePath(),
		DeliveryCost:        int(twResponse.GetDeliveryCost()),
		OverdrawningCost:    int(twResponse.GetOverdrawningCost()),
		AmountLimit:         int(twResponse.GetAmountLimit()),
		ServiceCost:         int(twResponse.GetServiceCost()),
		SmsCost:             int(twResponse.GetSmsCost()),
		Partner:             int(twResponse.GetPartner()),
		Description:         twResponse.GetDescription(),
		PartnerLogo:         twResponse.GetPartnerLogo(),
	}
	for id, city := range twResponse.GetCities() {
		log.Print(id)
		response.Cities = append(response.Cities, model.City{
			ID:   int(city.GetId()),
			Name: city.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) GetDebitCard(ctx context.Context, id int) (model.DebitCard, error) {
	twRequest := &rpc.GetDebitCardReq{
		Id: int64(id),
	}
	twResponse, err := r.financedkzClient.GetDebitCard(ctx, twRequest)
	if err != nil {
		graph.AddErrorf(ctx, "something bad")
		return model.DebitCard{}, err
	}
	response := model.DebitCard{
		ID:                int(twResponse.GetId()),
		Name:              twResponse.GetName(),
		Overdraft:         twResponse.GetOverdraft(),
		Bonuses:           twResponse.GetBonuses(),
		PicturePath:       twResponse.GetPicturePath(),
		SmsCost:           int(twResponse.GetSmsCost()),
		ServiceCost:       int(twResponse.GetServiceCost()),
		DeliveryCost:      int(twResponse.GetDeliveryCost()),
		OverdrawningCost:  int(twResponse.GetOverdrawningCost()),
		MinCreditInterest: float64(twResponse.GetMinCreditInterest()),
		MaxCreditInterest: float64(twResponse.GetMaxCreditInterest()),
		MinCashback:       float64(twResponse.GetMinCashback()),
		MaxCashback:       float64(twResponse.GetMaxCashback()),
		Partner:           int(twResponse.GetPartner()),
		Description:       twResponse.GetDescription(),
		PartnerLogo:       twResponse.GetPartnerLogo(),
	}
	for id, city := range twResponse.GetCities() {
		log.Print(id)
		response.Cities = append(response.Cities, model.City{
			ID:   int(city.GetId()),
			Name: city.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) GetCity(ctx context.Context, id int) (model.City, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetCity(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something goes bady bad")
		return model.City{}, err
	}
	response := model.City{
		ID:   int(twRes.GetId()),
		Name: twRes.GetName(),
	}
	return response, nil
}
func (r *queryResolver) GetAim(ctx context.Context, id int) (model.Aim, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetAim(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something goes bady bad")
		return model.Aim{}, err
	}
	response := model.Aim{
		ID:   int(twRes.GetId()),
		Name: twRes.GetName(),
	}
	return response, nil
}
func (r *queryResolver) GetOccupation(ctx context.Context, id int) (model.Occupation, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetOccupation(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something goes bady bad")
		return model.Occupation{}, err
	}
	response := model.Occupation{
		ID:   int(twRes.GetId()),
		Name: twRes.GetName(),
	}
	return response, nil
}
func (r *queryResolver) GetPawn(ctx context.Context, id int) (model.Pawn, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetPawn(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something goes bady bad")
		return model.Pawn{}, err
	}
	response := model.Pawn{
		ID:   int(twRes.GetId()),
		Name: twRes.GetName(),
	}
	return response, nil
}
func (r *queryResolver) GetHistory(ctx context.Context, id int) (model.History, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetHistory(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something goes bady bad")
		return model.History{}, err
	}
	response := model.History{
		ID:   int(twRes.GetId()),
		Name: twRes.GetName(),
	}
	return response, nil
}
func (r *queryResolver) GetPaymentScheme(ctx context.Context, id int) (model.PaymentScheme, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetScheme(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something goes bady bad")
		return model.PaymentScheme{}, err
	}
	response := model.PaymentScheme{
		ID:   int(twRes.GetId()),
		Name: twRes.GetName(),
	}
	return response, nil
}
func (r *queryResolver) ListLoans(ctx context.Context) ([]model.Loan, error) {
	twReq := &rpc.ListLoansReq{}
	twRes, err := r.financedkzClient.ListLoans(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something bad")
		return []model.Loan{}, err
	}
	var response []model.Loan
	for i, loan := range twRes.GetLoans() {
		log.Println(i)
		qLoan := model.Loan{
			ID:                   int(loan.GetId()),
			Name:                 loan.GetName(),
			MinAge:               int(loan.GetMinAge()),
			MaxAge:               int(loan.GetMaxAge()),
			MinLoanTerm:          int(loan.GetMinLoanTerm()),
			MaxLoanTerm:          int(loan.GetMaxLoanTerm()),
			EarlyRepayment:       loan.GetEarlyRepayment(),
			EarningsConfirmation: loan.GetEarningsConfirmation(),
			WorkExperience:       int(loan.GetWorkExperience()),
			LastWorkExperience:   int(loan.GetLastWorkExperience()),
			Currency:             loan.GetCurrency(),
			Guarantor:            loan.GetGuarantor(),
			Comisson:             int(loan.GetComission()),
			MinAmount:            int(loan.GetMinAmount()),
			MaxAmount:            int(loan.GetMaxAmount()),
			PicturePath:          loan.GetPicturePath(),
			Pawn:                 loan.GetPawn(),
			Partner:              int(loan.GetPartner()),
			Rate:                 loan.GetRate(),
			MinApprovalDays:      int(loan.GetMinApprovalDays()),
			MaxApprovalDays:      int(loan.GetMaxApprovalDays()),
			Description:          loan.GetDescription(),
			PartnerLogo:          loan.GetPartnerLogo(),
		}
		for id, aim := range loan.GetAims() {
			log.Println(id)
			qLoan.Aims = append(qLoan.Aims, model.Aim{
				ID:   int(aim.GetId()),
				Name: aim.GetName(),
			})
		}
		for id, city := range loan.GetCities() {
			log.Print(id)
			qLoan.Cities = append(qLoan.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		for id, pawn := range loan.GetPawns() {
			log.Print(id)
			qLoan.Pawns = append(qLoan.Pawns, model.Pawn{
				ID:   int(pawn.GetId()),
				Name: pawn.GetName(),
			})
		}
		for id, occupation := range loan.GetOccupations() {
			log.Print(id)
			qLoan.Occupations = append(qLoan.Occupations, model.Occupation{
				ID:   int(occupation.GetId()),
				Name: occupation.GetName(),
			})
		}
		for id, history := range loan.GetHistories() {
			log.Print(id)
			qLoan.Histories = append(qLoan.Histories, model.History{
				ID:   int(history.GetId()),
				Name: history.GetName(),
			})
		}
		for id, scheme := range loan.GetPaymentSchemes() {
			log.Print(id)
			qLoan.PaymentSchemes = append(qLoan.PaymentSchemes, model.PaymentScheme{
				ID:   int(scheme.GetId()),
				Name: scheme.GetName(),
			})
		}
		response = append(response, qLoan)
	}
	return response, nil
}
func (r *queryResolver) ListLoansByPartnerID(ctx context.Context, partner int) ([]model.Loan, error) {
	twReq := &rpc.ListLoansByPartnerIdReq{
		Partner: int64(partner),
	}
	twRes, err := r.financedkzClient.ListLoansByPartnerId(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "something bad")
		return []model.Loan{}, err
	}
	var response []model.Loan
	for i, loan := range twRes.GetLoans() {
		log.Println(i)
		qLoan := model.Loan{
			ID:                   int(loan.GetId()),
			Name:                 loan.GetName(),
			MinAge:               int(loan.GetMinAge()),
			MaxAge:               int(loan.GetMaxAge()),
			MinLoanTerm:          int(loan.GetMinLoanTerm()),
			MaxLoanTerm:          int(loan.GetMaxLoanTerm()),
			EarlyRepayment:       loan.GetEarlyRepayment(),
			EarningsConfirmation: loan.GetEarningsConfirmation(),
			WorkExperience:       int(loan.GetWorkExperience()),
			LastWorkExperience:   int(loan.GetLastWorkExperience()),
			Currency:             loan.GetCurrency(),
			Guarantor:            loan.GetGuarantor(),
			Comisson:             int(loan.GetComission()),
			MinAmount:            int(loan.GetMinAmount()),
			MaxAmount:            int(loan.GetMaxAmount()),
			PicturePath:          loan.GetPicturePath(),
			Pawn:                 loan.GetPawn(),
			Partner:              int(loan.GetPartner()),
			Rate:                 loan.GetRate(),
			MinApprovalDays:      int(loan.GetMinApprovalDays()),
			MaxApprovalDays:      int(loan.GetMaxApprovalDays()),
			Description:          loan.GetDescription(),
			PartnerLogo:          loan.GetPartnerLogo(),
		}
		for id, aim := range loan.GetAims() {
			log.Println(id)
			qLoan.Aims = append(qLoan.Aims, model.Aim{
				ID:   int(aim.GetId()),
				Name: aim.GetName(),
			})
		}
		for id, city := range loan.GetCities() {
			log.Print(id)
			qLoan.Cities = append(qLoan.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		for id, pawn := range loan.GetPawns() {
			log.Print(id)
			qLoan.Pawns = append(qLoan.Pawns, model.Pawn{
				ID:   int(pawn.GetId()),
				Name: pawn.GetName(),
			})
		}
		for id, occupation := range loan.GetOccupations() {
			log.Print(id)
			qLoan.Occupations = append(qLoan.Occupations, model.Occupation{
				ID:   int(occupation.GetId()),
				Name: occupation.GetName(),
			})
		}
		for id, history := range loan.GetHistories() {
			log.Print(id)
			qLoan.Histories = append(qLoan.Histories, model.History{
				ID:   int(history.GetId()),
				Name: history.GetName(),
			})
		}
		for id, scheme := range loan.GetPaymentSchemes() {
			log.Print(id)
			qLoan.PaymentSchemes = append(qLoan.PaymentSchemes, model.PaymentScheme{
				ID:   int(scheme.GetId()),
				Name: scheme.GetName(),
			})
		}
		response = append(response, qLoan)
	}
	return response, nil
}
func (r *queryResolver) ListCreditCards(ctx context.Context) ([]model.CreditCard, error) {
	twReq := &rpc.ListCreditCardsReq{}
	twRes, err := r.financedkzClient.ListCreditCards(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Baaaaaaad")
	}
	var response []model.CreditCard
	for i, creditCard := range twRes.GetCreditCards() {
		log.Print(i)
		item := model.CreditCard{
			ID:                  int(creditCard.GetId()),
			Name:                creditCard.GetName(),
			EarningConfirmation: creditCard.GetEarningConfirmation(),
			Bonuses:             creditCard.GetBonuses(),
			GracePeriod:         creditCard.GetGracePeriod(),
			Secure3D:            creditCard.GetSecure3D(),
			Visa:                creditCard.GetVisa(),
			MasterCard:          creditCard.GetMasterCard(),
			Amex:                creditCard.GetAmex(),
			PlayMarket:          creditCard.GetPlayMarket(),
			AppStore:            creditCard.GetAppStore(),
			Overdraft:           creditCard.GetOverdraft(),
			PayWave:             creditCard.GetPayWave(),
			PicturePath:         creditCard.GetPicturePath(),
			DeliveryCost:        int(creditCard.GetDeliveryCost()),
			OverdrawningCost:    int(creditCard.GetOverdrawningCost()),
			AmountLimit:         int(creditCard.GetAmountLimit()),
			ServiceCost:         int(creditCard.GetServiceCost()),
			SmsCost:             int(creditCard.GetSmsCost()),
			Partner:             int(creditCard.GetPartner()),
			Description:         creditCard.GetDescription(),
			PartnerLogo:         creditCard.GetPartnerLogo(),
		}
		for id, city := range creditCard.GetCities() {
			log.Print(id)
			item.Cities = append(item.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		response = append(response, item)
	}
	return response, nil
}
func (r *queryResolver) ListCreditCardsByPartnerID(ctx context.Context, partner int) ([]model.CreditCard, error) {
	twReq := &rpc.ListCreditCardsByPartnerIdReq{
		Partner: int64(partner),
	}
	twRes, err := r.financedkzClient.ListCreditCardsByPartnerId(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Baaaaaaad")
	}
	var response []model.CreditCard
	for i, creditCard := range twRes.GetCreditCards() {
		log.Print(i)
		item := model.CreditCard{
			ID:                  int(creditCard.GetId()),
			Name:                creditCard.GetName(),
			EarningConfirmation: creditCard.GetEarningConfirmation(),
			Bonuses:             creditCard.GetBonuses(),
			GracePeriod:         creditCard.GetGracePeriod(),
			Secure3D:            creditCard.GetSecure3D(),
			Visa:                creditCard.GetVisa(),
			MasterCard:          creditCard.GetMasterCard(),
			Amex:                creditCard.GetAmex(),
			PlayMarket:          creditCard.GetPlayMarket(),
			AppStore:            creditCard.GetAppStore(),
			Overdraft:           creditCard.GetOverdraft(),
			PayWave:             creditCard.GetPayWave(),
			PicturePath:         creditCard.GetPicturePath(),
			DeliveryCost:        int(creditCard.GetDeliveryCost()),
			OverdrawningCost:    int(creditCard.GetOverdrawningCost()),
			AmountLimit:         int(creditCard.GetAmountLimit()),
			ServiceCost:         int(creditCard.GetServiceCost()),
			SmsCost:             int(creditCard.GetSmsCost()),
			Partner:             int(creditCard.GetPartner()),
			Description:         creditCard.GetDescription(),
			PartnerLogo:         creditCard.GetPartnerLogo(),
		}
		for id, city := range creditCard.GetCities() {
			log.Print(id)
			item.Cities = append(item.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		response = append(response, item)
	}
	return response, nil
}
func (r *queryResolver) ListDebitCards(ctx context.Context) ([]model.DebitCard, error) {
	twReq := &rpc.ListDebitCardsReq{}
	twRes, err := r.financedkzClient.ListDebitCards(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "sorry ")
	}
	var response []model.DebitCard
	for i, debitCard := range twRes.GetDebitCards() {
		log.Print(i)
		item := model.DebitCard{
			ID:                int(debitCard.GetId()),
			Name:              debitCard.GetName(),
			Overdraft:         debitCard.GetOverdraft(),
			Bonuses:           debitCard.GetBonuses(),
			PicturePath:       debitCard.GetPicturePath(),
			SmsCost:           int(debitCard.GetSmsCost()),
			ServiceCost:       int(debitCard.GetServiceCost()),
			DeliveryCost:      int(debitCard.GetDeliveryCost()),
			OverdrawningCost:  int(debitCard.GetOverdrawningCost()),
			MinCreditInterest: float64(debitCard.GetMinCreditInterest()),
			MaxCreditInterest: float64(debitCard.GetMaxCreditInterest()),
			MinCashback:       float64(debitCard.GetMinCashback()),
			MaxCashback:       float64(debitCard.GetMaxCashback()),
			Partner:           int(debitCard.GetPartner()),
			Description:       debitCard.GetDescription(),
			PartnerLogo:       debitCard.GetPartnerLogo(),
		}
		for id, city := range debitCard.GetCities() {
			log.Print(id)
			item.Cities = append(item.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		response = append(response, item)
	}
	return response, nil
}
func (r *queryResolver) ListDebitCardsByPartnerID(ctx context.Context, partner int) ([]model.DebitCard, error) {
	twReq := &rpc.ListDebitCardsByPartnerIdReq{
		Partner: int64(partner),
	}
	twRes, err := r.financedkzClient.ListDebitCardsByPartnerId(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "sorry ")
	}
	var response []model.DebitCard
	for i, debitCard := range twRes.GetDebitCards() {
		log.Print(i)
		item := model.DebitCard{
			ID:                int(debitCard.GetId()),
			Name:              debitCard.GetName(),
			Overdraft:         debitCard.GetOverdraft(),
			Bonuses:           debitCard.GetBonuses(),
			PicturePath:       debitCard.GetPicturePath(),
			SmsCost:           int(debitCard.GetSmsCost()),
			ServiceCost:       int(debitCard.GetServiceCost()),
			DeliveryCost:      int(debitCard.GetDeliveryCost()),
			OverdrawningCost:  int(debitCard.GetOverdrawningCost()),
			MinCreditInterest: float64(debitCard.GetMinCreditInterest()),
			MaxCreditInterest: float64(debitCard.GetMaxCreditInterest()),
			MinCashback:       float64(debitCard.GetMinCashback()),
			MaxCashback:       float64(debitCard.GetMaxCashback()),
			Partner:           int(debitCard.GetPartner()),
			Description:       debitCard.GetDescription(),
			PartnerLogo:       debitCard.GetPartnerLogo(),
		}
		for id, city := range debitCard.GetCities() {
			log.Print(id)
			item.Cities = append(item.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		response = append(response, item)
	}
	return response, nil
}
func (r *queryResolver) FilterLoans(ctx context.Context, loanFilters *model.LoanFilters) ([]model.Loan, error) {
	log.Println(loanFilters)
	twReq := &rpc.FilterLoansReq{}
	if loanFilters != nil {
		if loanFilters.CityID != nil {
			twReq.CityId = int64(*loanFilters.CityID)
		}
		if loanFilters.AimID != nil {
			twReq.AimId = int64(*loanFilters.AimID)
		}
		if loanFilters.PawnID != nil {
			twReq.PawnId = int64(*loanFilters.PawnID)
		}
		if loanFilters.OccupationID != nil {
			twReq.OccupationId = int64(*loanFilters.OccupationID)
		}
		if loanFilters.SchemeID != nil {
			twReq.SchemeId = int64(*loanFilters.SchemeID)
		}
		if loanFilters.HistoryID != nil {
			twReq.SchemeId = int64(*loanFilters.HistoryID)
		}
		if loanFilters.Amount != nil {
			twReq.Amount = int64(*loanFilters.Amount)
		}
		if loanFilters.EarlyRepayment != nil {
			twReq.EarlyRepayment = *loanFilters.EarlyRepayment
		}
		if loanFilters.EarningsConfirmation != nil {
			twReq.EarningsConfirmation = *loanFilters.EarningsConfirmation
		}
		if loanFilters.LoanTerm != nil {
			twReq.LoanTerm = int64(*loanFilters.LoanTerm)
		}
		if loanFilters.Comisson != nil {
			twReq.Comission = *loanFilters.Comisson
		} else {
			twReq.Comission = true
		}
	}

	twRes, err := r.financedkzClient.FilterLoans(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "My bbd lol")

	}
	var response []model.Loan
	for i, loan := range twRes.GetLoans() {
		log.Println(i)
		qLoan := model.Loan{
			ID:                   int(loan.GetId()),
			Name:                 loan.GetName(),
			MinAge:               int(loan.GetMinAge()),
			MaxAge:               int(loan.GetMaxAge()),
			MinLoanTerm:          int(loan.GetMinLoanTerm()),
			MaxLoanTerm:          int(loan.GetMaxLoanTerm()),
			EarlyRepayment:       loan.GetEarlyRepayment(),
			EarningsConfirmation: loan.GetEarningsConfirmation(),
			WorkExperience:       int(loan.GetWorkExperience()),
			LastWorkExperience:   int(loan.GetLastWorkExperience()),
			Currency:             loan.GetCurrency(),
			Guarantor:            loan.GetGuarantor(),
			Comisson:             int(loan.GetComission()),
			MinAmount:            int(loan.GetMinAmount()),
			MaxAmount:            int(loan.GetMaxAmount()),
			PicturePath:          loan.GetPicturePath(),
			Pawn:                 loan.GetPawn(),
			Partner:              int(loan.GetPartner()),
			Rate:                 loan.GetRate(),
			MinApprovalDays:      int(loan.GetMinApprovalDays()),
			MaxApprovalDays:      int(loan.GetMaxApprovalDays()),
			Description:          loan.GetDescription(),
			PartnerLogo:          loan.GetPartnerLogo(),
		}
		for id, aim := range loan.GetAims() {
			log.Println(id)
			qLoan.Aims = append(qLoan.Aims, model.Aim{
				ID:   int(aim.GetId()),
				Name: aim.GetName(),
			})
		}
		for id, city := range loan.GetCities() {
			log.Print(id)
			qLoan.Cities = append(qLoan.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		for id, pawn := range loan.GetPawns() {
			log.Print(id)
			qLoan.Pawns = append(qLoan.Pawns, model.Pawn{
				ID:   int(pawn.GetId()),
				Name: pawn.GetName(),
			})
		}
		for id, occupation := range loan.GetOccupations() {
			log.Print(id)
			qLoan.Occupations = append(qLoan.Occupations, model.Occupation{
				ID:   int(occupation.GetId()),
				Name: occupation.GetName(),
			})
		}
		for id, history := range loan.GetHistories() {
			log.Print(id)
			qLoan.Histories = append(qLoan.Histories, model.History{
				ID:   int(history.GetId()),
				Name: history.GetName(),
			})
		}
		for id, scheme := range loan.GetPaymentSchemes() {
			log.Print(id)
			qLoan.PaymentSchemes = append(qLoan.PaymentSchemes, model.PaymentScheme{
				ID:   int(scheme.GetId()),
				Name: scheme.GetName(),
			})
		}
		response = append(response, qLoan)
	}
	return response, nil
}
func (r *queryResolver) FilterCreditCards(ctx context.Context, creditCardFilters *model.CreditCardFilters) ([]model.CreditCard, error) {
	twReq := &rpc.FilterCreditCardsReq{}
	if creditCardFilters != nil {
		if creditCardFilters.CityID != nil {
			twReq.CityId = int64(*creditCardFilters.CityID)
		}
		if creditCardFilters.Bonuses != nil {
			twReq.Bonuses = *creditCardFilters.Bonuses
		}
		if creditCardFilters.GracePeriod != nil {
			twReq.GracePeriod = *creditCardFilters.GracePeriod
		}
		if creditCardFilters.Visa != nil {
			twReq.Visa = *creditCardFilters.Visa
		}
		if creditCardFilters.MasterCard != nil {
			twReq.MasterCard = *creditCardFilters.MasterCard
		}
		if creditCardFilters.Amex != nil {
			twReq.Amex = *creditCardFilters.Amex
		}
		if creditCardFilters.PlayMarket != nil {
			twReq.PlayMarket = *creditCardFilters.PlayMarket
		}
		if creditCardFilters.AppStore != nil {
			twReq.AppStore = *creditCardFilters.AppStore
		}
		if creditCardFilters.Overdraft != nil {
			twReq.Overdraft = *creditCardFilters.Overdraft
		}
		if creditCardFilters.PayWave != nil {
			twReq.PayWave = *creditCardFilters.PayWave
		}
	}
	twRes, err := r.financedkzClient.FilterCreditCards(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Got some error")
	}
	var response []model.CreditCard
	for i, creditCard := range twRes.GetCreditCards() {
		log.Print(i)
		item := model.CreditCard{
			ID:                  int(creditCard.GetId()),
			Name:                creditCard.GetName(),
			EarningConfirmation: creditCard.GetEarningConfirmation(),
			Bonuses:             creditCard.GetBonuses(),
			GracePeriod:         creditCard.GetGracePeriod(),
			Secure3D:            creditCard.GetSecure3D(),
			Visa:                creditCard.GetVisa(),
			MasterCard:          creditCard.GetMasterCard(),
			Amex:                creditCard.GetAmex(),
			PlayMarket:          creditCard.GetPlayMarket(),
			AppStore:            creditCard.GetAppStore(),
			Overdraft:           creditCard.GetOverdraft(),
			PayWave:             creditCard.GetPayWave(),
			PicturePath:         creditCard.GetPicturePath(),
			DeliveryCost:        int(creditCard.GetDeliveryCost()),
			OverdrawningCost:    int(creditCard.GetOverdrawningCost()),
			AmountLimit:         int(creditCard.GetAmountLimit()),
			ServiceCost:         int(creditCard.GetServiceCost()),
			SmsCost:             int(creditCard.GetSmsCost()),
			Partner:             int(creditCard.GetPartner()),
			Description:         creditCard.GetDescription(),
			PartnerLogo:         creditCard.GetPartnerLogo(),
		}
		for id, city := range creditCard.GetCities() {
			log.Print(id)
			item.Cities = append(item.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		response = append(response, item)
	}
	return response, nil
}
func (r *queryResolver) FilterDebitCards(ctx context.Context, debitCardFilters *model.DebitCardFilters) ([]model.DebitCard, error) {
	log.Println("Debito")
	twReq := &rpc.FilterDebitCardsReq{}
	if debitCardFilters != nil {
		if debitCardFilters.CityID != nil {
			twReq.CityId = int64(*debitCardFilters.CityID)
		}
		if debitCardFilters.Overdraft != nil {
			twReq.Overdraft = *debitCardFilters.Overdraft
		}
		if debitCardFilters.Bonuses != nil {
			twReq.Bonuses = *debitCardFilters.Bonuses
		}
		if debitCardFilters.FreeSms != nil {
			twReq.FreeSms = *debitCardFilters.FreeSms
		}
		if debitCardFilters.FreeService != nil {
			twReq.FreeService = *debitCardFilters.FreeService
		}
		if debitCardFilters.FreeDelivery != nil {
			twReq.FreeDelivery = *debitCardFilters.FreeDelivery
		}
		if debitCardFilters.FreeOverdraw != nil {
			twReq.FreeOverdraw = *debitCardFilters.FreeOverdraw
		}
		if debitCardFilters.Cashback != nil {
			twReq.Cashback = *debitCardFilters.Cashback
		}
	}
	log.Println("ASSEMBLED")
	twRes, err := r.financedkzClient.FilterDebitCards(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "ERRORR!")
	}
	var response []model.DebitCard
	for i, debitCard := range twRes.GetDebitCards() {
		log.Print(i)
		item := model.DebitCard{
			ID:                int(debitCard.GetId()),
			Name:              debitCard.GetName(),
			Overdraft:         debitCard.GetOverdraft(),
			Bonuses:           debitCard.GetBonuses(),
			PicturePath:       debitCard.GetPicturePath(),
			SmsCost:           int(debitCard.GetSmsCost()),
			ServiceCost:       int(debitCard.GetServiceCost()),
			DeliveryCost:      int(debitCard.GetDeliveryCost()),
			OverdrawningCost:  int(debitCard.GetOverdrawningCost()),
			MinCreditInterest: float64(debitCard.GetMinCreditInterest()),
			MaxCreditInterest: float64(debitCard.GetMaxCreditInterest()),
			MinCashback:       float64(debitCard.GetMinCashback()),
			MaxCashback:       float64(debitCard.GetMaxCashback()),
			Partner:           int(debitCard.GetPartner()),
			Description:       debitCard.GetDescription(),
			PartnerLogo:       debitCard.GetPartnerLogo(),
		}
		for id, city := range debitCard.GetCities() {
			log.Print(id)
			item.Cities = append(item.Cities, model.City{
				ID:   int(city.GetId()),
				Name: city.GetName(),
			})
		}
		response = append(response, item)
	}
	return response, nil
}
func (r *queryResolver) ListAims(ctx context.Context) ([]model.Aim, error) {
	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListAim(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something gone bad")
	}
	var response []model.Aim
	for i, item := range twRes.GetItems() {
		log.Print(i)
		response = append(response, model.Aim{
			ID:   int(item.GetId()),
			Name: item.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) ListCities(ctx context.Context) ([]model.City, error) {
	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListCity(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something gone bad")
	}
	var response []model.City
	for i, item := range twRes.GetItems() {
		log.Print(i)
		response = append(response, model.City{
			ID:   int(item.GetId()),
			Name: item.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) ListOccupations(ctx context.Context) ([]model.Occupation, error) {

	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListOccupation(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something gone bad")
	}
	var response []model.Occupation
	for i, item := range twRes.GetItems() {
		log.Print(i)
		response = append(response, model.Occupation{
			ID:   int(item.GetId()),
			Name: item.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) ListHistories(ctx context.Context) ([]model.History, error) {
	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListHistory(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something gone bad")
	}
	var response []model.History
	for i, item := range twRes.GetItems() {
		log.Print(i)
		response = append(response, model.History{
			ID:   int(item.GetId()),
			Name: item.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) ListPawns(ctx context.Context) ([]model.Pawn, error) {
	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListPawn(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something gone bad")
	}
	var response []model.Pawn
	for i, item := range twRes.GetItems() {
		log.Print(i)
		response = append(response, model.Pawn{
			ID:   int(item.GetId()),
			Name: item.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) ListPaymentSchemes(ctx context.Context) ([]model.PaymentScheme, error) {
	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListScheme(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something gone bad")
	}
	var response []model.PaymentScheme
	for i, item := range twRes.GetItems() {
		log.Print(i)
		response = append(response, model.PaymentScheme{
			ID:   int(item.GetId()),
			Name: item.GetName(),
		})
	}
	return response, nil
}
func (r *queryResolver) GetCards(ctx context.Context) ([]model.CardOut, error) {
	result, err := r.cpClient.ListCards(ctx, &cp.EmptyRequest{})
	log.Println(result)
	var resp []model.CardOut
	for k, v := range result.GetCards() {
		log.Println(k, v)
		resp = append(resp, model.CardOut{
			ID:         int(v.GetId()),
			CardHolder: v.GetCardholder(),
			ExpDate:    v.GetExpDate(),
			LastFour:   v.GetLastFour(),
			Number:     v.GetLogin(),
		})
	}
	log.Println(err)
	return resp, nil
}
func (r *queryResolver) GetCard(ctx context.Context, id int) (model.CardOut, error) {
	log.Println("IN GET CARD", ctx.Value("jwt"))
	result, err := r.cpClient.GetCard(ctx, &cp.GetCardRequest{
		Id: int64(id),
	})
	if err != nil {
		return model.CardOut{}, err
	} else {
		return model.CardOut{
			ID:         id,
			CardHolder: result.GetCardholder(),
			ExpDate:    result.GetExpDate(),
			LastFour:   result.GetLastFour(),
			Number:     result.GetLogin(),
		}, nil
	}
}
func (r *queryResolver) Credit(ctx context.Context, id *int) (model.Credit, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	result, err := r.financeClient.GetCredit(ctx, int32(*id))

	if err != nil {
		log.Println(err)
	}

	return model.Credit{
		ID:   int(result.Id),
		Name: result.Name,
	}, nil
}
func (r *queryResolver) GetInsuranceCategory(ctx context.Context, id int) (model.InsuranceCategory, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetInsuranceCategory(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something is gone bad")
	}

	return model.InsuranceCategory{
		ID:          id,
		Name:        twRes.GetName(),
		PicturePath: twRes.GetPicturePath(),
	}, nil
}
func (r *queryResolver) GetInsurance(ctx context.Context, id int) (model.Insurance, error) {
	twReq := &rpc.GetSomethingReq{
		Id: int64(id),
	}
	twRes, err := r.financedkzClient.GetInsurance(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Sorry something is gone bad")
	}
	return model.Insurance{
		ID:          int(twRes.GetId()),
		Name:        twRes.GetName(),
		Label:       twRes.GetLabel(),
		Description: twRes.GetDescription(),
		PartnerID:   int(twRes.GetPartnerId()),
		PicturePath: twRes.GetPicturePath(),
		URL:         twRes.GetUrl(),
		PartnerLogo: twRes.GetPartnerLogo(),
		Category: model.InsuranceCategory{
			ID:   int(twRes.GetCategory().GetId()),
			Name: twRes.GetCategory().GetName(),
		},
	}, nil
}
func (r *queryResolver) ListInsuranceCategories(ctx context.Context) ([]model.InsuranceCategory, error) {
	twReq := &rpc.ListSomethingReq{}
	twRes, err := r.financedkzClient.ListInsuranceCategories(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Bad response")
	}
	var response []model.InsuranceCategory
	for i, item := range twRes.GetCategories() {
		log.Print(i)
		response = append(response, model.InsuranceCategory{
			ID:          int(item.GetId()),
			Name:        item.GetName(),
			PicturePath: item.GetPicturePath(),
		})
	}
	return response, nil
}
func (r *queryResolver) ListInsurancies(ctx context.Context) ([]model.Insurance, error) {
	twReq := &rpc.ListInsuranciesReq{}
	twRes, err := r.financedkzClient.ListInsurancies(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Bad response")
	}
	var response []model.Insurance
	for i, item := range twRes.GetInsurancies() {
		log.Print(i)
		response = append(response, model.Insurance{
			ID:          int(item.GetId()),
			Name:        item.GetName(),
			PicturePath: item.GetPicturePath(),
			PartnerID:   int(item.GetPartnerId()),
			Label:       item.GetLabel(),
			Description: item.GetDescription(),
			PartnerLogo: item.GetPartnerLogo(),
			URL:         item.GetUrl(),
			Category: model.InsuranceCategory{
				ID:          int(item.GetCategory().GetId()),
				Name:        item.GetCategory().GetName(),
				PicturePath: item.GetCategory().GetPicturePath(),
			},
		})
	}
	return response, nil
}
func (r *queryResolver) ListInsuranciesByCategoryID(ctx context.Context, categoryId int) ([]model.Insurance, error) {
	twReq := &rpc.ListInsuranciesByCategoryIdReq{
		CategoryId: int64(categoryId),
	}
	twRes, err := r.financedkzClient.ListInsuranciesByCategoryId(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Bad response")
	}
	var response []model.Insurance
	for i, item := range twRes.GetInsurancies() {
		log.Print(i)
		response = append(response, model.Insurance{
			ID:          int(item.GetId()),
			Name:        item.GetName(),
			PicturePath: item.GetPicturePath(),
			PartnerID:   int(item.GetPartnerId()),
			Label:       item.GetLabel(),
			Description: item.GetDescription(),
			URL:         item.GetUrl(),
			PartnerLogo: item.GetPartnerLogo(),
			Category: model.InsuranceCategory{
				ID:          int(item.GetCategory().GetId()),
				Name:        item.GetCategory().GetName(),
				PicturePath: item.GetCategory().GetPicturePath(),
			},
		})
	}
	return response, nil
}
func (r *queryResolver) ListInsuranciesByPartnerID(ctx context.Context, partnerId int) ([]model.Insurance, error) {
	twReq := &rpc.ListInsuranciesByPartnerIdReq{
		PartnerId: int64(partnerId),
	}
	twRes, err := r.financedkzClient.ListInsuranciesByPartnerId(ctx, twReq)
	if err != nil {
		graph.AddErrorf(ctx, "Bad response")
	}
	var response []model.Insurance
	for i, item := range twRes.GetInsurancies() {
		log.Print(i)
		response = append(response, model.Insurance{
			ID:          int(item.GetId()),
			Name:        item.GetName(),
			PicturePath: item.GetPicturePath(),
			Label:       item.GetLabel(),
			Description: item.GetDescription(),
			URL:         item.GetUrl(),
			PartnerLogo: item.GetPartnerLogo(),
			PartnerID:   int(item.GetPartnerId()),
			Category: model.InsuranceCategory{
				ID:          int(item.GetCategory().GetId()),
				Name:        item.GetCategory().GetName(),
				PicturePath: item.GetCategory().GetPicturePath(),
			},
		})
	}
	return response, nil
}

// Shop Functions

func (r *queryResolver) GetCurrencies(ctx context.Context, first *int, last *int) ([]model.Currency, error) {
	res, err := http.Get("http://www.nationalbank.kz/rss/rates_all.xml")
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		fmt.Println("status code error: %d %s", res.StatusCode, res.Status)
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	result := []model.Currency{}
	doc.Find("item").Each(func(i int, s *goquery.Selection) {
		var currency model.Currency
		currency.ID = i
		currency.Name = s.Find("title").Text()
		currency.Price = s.Find("description").Text()
		currency.Qty, _ = strconv.Atoi(s.Find("quant").Text())
		currency.Index = s.Find("index").Text()
		currency.Change = s.Find("change").Text()
		result = append(result, currency)
	})
	first_el := 0
	last_el := len(result)

	if first != nil {
		first_el = *first
	}
	if last != nil {
		last_el = *last
	}
	return result[first_el:last_el], nil
}
func (r *queryResolver) GetCurrenciesByPage(ctx context.Context, page int) ([]model.Currency, error) {
	res, err := http.Get("http://www.nationalbank.kz/rss/rates_all.xml")
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		fmt.Println("status code error: %d %s", res.StatusCode, res.Status)
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	result := []model.Currency{}
	doc.Find("item").Each(func(i int, s *goquery.Selection) {
		var currency model.Currency
		currency.ID = i
		currency.Name = s.Find("title").Text()
		currency.Price = s.Find("description").Text()
		currency.Index = s.Find("index").Text()
		currency.Change = s.Find("change").Text()
		currency.Qty, _ = strconv.Atoi(s.Find("quant").Text())
		result = append(result, currency)
	})
	return result[(page-1)*4 : page*4], nil
}
func (r *queryResolver) GetCurrency(ctx context.Context, id int) ([]model.Currency, error) {
	res, err := http.Get("http://www.nationalbank.kz/rss/rates_all.xml")
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		fmt.Println("status code error: %d %s", res.StatusCode, res.Status)
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	result := []model.Currency{}
	doc.Find("item").Each(func(i int, s *goquery.Selection) {
		var currency model.Currency
		currency.ID = i
		currency.Name = s.Find("title").Text()
		currency.Price = s.Find("description").Text()
		currency.Index = s.Find("index").Text()
		currency.Change = s.Find("change").Text()
		currency.Qty, _ = strconv.Atoi(s.Find("quant").Text())
		if id == i {
			result = append(result, currency)
		}
	})
	return result, nil
}

// B2B Functions

func (r *queryResolver) AllPartners(ctx context.Context) ([]model.Partner, error) {
	var ves QueryAllPartners
	var res []model.Partner

	client := graphql.NewClient("https://b2b-taec.c9users.io:8080/graphql")

	req := graphql.NewRequest(`query{
		allPartners{
			id
			name
			bankAccount{
				legalAddress
				actualAddress
				bank
				iic
				bin
				bic
			}
			employees{
				id
				firstName
				lastName
				username
				email
			}
			roles{
				id
				name
			}

		  }
	}`)

	// req.Header.Set("x-admin-token", ctx.Value("x-admin-token").(string))

	ctx = context.Background()

	if err := client.Run(ctx, req, &ves); err != nil {
		log.Println(err)
		return nil, err
	}

	for _, p := range ves.AllPartners {

		var emp []model.Employee
		var roles []model.Role

		for _, v := range p.Roles {
			roles = append(roles, model.Role{
				ID:   v.ID,
				Name: v.Name,
			})
		}

		for _, v := range p.Employees {
			emp = append(emp, model.Employee{
				ID:        v.ID,
				FirstName: v.FirstName,
				LastName:  v.LastName,
				Username:  v.Username,
				Email:     v.Email,
			})
		}

		res = append(res, model.Partner{
			ID:        p.ID,
			Name:      p.Name,
			Employees: emp,
			Roles:     roles,
		})
	}

	return res, nil
}
func (r *queryResolver) FindPartnerByID(ctx context.Context, id string) (model.Partner, error) {

	var ves QueryFindPartnerByID

	client := graphql.NewClient("https://b2b-taec.c9users.io:8080/graphql")

	req := graphql.NewRequest(`query($key: ID!){
		findPartnerById	(id:$key){
			id
			name
			bankAccount{
				legalAddress
				actualAddress
				bank
				iic
				bin
				bic
			}
			employees{
				id
				firstName
				lastName
				username
				email
			}
			roles{
				id
				name
			}

		  }
	}`)

	req.Var("key", id)
	// req.Header.Set("x-admin-token", ctx.Value("x-admin-token").(string))

	ctx = context.Background()

	if err := client.Run(ctx, req, &ves); err != nil {
		log.Println(err)
		return model.Partner{}, err
	}
	var emp []model.Employee
	var roles []model.Role

	for _, v := range ves.FindPartnerByID.Roles {
		roles = append(roles, model.Role{
			ID:   v.ID,
			Name: v.Name,
		})
	}

	for _, v := range ves.FindPartnerByID.Employees {
		emp = append(emp, model.Employee{
			ID:        v.ID,
			FirstName: v.FirstName,
			LastName:  v.LastName,
			Username:  v.Username,
			Email:     v.Email,
		})
	}

	res := model.Partner{
		ID:   ves.FindPartnerByID.ID,
		Name: ves.FindPartnerByID.Name,
		BankAccount: model.BankAccount{
			LegalAddress:  ves.FindPartnerByID.BankAccount.LegalAddress,
			ActualAddress: ves.FindPartnerByID.BankAccount.ActualAddress,
			Bank:          ves.FindPartnerByID.BankAccount.Bank,
			Iic:           ves.FindPartnerByID.BankAccount.Iic,
			Bin:           ves.FindPartnerByID.BankAccount.Bin,
			Bic:           ves.FindPartnerByID.BankAccount.Bic,
		},
		Employees: emp,
		Roles:     roles,
	}

	return res, nil
}
func (r *queryResolver) GetPartnerByType(ctx context.Context, id int) (result []model.NewPartner, err error) {
	twirpBuffer, err := r.partnerClient.GetPartnerByType(ctx, &partnerFunc.IdRequest{Id: int64(id)})
	jsonBuffer, _ := json.Marshal(twirpBuffer.Partner)
	json.Unmarshal(jsonBuffer, &result)
	return
}
func (r *queryResolver) GetPartnerByID(ctx context.Context, id int) (result model.NewPartner, err error) {
	twirpBuffer, err := r.partnerClient.GetPartnerById(ctx, &partnerFunc.IdRequest{Id: int64(id)})
	jsonBuffer, _ := json.Marshal(twirpBuffer)
	json.Unmarshal(jsonBuffer, &result)
	return
}

// Favorits Funcions

func (r *queryResolver) GetFavorities(ctx context.Context) (response []model.Favorit, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return nil, err
	}
	client := graphql.NewClient("http://api.unie.kz/v1")
	buffer, err := r.storeClient.GetFavorite(ctx, &storeFunc.UserId{Id: int64(userId)})
	if err != nil {
		return
	}
	jsonBuffer, err := json.Marshal(buffer.List)
	if err != nil {
		return
	}
	err = json.Unmarshal(jsonBuffer, &response)
	if err != nil {
		return
	}
	count := len(response)
	for i := 0; i < count; i++ {
		req := graphql.NewRequest(`
			query ($id: Int!){
				getOfferByPartnerId(id:$id) {
					prodId {
						oid
						name
						description
					}
				}
			}
		`)
		req.Var("id", int(response[i].ProductID))
		var res ProductFromGetOrder
		err := client.Run(ctx, req, &res)
		if err != nil {
			continue
		}
		response[i].ProductID = res.GetOfferByPartnerId.OrderItems.Id
		response[i].Name = res.GetOfferByPartnerId.OrderItems.Name
		response[i].Description = res.GetOfferByPartnerId.OrderItems.Description
	}
	return
}

/* func (r *queryResolver) Pay(ctx context.Context, tokenData model.TokenPayment) (bool, error) {
	panic("not implemented")
} */

/*func (r *queryResolver) SignIn(ctx context.Context, user *model.UserSignIn) (string, error) {

	result, err := r.cpClient.SignIn(ctx, &cp.UserSignInRequest{
		Number:   user.Number,
		Password: user.Password,
	})
	log.Println(ctx.Value("jwt"))
	if err != nil {
		return "", err
	} else {
		return result.Token, nil
	}
}*/

// Bonuse

func (r *queryResolver) GetAccount(ctx context.Context, id int) (response model.Account, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return response, err
	}	
	twirpBuffer, err := r.bonuseClient.GetAccount(ctx, &bonuseFunc.GetAccountRequest{Id:int64(id), UserId:int64(userId)});
	jsonBuffer, err := json.Marshal(twirpBuffer);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}

func (r *queryResolver) GetBalance(ctx context.Context,id int) (response model.Balance, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return response, err;
	}	
	twirpBuffer, err := r.bonuseClient.GetBalance(ctx, &bonuseFunc.GetAccountRequest{Id:int64(id), UserId:int64(userId)});
	jsonBuffer, err := json.Marshal(twirpBuffer);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}
func (r *queryResolver) GetTransaction(ctx context.Context, id int) (response model.Transaction, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return response, err;
	}	
	twirpBuffer, err := r.bonuseClient.GetTransaction(ctx, &bonuseFunc.GetAccountRequest{Id:int64(id), UserId:int64(userId)});
	jsonBuffer, err := json.Marshal(twirpBuffer);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return; 
}
func (r *queryResolver) GetTransactions(ctx context.Context) (response []model.Transaction, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return response, err;
	}	
	twirpBuffer, err := r.bonuseClient.GetTransactions(ctx, &bonuseFunc.GetAccountRequest{UserId:int64(userId)});
	jsonBuffer, err := json.Marshal(twirpBuffer.Transactions);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}
func (r *queryResolver) Extract(ctx context.Context) (response []model.Extract, err error) {
	token := ctx.Value("jwt").(string)
	userId, err := jwtEncodingProfile(token)
	if err != nil {
		graph.AddErrorf(ctx, "Unoautorizated")
		return response, err;
	}	
	twirpBuffer, err := r.bonuseClient.Extract(ctx, &bonuseFunc.GetAccountRequest{UserId:int64(userId)});
	jsonBuffer, err := json.Marshal(twirpBuffer.Extract);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}


// instagramShop functions

func (r *queryResolver) GetCategoriesInstagram(ctx context.Context, partner int) (response []model.CategoryInstagram, err error) {
	twirpBuffer, err := r.instagramShopClient.GetCategories(ctx, &instagramShopFunc.EmptyRequest{Id: int64(partner)});
	if err != nil { return;}
	jsonBuffer, err := json.Marshal(twirpBuffer.Categories);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}
func (r *queryResolver) GetCategoryInstagram(ctx context.Context, id int, partner int) (response model.CategoryInstagram, err error) {
	twirpBuffer, err := r.instagramShopClient.GetCategory(ctx, &instagramShopFunc.IdRequest{Id: int64(id), Partner: int64(partner)});
	if err != nil { return;}
	jsonBuffer, err := json.Marshal(twirpBuffer);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}
func (r *queryResolver) GetProductsInstagram(ctx context.Context, partner int) (response []model.ProductInstagram, err error) {
	twirpBuffer, err := r.instagramShopClient.GetProducts(ctx, &instagramShopFunc.EmptyRequest{Id: int64(partner)});
	if err != nil { return;}
	jsonBuffer, err := json.Marshal(twirpBuffer.Products);
	if err != nil { return;}	
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}
func (r *queryResolver) GetProductCategoryIDInstagram(ctx context.Context, id int, partner int) (response []model.ProductInstagram, err error) {
	twirpBuffer, err := r.instagramShopClient.GetProductCategoryId(ctx, &instagramShopFunc.IdRequest{Id: int64(id), Partner: int64(partner)});
	if err != nil { return;}
	jsonBuffer, err := json.Marshal(twirpBuffer.Products);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}
func (r *queryResolver) GetProductInstagram(ctx context.Context, id int, partner int) (response model.ProductInstagram, err error) {
	twirpBuffer, err := r.instagramShopClient.GetProduct(ctx, &instagramShopFunc.IdRequest{Id: int64(id), Partner: int64(partner)});
	if err != nil { return;}
	jsonBuffer, err := json.Marshal(twirpBuffer);
	if err != nil { return;}
	err = json.Unmarshal(jsonBuffer, &response);
	return;
}