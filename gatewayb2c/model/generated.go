// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

type Account struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Type int    `json:"type"`
}

type Aim struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type AnswerSMS struct {
	Status bool   `json:"status"`
	Key    string `json:"key"`
}

type Balance struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Type    int    `json:"type"`
	Balance int    `json:"balance"`
}

type BankAccount struct {
	LegalAddress  string `json:"legalAddress"`
	ActualAddress string `json:"actualAddress"`
	Bank          string `json:"bank"`
	Iic           string `json:"iic"`
	Bic           string `json:"bic"`
	Bin           string `json:"bin"`
}

type Book struct {
	ID          int    `json:"id"`
	Prepaid     int    `json:"prepaid"`
	BookingDate string `json:"bookingDate"`
	StartDate   string `json:"startDate"`
	EndDate     string `json:"endDate"`
}

type BookLifeStyle struct {
	ID    int    `json:"id"`
	Start string `json:"start"`
	Long  int    `json:"long"`
	End   string `json:"end"`
	Price int    `json:"price"`
}

type CardIn struct {
	CardHolder string `json:"cardHolder"`
	Ccpacket   string `json:"ccpacket"`
	ExpDate    string `json:"expDate"`
	CardToken  string `json:"cardToken"`
	LastFour   string `json:"lastFour"`
}

type CardOut struct {
	ID         int    `json:"id"`
	CardHolder string `json:"cardHolder"`
	ExpDate    string `json:"expDate"`
	LastFour   string `json:"lastFour"`
	Number     string `json:"number"`
}

type CardOutId struct {
	ID int `json:"id"`
}

type Categories struct {
	ID       int                `json:"id"`
	Name     string             `json:"name"`
	URL      string             `json:"url"`
	Services []ServicesCategory `json:"services"`
}

type CategoryInstagram struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Tags []Tag  `json:"tags"`
}

type City struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Credit struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type CreditCard struct {
	ID                  int    `json:"id"`
	Name                string `json:"name"`
	EarningConfirmation bool   `json:"earningConfirmation"`
	Bonuses             bool   `json:"bonuses"`
	GracePeriod         bool   `json:"gracePeriod"`
	Secure3D            bool   `json:"secure3D"`
	Visa                bool   `json:"visa"`
	MasterCard          bool   `json:"masterCard"`
	Amex                bool   `json:"amex"`
	PlayMarket          bool   `json:"playMarket"`
	AppStore            bool   `json:"appStore"`
	Overdraft           bool   `json:"overdraft"`
	PayWave             bool   `json:"payWave"`
	PicturePath         string `json:"picturePath"`
	DeliveryCost        int    `json:"deliveryCost"`
	OverdrawningCost    int    `json:"overdrawningCost"`
	AmountLimit         int    `json:"amountLimit"`
	ServiceCost         int    `json:"serviceCost"`
	SmsCost             int    `json:"smsCost"`
	Partner             int    `json:"partner"`
	Cities              []City `json:"cities"`
	Description         string `json:"description"`
	PartnerLogo         string `json:"partnerLogo"`
}

type CreditCardFilters struct {
	CityID      *int  `json:"cityId"`
	Bonuses     *bool `json:"bonuses"`
	GracePeriod *bool `json:"gracePeriod"`
	Visa        *bool `json:"visa"`
	MasterCard  *bool `json:"masterCard"`
	Amex        *bool `json:"amex"`
	PlayMarket  *bool `json:"playMarket"`
	AppStore    *bool `json:"appStore"`
	Overdraft   *bool `json:"overdraft"`
	PayWave     *bool `json:"payWave"`
}

type Currency struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Price  string `json:"price"`
	Qty    int    `json:"qty"`
	Index  string `json:"index"`
	Change string `json:"change"`
}

type Date struct {
	Day   int `json:"day"`
	Month int `json:"month"`
	Year  int `json:"year"`
}

type Day struct {
	ID    int `json:"id"`
	Start int `json:"start"`
	End   int `json:"end"`
}

type DebitCard struct {
	ID                int     `json:"id"`
	Name              string  `json:"name"`
	Overdraft         bool    `json:"overdraft"`
	Bonuses           bool    `json:"bonuses"`
	PicturePath       string  `json:"picturePath"`
	SmsCost           int     `json:"smsCost"`
	ServiceCost       int     `json:"serviceCost"`
	DeliveryCost      int     `json:"deliveryCost"`
	OverdrawningCost  int     `json:"overdrawningCost"`
	MinCreditInterest float64 `json:"minCreditInterest"`
	MaxCreditInterest float64 `json:"maxCreditInterest"`
	MinCashback       float64 `json:"minCashback"`
	MaxCashback       float64 `json:"maxCashback"`
	Partner           int     `json:"partner"`
	Cities            []City  `json:"cities"`
	Description       string  `json:"description"`
	PartnerLogo       string  `json:"partnerLogo"`
}

type DebitCardFilters struct {
	CityID       *int  `json:"cityId"`
	Overdraft    *bool `json:"overdraft"`
	Bonuses      *bool `json:"bonuses"`
	FreeSms      *bool `json:"freeSms"`
	FreeService  *bool `json:"freeService"`
	FreeDelivery *bool `json:"freeDelivery"`
	FreeOverdraw *bool `json:"freeOverdraw"`
	Cashback     *bool `json:"cashback"`
}

type Employee struct {
	ID        string  `json:"id"`
	FirstName string  `json:"firstName"`
	LastName  string  `json:"lastName"`
	Username  string  `json:"username"`
	Email     string  `json:"email"`
	Role      Role    `json:"role"`
	Partner   Partner `json:"partner"`
}

type ExampleWork struct {
	URL         string `json:"url"`
	Description string `json:"description"`
}

type Extract struct {
	Date         string                `json:"date"`
	Transactions []*TransactionExtract `json:"transactions"`
}

type Favorit struct {
	ID          int    `json:"id"`
	UserID      int    `json:"userId"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ProductID   int    `json:"productId"`
}

type History struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Insurance struct {
	ID          int               `json:"id"`
	Name        string            `json:"name"`
	Description string            `json:"description"`
	Label       string            `json:"label"`
	PartnerID   int               `json:"partnerId"`
	PicturePath string            `json:"picturePath"`
	Category    InsuranceCategory `json:"category"`
	URL         string            `json:"url"`
	PartnerLogo string            `json:"partnerLogo"`
}

type InsuranceCategory struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	PicturePath string `json:"picturePath"`
}

type InsuranceCategoryInput struct {
	Name        string `json:"name"`
	PicturePath string `json:"picturePath"`
}

type InsuranceCategoryInputFull struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	PicturePath string `json:"picturePath"`
}

type InsuranceInput struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Label       string `json:"label"`
	CategoryID  int    `json:"categoryId"`
	PartnerID   int    `json:"partnerId"`
	PicturePath string `json:"picturePath"`
	URL         string `json:"url"`
}

type InsuranceInputFull struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Label       string `json:"label"`
	PartnerID   int    `json:"partnerId"`
	PicturePath string `json:"picturePath"`
	CategoryID  int    `json:"categoryId"`
	URL         string `json:"url"`
}

type Loan struct {
	ID                   int             `json:"id"`
	Name                 string          `json:"name"`
	MinAge               int             `json:"minAge"`
	MaxAge               int             `json:"maxAge"`
	MinLoanTerm          int             `json:"minLoanTerm"`
	MaxLoanTerm          int             `json:"maxLoanTerm"`
	EarlyRepayment       bool            `json:"earlyRepayment"`
	EarningsConfirmation bool            `json:"earningsConfirmation"`
	WorkExperience       int             `json:"workExperience"`
	LastWorkExperience   int             `json:"lastWorkExperience"`
	Currency             string          `json:"currency"`
	Guarantor            bool            `json:"guarantor"`
	Comisson             int             `json:"comisson"`
	MinAmount            int             `json:"minAmount"`
	MaxAmount            int             `json:"maxAmount"`
	Cities               []City          `json:"cities"`
	Aims                 []Aim           `json:"aims"`
	Histories            []History       `json:"histories"`
	Pawns                []Pawn          `json:"pawns"`
	Occupations          []Occupation    `json:"occupations"`
	PaymentSchemes       []PaymentScheme `json:"paymentSchemes"`
	PicturePath          string          `json:"picturePath"`
	Partner              int             `json:"partner"`
	Pawn                 bool            `json:"pawn"`
	Rate                 float64         `json:"rate"`
	MinApprovalDays      int             `json:"minApprovalDays"`
	MaxApprovalDays      int             `json:"maxApprovalDays"`
	Description          string          `json:"description"`
	PartnerLogo          string          `json:"partnerLogo"`
}

type LoanFilters struct {
	CityID               *int  `json:"cityId"`
	AimID                *int  `json:"aimId"`
	HistoryID            *int  `json:"historyId"`
	SchemeID             *int  `json:"schemeId"`
	PawnID               *int  `json:"pawnId"`
	OccupationID         *int  `json:"occupationId"`
	Amount               *int  `json:"amount"`
	LoanTerm             *int  `json:"loanTerm"`
	EarlyRepayment       *bool `json:"earlyRepayment"`
	EarningsConfirmation *bool `json:"earningsConfirmation"`
	Comisson             *bool `json:"comisson"`
}

type Master struct {
	ID         int             `json:"id"`
	FirstName  string          `json:"firstName"`
	LastName   string          `json:"lastName"`
	Patronymic string          `json:"patronymic"`
	Book       []BookLifeStyle `json:"book"`
}

type NewAccount struct {
	Name string `json:"name"`
	Type int    `json:"type"`
}

type NewBook struct {
	Prepaid     int    `json:"prepaid"`
	BookingDate string `json:"bookingDate"`
	StartDate   string `json:"startDate"`
	EndDate     string `json:"endDate"`
}

type NewBookLifeStyleMaster struct {
	Prepaid     int    `json:"prepaid"`
	BookingDate string `json:"bookingDate"`
	StartDate   string `json:"startDate"`
	EndDate     string `json:"endDate"`
}

type NewOffice struct {
	ID          int          `json:"id"`
	Name        string       `json:"name"`
	Description string       `json:"description"`
	Media       string       `json:"media"`
	Address     string       `json:"address"`
	Lon         string       `json:"lon"`
	Lat         string       `json:"lat"`
	Action      bool         `json:"action"`
	City        City         `json:"city"`
	Shedule     []NewShedule `json:"shedule"`
}

type NewOrder struct {
	BankID     *int            `json:"bankId"`
	FirstName  string          `json:"firstName"`
	LastName   string          `json:"lastName"`
	MiddleName string          `json:"middleName"`
	Email      string          `json:"email"`
	Phone      string          `json:"phone"`
	Address    string          `json:"address"`
	PostalCode string          `json:"postalCode"`
	City       string          `json:"city"`
	Comments   string          `json:"comments"`
	Iin        string          `json:"iin"`
	OrderItems []*NewOrderItem `json:"orderItems"`
}

type NewOrderItem struct {
	SubjectID    *int     `json:"subjectId"`
	Quantity     int      `json:"quantity"`
	Label        string   `json:"label"`
	DeliveryDate string   `json:"deliveryDate"`
	Type         int      `json:"type"`
	Book         *NewBook `json:"book"`
	Cost         *int     `json:"cost"`
}

type NewOrderItemLifeStyleMaster struct {
	SubjectID    int                    `json:"subjectId"`
	Quantity     int                    `json:"quantity"`
	Label        string                 `json:"label"`
	DeliveryDate string                 `json:"deliveryDate"`
	Type         int                    `json:"type"`
	Book         NewBookLifeStyleMaster `json:"book"`
}

type NewOrderLifeStyleMaster struct {
	BankID     *int                           `json:"bankId"`
	FirstName  string                         `json:"firstName"`
	LastName   string                         `json:"lastName"`
	MiddleName string                         `json:"middleName"`
	Email      string                         `json:"email"`
	Phone      string                         `json:"phone"`
	Address    string                         `json:"address"`
	PostalCode string                         `json:"postalCode"`
	City       string                         `json:"city"`
	Comments   string                         `json:"comments"`
	Iin        string                         `json:"iin"`
	OrderItems []*NewOrderItemLifeStyleMaster `json:"orderItems"`
}

type NewPartner struct {
	ID            int         `json:"id"`
	Name          string      `json:"name"`
	Description   string      `json:"description"`
	Image         string      `json:"image"`
	LegalAddress  string      `json:"legalAddress"`
	ActualAddress string      `json:"actualAddress"`
	Phone         string      `json:"phone"`
	URL           string      `json:"url"`
	Created       string      `json:"created"`
	Bank          string      `json:"bank"`
	Bic           string      `json:"bic"`
	Bin           string      `json:"bin"`
	Iic           string      `json:"iic"`
	Office        []NewOffice `json:"office"`
	Media         []string    `json:"media"`
	Banner        []string    `json:"banner"`
}

type NewShedule struct {
	ID    int `json:"id"`
	Day   int `json:"day"`
	Start int `json:"start"`
	End   int `json:"end"`
}

type NewTodo struct {
	Text string `json:"text"`
}

type Occupation struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Office struct {
	ID           int             `json:"id"`
	Name         string          `json:"name"`
	Description  string          `json:"description"`
	FullName     string          `json:"fullName"`
	Number       string          `json:"number"`
	Street       string          `json:"street"`
	Lon          string          `json:"lon"`
	Lat          string          `json:"lat"`
	PostCode     string          `json:"postCode"`
	BuildingCode string          `json:"buildingCode"`
	CityID       int             `json:"cityId"`
	PartnerID    int             `json:"partnerId"`
	ExampleWorks []ExampleWork   `json:"exampleWorks"`
	Services     []ServiceOffice `json:"services"`
	Days         []Day           `json:"days"`
}

type Order struct {
	ID            int          `json:"id"`
	Status        string       `json:"status"`
	PartnerID     int          `json:"partnerId"`
	StatusID      int          `json:"statusId"`
	IDMerchant    int          `json:"idMerchant"`
	FirstName     string       `json:"firstName"`
	LastName      string       `json:"lastName"`
	MiddleName    string       `json:"middleName"`
	Email         string       `json:"email"`
	Phone         string       `json:"phone"`
	Address       string       `json:"address"`
	PostalCode    string       `json:"postalCode"`
	City          string       `json:"city"`
	TransactionID int          `json:"transactionId"`
	Comments      string       `json:"comments"`
	Iin           string       `json:"iin"`
	Created       string       `json:"created"`
	OrderItems    []*OrderItem `json:"orderItems"`
}

type OrderItem struct {
	ID               int    `json:"id"`
	SubjectID        int    `json:"subjectId"`
	PartnerID        int    `json:"partnerId"`
	Cost             int    `json:"cost"`
	Quantity         int    `json:"quantity"`
	Label            string `json:"label"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	DeliveryDate     string `json:"deliveryDate"`
	Type             int    `json:"type"`
	TypeDesc         string `json:"typeDesc"`
	Paid             int    `json:"paid"`
	InstalmentPeriod string `json:"instalmentPeriod"`
	Book             *Book  `json:"book"`
}

// Заявки для админки клиентов
type OrderProfile struct {
	ID            int          `json:"id"`
	Status        string       `json:"status"`
	PartnerID     int          `json:"partnerId"`
	StatusID      int          `json:"statusId"`
	IDMerchant    int          `json:"idMerchant"`
	PostalCode    string       `json:"postalCode"`
	TransactionID int          `json:"transactionId"`
	Comments      string       `json:"comments"`
	Created       string       `json:"created"`
	OrderItems    []*OrderItem `json:"orderItems"`
}

type Partner struct {
	ID          string      `json:"id"`
	Name        string      `json:"name"`
	BankAccount BankAccount `json:"bankAccount"`
	Employees   []Employee  `json:"employees"`
	Roles       []Role      `json:"roles"`
}

type Pawn struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type PaymentScheme struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Permission struct {
	ID          string    `json:"id"`
	Role        Role      `json:"role"`
	Submodule   Submodule `json:"submodule"`
	AccessLevel int       `json:"accessLevel"`
}

type ProductInstagram struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Price        int    `json:"price"`
	CategoryID   int    `json:"categoryId"`
	CategoryName string `json:"categoryName"`
}

type Role struct {
	ID          string       `json:"id"`
	Name        string       `json:"name"`
	Permissions []Permission `json:"permissions"`
	Partner     Partner      `json:"partner"`
	Employees   []Employee   `json:"employees"`
}

type SMS struct {
	Phone string `json:"phone"`
	Kod   string `json:"kod"`
}

type ServiceOffice struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Time  int    `json:"time"`
	Price int    `json:"price"`
}

type Services struct {
	ID           int    `json:"id"`
	Address      string `json:"address"`
	CategoryName string `json:"categoryName"`
	OfficeName   string `json:"officeName"`
	URL          string `json:"url"`
}

type ServicesCategory struct {
	ID         int    `json:"id"`
	OfficeName string `json:"officeName"`
	URL        string `json:"url"`
	Address    string `json:"address"`
}

type Shedule struct {
	Days    []Day    `json:"days"`
	Masters []Master `json:"masters"`
}

type Status struct {
	OrderID int `json:"orderId"`
	Status  int `json:"status"`
}

type StatusRes struct {
	ID     int  `json:"id"`
	Status bool `json:"status"`
}

type Submodule struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Tag struct {
	ID    int    `json:"id"`
	Value string `json:"value"`
}

// Test query
type Todo struct {
	ID   string `json:"id"`
	Text string `json:"text"`
	Done bool   `json:"done"`
}

type TokenPayment struct {
	CardID  int `json:"cardId"`
	Amount  int `json:"amount"`
	OrderID int `json:"orderId"`
}

type Transaction struct {
	ID          int    `json:"id"`
	Date        string `json:"date"`
	Description string `json:"description"`
}

type TransactionExtract struct {
	Description string `json:"description"`
	Amount      int    `json:"amount"`
}

type TypeOrderItem struct {
	ID   int    `json:"id"`
	Type string `json:"type"`
}

type UserDetailsIn struct {
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	Iin          string `json:"iin"`
	Bday         string `json:"bday"`
	Address      string `json:"address"`
	Email        string `json:"email"`
	SocialStatus string `json:"socialStatus"`
}

type UserDetailsOut struct {
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	Iin          string `json:"iin"`
	Bday         string `json:"bday"`
	Address      string `json:"address"`
	Email        string `json:"email"`
	SocialStatus string `json:"socialStatus"`
}

type UserSignIn struct {
	Number   string `json:"number"`
	Password string `json:"password"`
}

type UserSignUp struct {
	Number   string        `json:"number"`
	Password string        `json:"password"`
	Crypted  string        `json:"crypted"`
	Details  UserDetailsIn `json:"details"`
}
