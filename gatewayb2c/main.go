package main

import (
	"os";
	"log";
	"fmt";
	"strconv";
	"context";
	"net/http";
	"encoding/json";
	"github.com/rs/cors";
	"github.com/joho/godotenv";
	"github.com/99designs/gqlgen/handler";
	"gitlab.com/unie.kz/nova/gatewayb2c/graph";
	orderOneClient "gitlab.com/unie.kz/nova/order/rpc/order";
	profileClient "gitlab.com/unie.kz/nova/cloudpayments/rpc/cloudpayments";
)


func WithUserAgent(base http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ua := r.Header.Get("Authorization")
		b2b := r.Header.Get("X-Admin-Token")
		ctx = context.WithValue(ctx, "jwt", ua)
		ctx = context.WithValue(ctx, "b2b", b2b)
		r = r.WithContext(ctx)

		base.ServeHTTP(w, r)
	})
}
func main() {
	err := godotenv.Load();
	if err != nil {	log.Fatal(err);}

	financeURL := os.Getenv("FINANCE_URL");
	orderURL := os.Getenv("ORDER_URL");
	smsURL := os.Getenv("SMS_URL");
	emailURL := os.Getenv("EMAIL_URL");
	cpURL := os.Getenv("CP_URL");
	lifestyleURL := os.Getenv("LIFESTYLE_URL");
	financedkzURL := os.Getenv("FINANCEDKZ_URL");
	b2bURL := os.Getenv("B2B_URL");
	partnerURL := os.Getenv("PARTNER_URL");
	storeURL := os.Getenv("STORE_URL");
	profileURL := os.Getenv("PROFILE_URL");
	bonuseURL := os.Getenv("BONUSE_URL");
	instagramShopURL := os.Getenv("INSTAGRAM_SHOP_URL");

	s := graph.New(
			financeURL, 		orderURL, 
			smsURL, 			emailURL, 
			cpURL, 				lifestyleURL, 
			financedkzURL, 		b2bURL, 
			partnerURL, 		storeURL, 
			profileURL, 		bonuseURL, 
			instagramShopURL,
		);

	mux := http.NewServeMux();
	mux.Handle("/graphql", handler.GraphQL(graph.NewExecutableSchema(s)));
	mux.Handle("/playground", handler.Playground("Gateway", "/graphql"));
	mux.HandleFunc("/cp/pay", cpPay);
	mux.HandleFunc("/epay", epay);

	handler := cors.Default().Handler(mux);
	wrap := WithUserAgent(handler);
	log.Fatal(http.ListenAndServe(":8082", wrap));
}


type Response struct {
	code int
}
func cpPay(w http.ResponseWriter, req *http.Request) {
	profile := Response{0}
	fmt.Println("tyuuuuuut")
	req.ParseForm()
	data := req.PostForm
	//ORDER DATA
	fmt.Println(data, data["InvoiceId"][0])
	invoiceId, _ := strconv.Atoi(data["InvoiceId"][0])
	client := orderOneClient.NewOrderServiceProtobufClient("http://localhost:9000", &http.Client{})
	hat, err := client.UpdateOrderStatus(context.Background(), &orderOneClient.StatusReq{OrderId: int64(invoiceId), Status: 2})
	fmt.Println(hat, err)
	//ORDER DATA
	//PROFILE DATA
	fClient := profileClient.NewCloudPaymentsProtobufClient("http://localhost:9013", &http.Client{})
	if data["AccountId"][0] != "" {
		var accId int64

		accId, _ = strconv.ParseInt(data["AccountId"][0], 10, 64)
		dat, err := fClient.InnerLinkCard(context.Background(), &profileClient.InnerCardLinkReq{
			AccountId:  accId,
			CardHolder: data["name"][0],
			ExpDate:    data["CardExpDate"][0],
			CardToken:  data["Token"][0],
			LastFour:   data["CardLastFour"][0],
			FirstSix:   data["CardFirstSix"][0],
		})
		log.Println(dat, err)
	}

	//PROFILE DATA
	jsonResult, err := json.Marshal(profile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//
	//

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResult)
}


type EpayRequest struct {
	BankName              string `json:"BANK_NAME"`
	CustomerName          string `json:"CUSTOMER_NAME"`
	CustomerMail          string `json:"CUSTOMER_MAIL"`
	CustomerPhone         string `json:"CUSTOMER_PHONE"`
	MerchantCertId        string `json:"MERCHANT_CERT_ID"`
	MerchantName          string `json:"MERCHANT_NAME"`
	OrderId               string `json:"ORDER_ID"`
	OrderAmount           string `json:"ORDER_AMOUNT"`
	OrderCurrency         string `json:"ORDER_CURRENCY"`
	DepartamentMerchantId string `json:"DEPARTMENT_MERCHANT_ID"`
	DepartamentAmount     string `json:"DEPARTMENT_AMOUNT"`
	MerchantSignType      string `json:"MERCHANT_SIGN_TYPE"`
	CustomerSignType      string `json:"CUSTOMER_SIGN_TYPE"`
	ResultsTimeStamp      string `json:"RESULTS_TIMESTAMP"`
	PaymentMerchantId     string `json:"PAYMENT_MERCHANT_ID"`
	PaymentAmount         string `json:"PAYMENT_AMOUNT"`
	PaymentReference      string `json:"PAYMENT_REFERENCE"`
	PaymentApprovalCode   string `json:"PAYMENT_APPROVAL_CODE"`
	PaymentResponseCode   string `json:"PAYMENT_RESPONSE_CODE"`
	BankSignCertId        string `json:"BANK_SIGN_CERT_ID"`
	BankSignType          string `json:"BANK_SIGN_TYPE"`
}
func epay(w http.ResponseWriter, req *http.Request) {
	data := EpayRequest{}
	decoder := json.NewDecoder(req.Body)
	decoder.Decode(&data)
	insertId, _ := strconv.Atoi(data.OrderId)
	client := orderOneClient.NewOrderServiceProtobufClient("http://localhost:9000", &http.Client{})
	client.UpdateOrderStatus(context.Background(), &orderOneClient.StatusReq{OrderId: int64(insertId), Status: 2})
}
