package sms

import (
	"context"
	"gitlab.com/unie.kz/nova/sms/rpc/sms"
	"net/http"
)

type Client struct {
	service sms.SmsService
}

func NewClient(url string) (*Client, error) {
	c := sms.NewSmsServiceProtobufClient(url, &http.Client{})
	return &Client{c}, nil
}

func (c *Client) SendSms(ctx context.Context, request *sms.SMS) (bool, error) {
	result, err := c.service.Send(context.Background(), request)
	if err != nil {
		return false, err
	}
	return result.Answer, nil
}

func (c *Client) ValidationKodSms(ctx context.Context, request *sms.SMS) (*sms.Answer, error) {
	result, err := c.service.Validation(context.Background(), request)
	if err != nil {
		return nil, err
	}
	return &sms.Answer{Status: result.Status, Key: result.Key}, nil
}
