package main

import (
    "net/http"

    s "gitlab.com/unie.kz/nova/sms/internal/sms"
    pb "gitlab.com/unie.kz/nova/sms/rpc/sms"
)

func main() {
  server := &s.Server{} // implements Haberdasher interface
  twirpHandler := pb.NewSmsServiceServer(server, nil)
  http.ListenAndServe(":9123", twirpHandler)
}
