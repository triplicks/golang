package smsserver;

import (
	"context";
	pb "gitlab.com/unie.kz/nova/sms/rpc/sms";
	"github.com/go-redis/redis";
	"math/rand";
	"strconv";
	"time";
)


func (s *Server) Send(response context.Context, request *pb.SMS) (Status *pb.Status, err error) {
	client := redis.NewClient(&redis.Options{
		Addr:"localhost:6379",
		Password: "",
		DB:0,
	})
	val, err := client.Get(request.Phone).Result()
	if val!=""{return &pb.Status{Answer:false},nil;}
	rand.Seed(time.Now().UTC().UnixNano())
	kod := strconv.Itoa(RandInt(1000,9999));
	err = ApiSend(request.Phone,kod);
	if err != nil { return nil, err;}
	err = client.Set(request.Phone, kod, 120000000000).Err() 
	if err != nil {	return nil, err;}
	return &pb.Status{Answer:true},nil;
}

func (s *Server) Validation(response context.Context, request *pb.SMS) (Answer *pb.Answer, err error){
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	val, err := client.Get(request.Phone).Result()
	if err != nil {	return nil, err;}
	answer:=&pb.Answer{};
	if(val==request.Kod) {
		answer.Status = true;
		answer.Key = GenerateHash(request.Phone);
		client.Del(request.Phone);	
	}else{answer.Status = false;}
	return answer,nil;
}
