package smsserver;


type Answer struct{
	Code int `json:"code"`
	Data struct{
		CampaignId int `json:"campaignId"`
		MessageId int `json:"messageId"`
		Status int `json:"status"`
		Recipient string `json:"recipient"`
	} `json:"data"`
	Message string `json:"message"`
}

type Server struct {}