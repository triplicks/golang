package smsserver;

import (
	"net/http";
	"encoding/json";
	"strings";
	"net/url";
	"io/ioutil";
	"encoding/hex"; 
	"golang.org/x/crypto/sha3";
	"math/rand";
);


func ApiSend(phone string, kod string) (err error){
	answer := Answer{};
	data:= url.Values{};
	data.Set("recipient",phone);
	data.Set("text",kod);
	curl,err := http.NewRequest("POST","https://api.mobizon.kz/service/message/sendsmsmessage?apiKey=72d23e6b27b6e3b0e7abb0314b1829e34dc4dd99",
						strings.NewReader(data.Encode()));
	if err!=nil {return err;}
	curl.Header.Add("Content-Type", "application/x-www-form-urlencoded");
	client := &http.Client{};
	r, err := client.Do(curl);
	if err!=nil {return err;}
	defer r.Body.Close();
	body,err := ioutil.ReadAll(r.Body);
	if err!=nil {return err;}
	json.Unmarshal(body,&answer);
    return;
}

func GenerateHash(phone string) (answer string){
	verNumberKey:="ghjdthrfyjvthfgjgjkextyyjverjlejngjkmpjdfntkz";
	hash:=sha3.New256();
	hash.Write([]byte(phone));
	answer = hex.EncodeToString(hash.Sum([]byte(verNumberKey)))
	return;
}
func RandInt(min int, max int) int {
    return min + rand.Intn(max-min);
}